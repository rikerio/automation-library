#include <catch2/catch_test_macros.hpp>
#include "memory_address.h"

using namespace automation;

TEST_CASE("MemoryAddress")
{
    SECTION("Compare")
    {
        CHECK(MemoryAddress(13, 4).equals({ 13, 4 }));
        CHECK(MemoryAddress(13, 4).equals({ 13, 5 }) == false);
    }

    SECTION("Construct")
    {
        MemoryAddress a(1, 8);
        CHECK(a.equals({ 2, 0 }));
    }

    SECTION("Addition")
    {
        MemoryAddress a(1, 3);
        MemoryAddress b(2, 7);
        MemoryAddress c = a + b;
        CHECK(c.equals({ 4, 2 }));
    }
}
