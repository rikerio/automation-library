#include <catch2/catch_test_macros.hpp>

#include "acyclic/var.h"
#include "acyclic/storage.h"
#include "acyclic/linker.h"
#include "utils/temp-folder.h"
#include <unistd.h>
#include <chrono>
#include <thread>
#include <iostream>

using namespace automation;
using namespace std::literals::chrono_literals;

SCENARIO("acyclic::Linker")
{
    TemporaryFolder temp_folder("acyclic_linker");
    auto storage = acyclic::Storage::create(temp_folder.path());

    REQUIRE(storage.has_value());

    GIVEN("A Data Object")
    {
        auto meta = acyclic::Data::create<uint32_t>(42);

        REQUIRE(storage->store("test", meta).has_value());

        acyclic::Linker linker(*storage);

        std::this_thread::sleep_for(200ms);

        WHEN("linking read only variable and meta object")
        {
            auto var = acyclic::ReadOnly::UInt32(0);
            linker.link("test", var);

            THEN("the variable will be updated")
            {
                auto new_meta = acyclic::Data::create<uint32_t>(43);
                CHECK(storage->store("test", new_meta).has_value());
                std::this_thread::sleep_for(200ms);
                linker.pre_loop();
                CHECK(var.value() == 43);
                linker.quit();
            }
        }

        WHEN("linking read/write variable and meta object")
        {
            int16_t tmp = 0;
            auto var = acyclic::ReadWrite::Int16(0);
            linker.link("test_rw", var);

            THEN("the variable will be updated")
            {
                var.value(43);
                linker.quit();
                std::this_thread::sleep_for(200ms);
                auto meta = storage->load("test_rw");
                REQUIRE(meta.has_value());
                std::memcpy(&tmp, meta->buffer().data(), meta->type().size());
                CHECK(tmp == 43);
            }
        }
    }

    GIVEN("No initial Data Object")
    {
        acyclic::Linker linker(*storage);
        std::this_thread::sleep_for(200ms);

        WHEN("linking variable and meta object")
        {
            auto var = acyclic::ReadOnly::Float(0.0f);
            linker.link("test", var);

            THEN("the variable will be updated")
            {
                auto new_meta = acyclic::Data::create<float>(43.0f);
                CHECK(storage->store("test", new_meta).has_value());
                std::this_thread::sleep_for(200ms);
                linker.pre_loop();
                CHECK(var.value() == 43.0f);
                linker.quit();
            }
        }
    }
}
