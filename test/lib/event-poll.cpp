#include <catch2/catch_test_macros.hpp>

#include <chrono>
#include <iostream>
#include <fstream>
#include <future>
#include <filesystem>
#include <functional>
#include <fcntl.h>
#include <sys/signal.h>
#include <sys/types.h>
#include "utils/child-process.h"
#include "utils/temp-folder.h"
#include "utils/event-poll.h"
#include "utils/event-descriptor.h"
#include "utils/signal-descriptor.h"
#include "utils/timer-descriptor.h"
#include "utils/inotify-descriptor.h"

using namespace std::literals::chrono_literals;
using namespace automation;

TEST_CASE("EventPoll")
{
    SECTION("Unregister")
    {
        utils::EventPoll epoll;
        auto event = utils::EventDescriptor::create();

        epoll.reg(event.fd());

        event.write(43);

        std::array<utils::EventPoll::Event, 1> events;
        CHECK(epoll.wait(events, 0) == 1);
        CHECK(events[0].data.fd == event.fd());
        event.read();

        epoll.unreg(event.fd());
        event.write(44);

        CHECK(epoll.wait(events, 1) == 0);
    }

    SECTION("Event")
    {
        utils::EventPoll epoll;
        auto event = utils::EventDescriptor::create();
        epoll.reg(event.fd());
        event.write(42);

        std::array<utils::EventPoll::Event, 1> events;
        CHECK(epoll.wait(events, 100) == 1);

        auto read = event.read();
        REQUIRE(read.valid());
        CHECK(read.value() == 42);
    }

    SECTION("Signal")
    {
        utils::EventPoll epoll;
        utils::SignalDescriptor signal({ SIGUSR1 });

        epoll.reg(signal.fd());

        ChildProcess process([&]() {
            ::kill(getppid(), SIGUSR1);
            ::exit(EXIT_SUCCESS);
        });

        process.wait();

        std::array<utils::EventPoll::Event, 1> events;
        CHECK(epoll.wait(events, 100) == 1);
        CHECK(events[0].data.fd == signal.fd());
    }

    SECTION("Timer")
    {
        utils::EventPoll epoll;
        auto timer = utils::TimerDescriptor::create_cyclic(1ms);

        epoll.reg(timer.fd());

        int cntr = 0;
        while (cntr < 10) {
            std::array<utils::EventPoll::Event, 1> events;
            CHECK(epoll.wait(events, 100) == 1);
            timer.read();
            cntr += 1;
        }

        CHECK(cntr >= 10);
    }

    SECTION("inotify")
    {
        namespace fs = std::filesystem;
        TemporaryFolder temp_folder("epoll-inotify");
        int cntr = 0;

        utils::EventPoll epoll;
        utils::InotifyDescriptor inotify_handler(temp_folder.path(), IN_CLOSE_WRITE);

        epoll.reg(inotify_handler.fd());

        auto touch_file = [&](const std::string& name) {
            std::ofstream(temp_folder.path() / name) << "test";
        };

        auto task = std::async([&]() {
            touch_file("file-1");
            touch_file("file-2");

            std::array<utils::EventPoll::Event, 20> events;
            CHECK(epoll.wait(events, 500) == 1);

            auto inotify_events = inotify_handler.read();

            std::for_each(inotify_events.begin(), inotify_events.end(), [&](auto& event) {
                CHECK(inotify_events.at(0).mask() & IN_CLOSE_WRITE);
                cntr += 1;
            });
        });

        task.wait();
        CHECK(cntr == 2);
    }
}
