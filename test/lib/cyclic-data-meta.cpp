#include <catch2/catch_test_macros.hpp>

#include "utils/scoped-cyclic-storage.h"
#include "utils/reset-env.h"
#include "utils/temp-folder.h"
#include "cyclic/memory-segment.h"
#include "cyclic/meta.h"

#include <unistd.h>

using namespace automation;

SCENARIO("cyclic::Meta")
{
    TemporaryFolder temp_folder("cyclic_data_meta");

    GIVEN("no storage")
    {
        WHEN("storing meta data on a filesystem")
        {
            using target_type = uint32_t;
            cyclic::Meta meta(Type<target_type>(), { 42, 0 });
            CHECK(cyclic::Meta::serialize(temp_folder.path() / "a", meta));

            THEN("it should be accessible on the filesystem")
            {
                auto exp_meta = cyclic::Meta::deserialize(temp_folder.path() / "a");

                REQUIRE(exp_meta.has_value());
                CHECK(exp_meta->adr().offset == 42);
                CHECK(exp_meta->adr().index == 0);
                CHECK(exp_meta->type() == Type<target_type>());
            }

            THEN("non exisiting file should be an invalid meta object")
            {
                auto exp_meta = cyclic::Meta::deserialize(temp_folder.path() / "b");
                CHECK(!exp_meta.has_value());
            }
        }
    }

    GIVEN("an existing memory segment")
    {
        ScopedStorage scoped_storage(temp_folder.path(), 1024);
        auto storage = cyclic::Storage::attach(scoped_storage.path());

        REQUIRE(storage.has_value());

        WHEN("a new data points new breaks naming policy")
        {
            auto exp = storage->allocate("tmp;", 1);

            CHECK(exp.has_value() == false);
        }

        WHEN("a new data point is created")
        {
            auto exp = storage->allocate("tmp", Type<BitType>());

            REQUIRE(exp.has_value());

            THEN("memory data is valid")
            {
                auto& meta = exp.value();
                CHECK(meta.type() == Type<BitType>());
                CHECK(std::filesystem::exists(storage->data_path() / "tmp"));
            }
        }

        WHEN("a new data point is created with manual offset")
        {
            auto meta = cyclic::Meta::create<uint64_t>({ 81, 3 });
            auto exp = storage->store("tmp", meta);

            REQUIRE(exp.has_value());

            THEN("memory data is valid")
            {
                auto load = storage->load("tmp");
                REQUIRE(load.has_value());
                CHECK(load->type() == Type<uint64_t>());
                CHECK(load->adr().offset == 81);
                CHECK(load->adr().index == 3);
            }
        }

        WHEN("a new data point is created outside of area")
        {
            auto segment_a = storage->allocate(512);
            auto segment_b = storage->allocate(512);
            auto segment_c = storage->allocate(sizeof(uint64_t));

            CHECK(segment_a.has_value());
            CHECK(segment_b.has_value() == false); // we cannot use the full size of the shm
            CHECK(segment_c.has_value() == true);
        }

        WHEN("multiple new data points are created with auto offset")
        {
            auto exp_meta_a = storage->allocate<uint64_t>("a");
            auto& meta_a = exp_meta_a.value();

            REQUIRE(exp_meta_a.has_value());

            THEN("memory data can be retrieved")
            {
                auto exp_meta = storage->load("a");
                auto& meta = exp_meta.value();
                REQUIRE(exp_meta.has_value());
                CHECK(meta.type() == meta_a.type());
                CHECK(meta.handle() == meta_a.handle());
                CHECK(meta.adr().offset == meta_a.adr().offset);
                CHECK(meta.adr().index == meta_a.adr().index);
            }

            THEN("removing data is invalid when fetching")
            {
                CHECK(storage->remove("a"));
                CHECK(storage->load("a").has_value() == false);
            }
        }
    }
}
