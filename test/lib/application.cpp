#include <catch2/catch_test_macros.hpp>
#include <unistd.h>
#include <iostream>
#include <filesystem>

#include "application.h"

using namespace automation;
namespace fs = std::filesystem;

namespace {

static std::string unique_name()
{
    static unsigned int id = 1;
    return std::string("application-" + std::to_string(::getpid()) + "-" + std::to_string(id++));
}

} // namespace

SCENARIO("Application")
{
    const std::string name = unique_name();

    GIVEN("No Application")
    {
        auto application = Application::attach(name);

        CHECK(application.has_value() == false);
        CHECK(fs::exists(Application::build_root_path(name)) == false);

        WHEN("creating a new application")
        {
            auto app = Application::create(name, { 1024 });
            REQUIRE(app.has_value());

            THEN("folder structure will be existing")
            {
                CHECK(fs::exists(app.value().root_path()));
                CHECK(Application::remove(name, true));
            }

            THEN("removing it will be successfull")
            {
                auto res = Application::remove(name, true);

                CHECK(res.has_value());
                CHECK(!Application::exists(name));
                CHECK(!fs::exists(app->root_path()));
            }
        }
    }
}
