#include <catch2/catch_test_macros.hpp>

#include <unistd.h>
#include <future>
#include "utils/lock-file.h"

using namespace automation;

std::filesystem::path name()
{
    static unsigned int cntr = 0;

    std::string filename = "tmp-" + std::to_string(getpid()) + "-" + std::to_string(++cntr);
    return std::filesystem::temp_directory_path() / filename;
}

TEST_CASE("utils::LockFile")
{
    auto filename = name();

    SECTION("lock")
    {
        std::string order = "";

        utils::LockFile first_lock(filename, utils::LockFile::Type::Exclusive);

        first_lock.lock();

        auto task = std::async([&]() {
            utils::LockFile second_lock(filename, utils::LockFile::Type::Exclusive);
            second_lock.lock();
            order += "b";
            second_lock.unlock();
        });

        order += "a";

        first_lock.unlock();

        task.wait();
        CHECK(order == "ab");
    }
}
