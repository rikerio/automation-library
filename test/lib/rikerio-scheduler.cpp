#include <catch2/catch_test_macros.hpp>

#include "cyclic/thread.h"
#include "cyclic/strategy/rikerio.h"
#include "utils/temp-folder.h"

using namespace automation;

struct CounterTask {

    unsigned int init_counter = 0;
    unsigned int pre_loop_counter = 0;
    unsigned int loop_counter = 0;
    unsigned int post_loop_counter = 0;
    unsigned int quit_counter = 0;
    std::atomic<bool> done = false;

    void init()
    {
        init_counter += 1;
    }

    void pre_loop()
    {
        pre_loop_counter += 1;
    }

    void loop()
    {
        loop_counter += 1;
        done.store(loop_counter > 10);
    }

    void post_loop()
    {
        post_loop_counter += 1;
    }

    void quit()
    {
        quit_counter += 1;
    }
};

TEST_CASE("RikerIO cyclic strategy")
{
    TemporaryFolder temp_folder("rikerio-strategy");
    auto socket_path = temp_folder.path() / "socket";

    SECTION("count task calls")
    {
        CounterTask task;
        auto thread = cyclic::Thread::create<cyclic::strategy::RikerIO>(socket_path, 1, 0);

        thread.use(task);
        thread.start();

        while (1) {
            usleep(100);

            if (task.done.load()) {
                break;
            }
        }

        thread.stop();

        CHECK(task.init_counter == 1);
        CHECK(task.loop_counter >= 10);
        CHECK(task.quit_counter == 1);
    }
}
