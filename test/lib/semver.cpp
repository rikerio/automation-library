#include <catch2/catch_test_macros.hpp>
#include "utils/semver.h"

using namespace automation::utils;

TEST_CASE("SemVer")
{

    SECTION("Malformated")
    {
        CHECK_THROWS(SemanticVersion(""));
        CHECK_THROWS(SemanticVersion("abcdef"));
        CHECK_THROWS(SemanticVersion("vabcdef"));
        CHECK_THROWS(SemanticVersion("v1.0.2-100-"));
    }

    SECTION("Formated with v")
    {
        SemanticVersion v("v1.0.2");

        CHECK(v.get_major() == 1);
        CHECK(v.get_minor() == 0);
        CHECK(v.get_patch() == 2);
        CHECK(v.get_version() == "1.0.2");
        CHECK(v.get_extended_version() == "1.0.2-0-");
    }

    SECTION("Formated without v")
    {

        SemanticVersion v("3.2.1");

        CHECK(v.get_major() == 3);
        CHECK(v.get_minor() == 2);
        CHECK(v.get_patch() == 1);
        CHECK(v.get_version() == "3.2.1");
    }

    SECTION("Formated with hash")
    {
        SemanticVersion v("v1.0.2-100-abcde");

        CHECK(v.get_major() == 1);
        CHECK(v.get_minor() == 0);
        CHECK(v.get_patch() == 2);

        CHECK(v.get_commit() == 100);
        CHECK(v.get_hash() == "abcde");
    }

    SECTION("Compatibility")
    {

        SemanticVersion a("1.0.0");
        SemanticVersion b("1.1.0");
        SemanticVersion c("1.1.5");
        SemanticVersion d("2.0.0");

        CHECK(b ^ a);
        CHECK(c ^ b);
        CHECK_FALSE(d ^ c);
        CHECK_FALSE(a ^ b);
        CHECK(a ^ a);
    }
}
