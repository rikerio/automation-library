#include <catch2/catch_test_macros.hpp>

#include "application.h"
#include "acyclic/storage.h"
#include "utils/scoped-application.h"
#include "utils/scoped-storage.h"
#include "utils/temp-folder.h"
#include <array>
#include <unistd.h>

using namespace automation;

SCENARIO("acyclic::Storage")
{
    GIVEN("No Acyclic Data Storage")
    {
        auto path = TemporaryFolder("acyclic-storage").path();

        WHEN("attaching to a storage")
        {
            auto storage = acyclic::Storage::attach(path);

            THEN("the storage is not valid")
            {
                CHECK(storage.has_value() == false);
            }
        }
    }

    GIVEN("A Acyclic Data Storage")
    {
        ScopedStorage scoped_storage("acyclic-storage");
        auto storage = acyclic::Storage::attach(scoped_storage.path());

        REQUIRE(storage.has_value());

        WHEN("storing data")
        {
            auto stored_data = acyclic::Data::create<uint32_t>(42);
            storage->store("test", stored_data);

            THEN("the data can be loaded")
            {
                auto loaded_data = storage->load("test");

                REQUIRE(loaded_data.has_value());
                CHECK(stored_data.buffer() == loaded_data->buffer());
            }

            THEN("the data can be reset")
            {
                CHECK(storage->reset("test").has_value());
                CHECK(storage->load("test").has_value() == false);
            }

            THEN("non existing data cannot be reset")
            {
                CHECK(storage->reset("foo").has_value() == false);
            }
        }
    }
}

SCENARIO("acyclic::Data")
{
    TemporaryFolder temp_folder("acyclic_meta");
    std::filesystem::path path = temp_folder.path() / "test";

    GIVEN("A Data Object")
    {
        auto meta = acyclic::Data::create<uint32_t>(42);

        WHEN("serializing the object")
        {
            REQUIRE(acyclic::Data::serialize(path, meta).has_value());

            THEN("it will be located in the temporary folder")
            {
                CHECK(std::filesystem::exists(path));
            }

            AND_THEN("reading from fs will generate an equal meta object")
            {
                auto read_meta = acyclic::Data::deserialize(path);
                REQUIRE(read_meta.has_value());
                CHECK(read_meta->type() == meta.type());
                CHECK(memcmp(std::data(read_meta->buffer()), std::data(meta.buffer()), 4) == 0);
            }
        }
    }
}
