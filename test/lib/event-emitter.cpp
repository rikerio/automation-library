#include <catch2/catch_test_macros.hpp>
#include "utils/event-emitter.h"

using namespace automation;

SCENARIO("EventEmitter")
{

    GIVEN("An Event Emitter")
    {

        utils::EventEmitter<> e;

        WHEN("registrating event")
        {
            unsigned int event_fired = 0;
            e.on(1, [&]() {
                event_fired += 1;
            });

            THEN("firing it will be handled")
            {

                e.emit(1);
                CHECK(event_fired == 1);
                e.emit(2);
                CHECK(event_fired == 1);
            }
        }

        WHEN("handling with nested call")
        {
            unsigned int event_fired = 0;
            std::function<void()> handler = [&event_fired]() {
                event_fired += 1;
            };
            e.on(1, [&]() {
                handler();
                CHECK_THROWS(e.on(2, handler));
            });

            THEN("will throw")
            {
                e.emit(1);
                e.emit(2);
                CHECK(event_fired == 1);
            }
        }
    }

    GIVEN("An extended EventEmitter")
    {

        utils::EventEmitter<int> e;

        unsigned int event_fired = 0;
        int event_arg = 0;

        e.on(1, [&](int arg) {
            event_fired += 1;
            event_arg = arg;
        });

        e.emit(1, 2);
        e.emit(2, 3);
        CHECK(event_fired == 1);
        CHECK(event_arg == 2);
    }
}
