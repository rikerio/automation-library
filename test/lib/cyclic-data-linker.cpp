#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_floating_point.hpp>
#include "direction.h"
#include "types.h"
#include "utils/reset-env.h"
#include "utils/scoped-cyclic-storage.h"
#include "cyclic/link.h"
#include "cyclic/meta.h"
#include "cyclic/linker.h"
#include "cyclic/converter.h"
#include "cyclic/provider.h"

using namespace automation;

namespace {

class TestConverter : public cyclic::AbstractConverter {
public:
    TestConverter(double min, double max)
        : _min(min)
        , _max(max)
    {
    }

    double convert(void* source) const
    {
        int16_t* source_value = (int16_t*)source;

        double tmp = (double)*source_value / (double)0x7FFF;
        return tmp * (_max - _min) + _min;
    }

    void deconvert(double source, void* target) const
    {
        double tmp_a = (source - _min) / (_max - _min);
        double tmp_b = tmp_a * (double)0x7fff;
        int16_t* target_value = (int16_t*)target;
        *target_value = tmp_b;
    }

private:
    double _min = 0.0f;
    double _max = 0.0f;
};

template <typename T>
class Pointer {
public:
    Pointer(void* ptr)
        : _ptr((char*)ptr)
    {
    }

    T value() const
    {
        return *(T*)(_ptr);
    }

    void value(T v)
    {
        std::memcpy(_ptr, &v, sizeof(T));
    }

private:
    char* _ptr = nullptr;
};

} // namespace

SCENARIO("CyclicData Linker")
{
    GIVEN("No application")
    {
        WHEN("creating a simple variable")
        {
            cyclic::ReadOnly::UInt32 var;

            THEN("the default value applies and it is not linked")
            {
                CHECK(var.value() == 0);
                CHECK(var.linked() == false);
            }
        }

        WHEN("creating a CopyJob to read")
        {
            uint32_t shm = 42;
            cyclic::ReadOnly::UInt32 var(0);
            cyclic::Job job(var, &shm, 0);

            THEN("the initial value won't be touched")
            {
                CHECK(var.value() == 0);
                CHECK(var.linked());
            }

            AND_THEN("the job reads from the shared memory")
            {
                job.execute_read();
                CHECK(var.value() == 42);
                CHECK(var.changed());
                CHECK(var.last_value() == 0);
            }
        }

        WHEN("creating a CopyJob to read a Bit")
        {
            uint8_t shm = 0;
            cyclic::ReadOnly::Bit var(false);
            cyclic::Job job(var, &shm, 0);

            THEN("the initial value won't be touched")
            {
                CHECK(var.linked());
                CHECK(var.value() == 0);
            }

            AND_THEN("the job reads from the shared memory")
            {
                shm = 0x01;
                job.execute_read();
                CHECK(var.value() == true);
                CHECK(var.last_value() == false);
                CHECK(var.changed() == true);
            }

            AND_THEN("the job reads false from shared memory")
            {
                shm = 0x00;
                job.execute_read();
                CHECK(var.value() == false);
            }
        }

        WHEN("creating a CopyJob to read a Bit with offset")
        {
            uint8_t shm = 0;
            cyclic::ReadOnly::Bit var(false);
            cyclic::Job job(var, &shm, { 0, 3 });

            THEN("the initial value won't be touched")
            {
                CHECK(var.linked());
                CHECK(var.value() == 0);
            }

            AND_THEN("the job reads false from the shared memory")
            {
                shm = 0x01;
                job.execute_read();
                CHECK(var.value() == false);
            }

            AND_THEN("the job reads true from shared memory")
            {
                shm = 0x08;
                job.execute_read();
                CHECK(var.value() == true);
            }

            AND_THEN("the job reads false from shared memory")
            {
                shm = 0xF7;
                job.execute_read();
                CHECK(var.value() == false);
            }
        }

        WHEN("creating a CopyJob to write")
        {
            uint32_t shm = 0;
            cyclic::WriteOnly::UInt32 var(42);
            cyclic::Job job(var, &shm, 0);

            THEN("the job writes to the shared memory")
            {
                CHECK(var.linked());
                job.execute_write();
                CHECK(shm == 42);
            }
        }

        WHEN("creating a CopyJob to write a Bit with some offset")
        {
            uint32_t shm = 0;
            cyclic::WriteOnly::Bit var(true);
            cyclic::Job job(var, &shm, { 0, 4 });

            THEN("the job writes to the shared memory")
            {
                CHECK(var.linked());
                job.execute_write();
                CHECK(shm == 16);
            }
        }

        WHEN("creating a CopyJob to read and write")
        {
            uint32_t shm = 42;
            cyclic::ReadWrite::UInt32 var(0);
            cyclic::Job job(var, &shm, 0);
            THEN("the job reads from the shared memory")
            {
                CHECK(var.linked());
                job.execute_read();
                CHECK(var.value() == 42);
            }

            THEN("the job executes write for non changed content")
            {
                job.execute_read();
                CHECK(var.value() == 42);
                shm = 43;
                job.execute_write(); // nothing changed in the var so no write is executed
                CHECK(shm == 43);
            }

            THEN("the job executes write non changed content")
            {
                job.execute_read();
                CHECK(var.value() == 42);
                var.value(44);
                job.execute_write();
                CHECK(shm == 44);
            }
        }

        WHEN("creating and using a converter")
        {
            int16_t source = 0x0000;
            TestConverter converter(0.0f, 10.0f);

            THEN("convert zero")
            {
                CHECK(converter.convert(&source) == 0.0f);
            }

            THEN("convert positive maximum")
            {
                source = 0x7FFF;
                CHECK(converter.convert(&source) == 10.0f);
            }

            THEN("convert negative maximum")
            {
                source = 0x8001;
                CHECK(converter.convert(&source) == -10.0f);
            }
        }

        WHEN("using a ConverterJob for conversions")
        {
            int16_t source = 0x0000;
            cd::ro::Double target(0.0f);
            auto converter = std::make_unique<TestConverter>(0.0f, 10.0f);
            cyclic::ConverterJob job(&source, target, std::move(converter));

            THEN("execute_read will trigger convert")
            {
                source = 0x7FFF;
                job.execute_read();
                CHECK(target.value() == 10.0f);
            }
        }

        WHEN("using a ConverterJob for deconversions")
        {
            int16_t shm = 0x0000;
            cd::wo::Double var(0.0f);
            auto converter = std::make_unique<TestConverter>(0.0f, 10.0f);
            cyclic::ConverterJob job(&shm, var, std::move(converter));

            THEN("execute_read will trigger convert")
            {
                var.value(10.0f);
                job.execute_write();
                CHECK(shm == 0x7FFF);
            }
        }

        WHEN("using a ConverterJob for float deconversions")
        {
            int16_t shm = 0x0000;
            cd::wo::Float var(0.0f);
            auto converter = std::make_unique<TestConverter>(0.0f, 10.0f);
            cyclic::ConverterJob job(&shm, var, std::move(converter));

            THEN("execute_read will trigger convert")
            {
                var.value(10.0f);
                job.execute_write();
                CHECK(shm == 0x7FFF);
            }
        }
    }

    ResetEnv reset_env({ "XDG_DATA_DIR" });
    GIVEN("an application")
    {
        ScopedStorage scoped_storage("cyclic_data_linker", { 4096 });
        auto storage = cyclic::Storage::attach(scoped_storage.path());

        REQUIRE(storage.has_value());

        GIVEN("A link for a read job with multiple entries")
        {
            storage->link("link.1", "data.1");
            storage->link("link.1", "data.2");
            auto linker = cyclic::Linker(*storage);
            auto var = cyclic::ReadOnly::UInt32(0);

            CHECK(linker.link("link.1", var).has_value() == false);
            CHECK(linker.error_list().size() == 1);
        }

        GIVEN("A link for a read job with exceeding offset")
        {
            storage->link("link.1", "data.1", 1024);
            auto linker = cyclic::Linker(*storage);
            auto var = cyclic::ReadOnly::UInt32(0);

            CHECK(linker.link("link.1", var).has_value() == false);
            CHECK(linker.error_list().size() == 1);
        }

        GIVEN("A Task, Linker and a Read-Only Var")
        {
            using target_type = uint32_t;

            auto meta = storage->allocate<target_type>("data.1");
            auto pointer = (target_type*)storage->get_address_from_handle(meta->handle());
            auto link = storage->link("link.1", "data.1");
            auto linker = cyclic::Linker(*storage);
            auto var = cyclic::ReadOnly::UInt32(0);
            auto res = linker.link("link.1", var);

            REQUIRE(res.has_value());

            THEN("Changes on the Shared Memory affect the local variable")
            {
                *pointer = 0;
                CHECK(var.value() == 0);
                *pointer = 42;
                linker.pre_loop();
                CHECK(var.value() == 42);
            }
        }

        GIVEN("Linker and a Write-Only Var")
        {
            using target_type = uint16_t;

            auto meta = storage->allocate<target_type>("data.1");
            auto pointer = Pointer<target_type>(storage->get_address_from_handle(meta->handle()));
            auto link = storage->link("link.1", "data.1");
            auto linker = cyclic::Linker(*storage);
            auto var = cyclic::WriteOnly::UInt16();
            auto res = linker.link("link.1", var);

            REQUIRE(res.has_value());

            THEN("init, write and quit are working")
            {
                CHECK(var.linked());
                var.value(45);
                linker.post_loop();
                CHECK(pointer.value() == 45);
                var.value(46);
                linker.quit();
                CHECK(pointer.value() == 46);
            }
        }

        GIVEN("Linker and a Read-Write Var")
        {
            using target_type = uint16_t;

            auto meta = storage->allocate<target_type>("data.1");
            auto pointer = Pointer<target_type>(storage->get_address_from_handle(meta->handle()));
            auto link = storage->link("link.1", "data.1");
            auto linker = cyclic::Linker(*storage);
            auto var = cyclic::ReadWrite::UInt16();
            auto res = linker.link("link.1", var);

            REQUIRE(res.has_value());

            THEN("init, read, write and quit are working")
            {
                pointer.value(44);

                CHECK(var.linked());
                linker.pre_loop();
                CHECK(var.value() == 44);
                var.value(45);
                linker.post_loop();
                CHECK(pointer.value() == 45);
                var.value(46);
                linker.quit();
                CHECK(pointer.value() == 46);
            }
        }

        GIVEN("Linker")
        {
            using target_type = int32_t;
            using converter_ptr = std::unique_ptr<cyclic::AbstractConverter>;

            auto meta = storage->allocate<target_type>("data.1");
            auto linker = cyclic::Linker(*storage);

            auto converter = [](const std::vector<double>& args) -> converter_ptr {
                return std::make_unique<TestConverter>(args[0], args[1]);
            };

            cyclic::ConverterFactory::setup("test", converter);

            WHEN("linking with no meta")
            {
                auto var = cd::ro::Int32(0);

                CHECK(storage->link("link.1", "data.2").has_value());
                CHECK(linker.link("link.1", var).has_value() == false);
            }

            WHEN("linking with meta of invalid size (read)")
            {
                auto var = cd::ro::Int16(0);

                CHECK(storage->link("link.1", "data.1").has_value());
                CHECK(linker.link("link.1", var).has_value());
                CHECK(linker.error_list().size() > 0);
            }

            WHEN("linking with meta of invalid size (write)")
            {
                auto var = cd::wo::UInt64(0);

                CHECK(storage->link("link.1", "data.1").has_value());
                CHECK(linker.link("link.1", var).has_value());
                CHECK(linker.error_list().size() > 0);
            }

            WHEN("linking with meta of invalid size (read/write)")
            {
                auto var = cd::rw::UInt16(0);

                CHECK(storage->link("link.1", "data.1").has_value());
                CHECK(linker.link("link.1", var).has_value());
                CHECK(linker.error_list().size() > 0);
            }

            WHEN("linking with link option and unknown converter")
            {
                auto var = cd::ro::Float(0);
                auto options = cyclic::LinkOptions { 0, "unknown" };

                CHECK(storage->link("link.1", "data.1", options).has_value());
                CHECK(linker.link("link.1", var).has_value());
                CHECK(linker.error_list().size() > 0);
            }

            WHEN("linking with link option but no float/double")
            {
                auto var = cd::ro::UInt32(0);
                auto options = cyclic::LinkOptions { 0, "test", 0.0f, 10.0f };

                CHECK(storage->link("link.1", "data.1", options).has_value());
                CHECK(linker.link("link.1", var).has_value());
                CHECK(linker.error_list().size() > 0);
            }
        }

        GIVEN("Linker, Converter and Read Var")
        {
            using target_type = int16_t;
            using converter_ptr = std::unique_ptr<cyclic::AbstractConverter>;

            auto converter = [](const std::vector<double>& args) -> converter_ptr {
                return std::make_unique<TestConverter>(args[0], args[1]);
            };

            cyclic::ConverterFactory::setup("test", converter);
            cyclic::LinkOptions link_options { 0, "test", 0.0f, 10.0f };

            auto meta = storage->allocate<target_type>("data.1");
            auto pointer = Pointer<target_type>(storage->get_address_from_handle(meta->handle()));
            auto link = storage->link("link.1", "data.1", link_options);
            auto linker = cyclic::Linker(*storage);
            auto var = cd::ro::Float(0.0f);
            auto res = linker.link("link.1", var);

            REQUIRE(res.has_value());

            CHECK(linker.error_list().size() == 0);

            THEN("init, read, write and quit are working")
            {
                pointer.value(0x7fff);

                CHECK(var.linked());
                linker.pre_loop();
                CHECK(var.value() == 10.0f);
            }
        }

        GIVEN("Linker, Converter and Write Var")
        {
            using target_type = int16_t;
            using converter_ptr = std::unique_ptr<cyclic::AbstractConverter>;

            auto converter = [](const std::vector<double>& args) -> converter_ptr {
                return std::make_unique<TestConverter>(args[0], args[1]);
            };

            cyclic::ConverterFactory::setup("test", converter);
            cyclic::LinkOptions link_options { 0, "test", 0.0f, 10.0f };

            auto meta = storage->allocate<target_type>("data.1");
            auto pointer = Pointer<target_type>(storage->get_address_from_handle(meta->handle()));
            auto link = storage->link("link.1", "data.1", link_options);
            auto linker = cyclic::Linker(*storage);
            auto var = cd::wo::Float(0.0f);
            auto res = linker.link("link.1", var);

            REQUIRE(res.has_value());

            CHECK(linker.error_list().size() == 0);

            THEN("init, read, write and quit are working")
            {
                var.value(10.0f);
                CHECK(var.linked());
                linker.post_loop();
                CHECK(pointer.value() == 0x7fff);
                var.value(0);
                linker.post_loop();
                CHECK(pointer.value() == 0x0000);
            }
        }
    }
}

SCENARIO("CyclicData Provider")
{
    ResetEnv reset_env({ "XDG_DATA_DIR" });
    GIVEN("an application")
    {
        ScopedStorage scoped_storage("cyclic_data_linker", { 4096 });
        auto storage = cyclic::Storage::attach(scoped_storage.path());

        REQUIRE(storage.has_value());

        GIVEN("A provider for a read job")
        {
            auto provider = cyclic::Provider(*storage);

            WHEN("creating and linking a variable")
            {
                auto var = cyclic::ReadOnly::UInt32(0);

                provider.create(var, "test");

                THEN("the variable is represented on the shm")
                {
                    auto meta = storage->load("test");

                    REQUIRE(meta.has_value());
                    CHECK(meta->type() == Type<uint32_t>());
                }
            }

            WHEN("creating, linking and altering a read only variable")
            {
                auto var = cyclic::ReadOnly::UInt32(0);

                provider.create(var, "test");

                THEN("the changes are visible on the shm")
                {
                    auto meta = storage->load("test");
                    uint32_t* tmp = (uint32_t*)storage->get_address_from_handle(meta->handle());
                    *tmp = 42;
                    provider.pre_loop();
                    CHECK(var.value() == 42);
                }
            }

            WHEN("creating, linking and altering a read write variable")
            {
                auto var = cyclic::ReadWrite::UInt32(0);

                provider.create(var, "test");

                THEN("the changes are visible on the shm")
                {
                    auto meta = storage->load("test");
                    REQUIRE(meta.has_value());
                    uint32_t* tmp = (uint32_t*)storage->get_address_from_handle(meta->handle());
                    var.value(42);
                    provider.post_loop();
                    CHECK(*tmp == 42);
                }
            }
        }
    }
}
