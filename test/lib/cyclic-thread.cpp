#include <catch2/catch_test_macros.hpp>

#include <chrono>
#include <mutex>
#include <condition_variable>
#include "cyclic/thread.h"

using namespace automation;
using namespace std::chrono_literals;

namespace {

static std::mutex _mutex;
static std::condition_variable _cond_var;

bool wait(std::chrono::duration<long, std::milli> duration = 1s)
{
    std::unique_lock lock(_mutex);
    return _cond_var.wait_for(lock, duration) != std::cv_status::timeout;
}

void notify()
{
    _cond_var.notify_one();
}

} // namespace

struct CounterTask {

    unsigned int init_counter = 0;
    unsigned int pre_loop_counter = 0;
    unsigned int loop_counter = 0;
    unsigned int post_loop_counter = 0;
    unsigned int quit_counter = 0;
    std::atomic<bool> done = false;

    enum class Type {
        yield,
        noyield
    } type;

    CounterTask(Type type = Type::noyield)
        : type(type)
    {
    }

    void init()
    {
        init_counter += 1;
    }

    void pre_loop()
    {
        pre_loop_counter += 1;
    }

    void loop()
    {
        loop_counter += 1;
        done.store(loop_counter > 10);
        if (type == Type::yield && loop_counter > 10) {
            notify();
        }
    }

    void post_loop()
    {
        post_loop_counter += 1;
    }

    void quit()
    {
        quit_counter += 1;
    }
};

class MockStrategy : public cyclic::Thread::Strategy {

public:
    uint32_t setup_cntr = 0;
    uint32_t yield_cntr = 0;

    MockStrategy(uint32_t cycle_count)
        : _cycle_count(cycle_count)
    {
    }

    void setup()
    {
        setup_cntr += 1;
    }

    void yield()
    {
        yield_cntr += 1;
        if (_cycle_count > 0 && yield_cntr >= _cycle_count) {
            notify();
        }
        std::this_thread::sleep_for(10ms);
    }

    void quit()
    {
        yield_cntr = _cycle_count;
    }

private:
    uint32_t _cycle_count = 0;
};

TEST_CASE("CyclicThread")
{

    SECTION("count setups and yields")
    {
        auto mock_strategy_ptr = std::make_unique<MockStrategy>(10);
        auto& mock_strategy = *mock_strategy_ptr.get();

        CHECK(mock_strategy.setup_cntr == 0);
        CHECK(mock_strategy.yield_cntr == 0);

        cyclic::Thread thread(std::move(mock_strategy_ptr));

        thread.start();
        CHECK(wait(1s));
        thread.stop();

        CHECK(mock_strategy.setup_cntr == 1);
        CHECK(mock_strategy.yield_cntr >= 10);
    }

    SECTION("execute single task")
    {
        auto mock_strategy_ptr = std::make_unique<MockStrategy>(0);

        CounterTask task(CounterTask::Type::yield);
        cyclic::Thread thread(std::move(mock_strategy_ptr));

        thread.use(task);
        thread.start();
        CHECK(wait(2s));
        thread.stop();

        CHECK(task.init_counter == 1);
        CHECK(task.loop_counter >= 10);
        CHECK(task.quit_counter == 1);
    }
}
