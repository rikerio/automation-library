#include <catch2/catch_test_macros.hpp>
#include "types.h"

using namespace automation;

TEST_CASE("Type")
{
    SECTION("Undefined")
    {
        CHECK(BaseType() != Type<Undefined>());
    }

    SECTION("create")
    {
        auto type = Type<uint32_t>();
        CHECK(type.name() == "Uint32");
        CHECK(type.size() == sizeof(uint32_t));
    }

    SECTION("create int8")
    {
        auto type = Type<int8_t>();
        CHECK(type.name() == "Int8");
        CHECK(type.size() == sizeof(int8_t));
    }

    SECTION("compare")
    {
        auto type_uint = Type<uint8_t>();
        auto type_int = Type<int8_t>();
        auto type_char = Type<char>();
        auto type_string = Type<std::string>(32);
        CHECK(type_uint != type_int);
        CHECK(type_uint != type_char);
        CHECK(type_char != type_string);
        CHECK(type_uint == type_uint);
    }

    SECTION("string")
    {
        auto type_string = Type<std::string>(32);
        CHECK(type_string.name() == "String");
        CHECK(type_string.size() == 32);
    }

    SECTION("buffer")
    {
        auto type_buffer = Type<Buffer>(32);
        CHECK(type_buffer.name() == "Buffer");
        CHECK(type_buffer.size() == 32);
        CHECK(type_buffer.sizing_strategy() == SizingStrategy::Bytes);
    }

    SECTION("custom")
    {
        struct Custom {
            uint32_t a;
            uint16_t b;
        };
        auto type_custom = Type<Custom>();
        CHECK(type_custom.name() == "Buffer");
        CHECK(type_custom.size() == sizeof(Custom));
    }

    SECTION("from name")
    {
        auto type_uint32 = BaseType::type_from_name("Uint32");
        auto type_float = BaseType::type_from_name("Float");
        auto type_string = BaseType::type_from_name("String", 32);
        auto type_buffer = BaseType::type_from_name("Buffer", 16);
        auto type_undef = BaseType::type_from_name("abcd");

        CHECK(type_uint32.name() == "Uint32");
        CHECK(type_uint32.size() == sizeof(uint32_t));

        CHECK(type_float.name() == "Float");
        CHECK(type_float.size() == sizeof(float));

        CHECK(type_string.name() == "String");
        CHECK(type_string.size() == 32);

        CHECK(type_buffer.name() == "Buffer");
        CHECK(type_buffer.size() == 16);

        CHECK(type_undef.name() == "Undefined");
        CHECK(type_undef.size() == 0);
    }
}
