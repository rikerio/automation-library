#include <catch2/catch_test_macros.hpp>
#include <unistd.h>
#include "utils/scoped-cyclic-storage.h"
#include "utils/temp-folder.h"
#include "cyclic/link.h"

using namespace automation;

SCENARIO("cyclic::Link")
{
    TemporaryFolder temp_folder("cyclic_data_storage");

    GIVEN("No Storage")
    {

        WHEN("assembling a link converter")
        {
            cyclic::LinkConverter converter { "test", 1.0f, 2.0f };
            THEN("it should contain the data")
            {
                CHECK(converter.name == "test");
                REQUIRE(converter.args.size() == 2);
            }
        }

        WHEN("assembling a link option")
        {
            cyclic::LinkOptions options { 0, "test", 0.0f, 10.0f };

            THEN("it should contain everything")
            {
                CHECK(options.offset.offset == 0);
                CHECK(options.offset.index == 0);
                REQUIRE(options.converter.has_value() == true);
                CHECK(options.converter->name == "test");
                REQUIRE(options.converter->args.size() == 2);
            }
        }

        WHEN("storing links on the filesystem")
        {
            std::filesystem::path path = temp_folder.path() / "1";
            cyclic::LinkList link_list;

            link_list.emplace_back("a");
            link_list.emplace_back("b", 16);
            link_list.emplace_back("c", cyclic::LinkOptions { 0, "test", 0.0f, 10.0f });

            CHECK(cyclic::LinkList::serialize(path, link_list).has_value());

            THEN("this link can be read from fs")
            {
                auto link_list = cyclic::LinkList::deserialize(path);

                REQUIRE(link_list.has_value());
                REQUIRE(link_list->size() == 3);
                CHECK(link_list->at(0).name == "a");
                CHECK(link_list->at(0).options.offset.offset == 0);
                CHECK(link_list->at(0).options.offset.index == 0);
                CHECK(link_list->at(0).options.converter.has_value() == false);
                CHECK(link_list->at(1).name == "b");
                CHECK(link_list->at(1).options.offset.offset == 16);
                CHECK(link_list->at(1).options.offset.index == 0);
                CHECK(link_list->at(1).options.converter.has_value() == false);
                CHECK(link_list->at(2).name == "c");
                CHECK(link_list->at(2).options.offset.offset == 0);
                CHECK(link_list->at(2).options.offset.index == 0);
                REQUIRE(link_list->at(2).options.converter.has_value() == true);
                CHECK(link_list->at(2).options.converter->name == "test");
                REQUIRE(link_list->at(2).options.converter->args.size() == 2);
                CHECK(link_list->at(2).options.converter->args[0] == 0.0f);
                CHECK(link_list->at(2).options.converter->args[1] == 10.0f);
            }

            THEN("non existing files create empty links")
            {
                auto link_list = cyclic::LinkList::deserialize(temp_folder.path() / "2");

                REQUIRE(link_list.has_value());
                CHECK(link_list->size() == 0);
            }
        }
    }

    GIVEN("A storage")
    {
        ScopedStorage scoped_storage(temp_folder.path(), 1024);
        auto storage = cyclic::Storage::attach(scoped_storage.path());

        REQUIRE(storage.has_value());

        WHEN("removing a link")
        {
            CHECK(storage->link("1", "a").has_value());
            CHECK(storage->link("1", "b", 16).has_value());
            CHECK(storage->link("1", "a").has_value());

            CHECK(storage->link("1")->size() == 2);
            CHECK(storage->unlink("1", "b", 16).has_value());
            CHECK(storage->link("1")->size() == 1);

            THEN("the data is not found in the link")
            {
                auto link = storage->link("1");

                REQUIRE(link.has_value());
                REQUIRE(link->size() == 1);
                CHECK(link->at(0).name == "a");
                CHECK(link->at(0).options.offset.offset == 0);
                CHECK(link->at(0).options.offset.index == 0);
            }

            THEN("removing all data removes the link too")
            {
                storage->unlink("1");
                auto res = storage->link("1");
                REQUIRE(res.has_value());
                CHECK(res->size() == 0);
            }
        }
    }
}
