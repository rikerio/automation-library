#include <catch2/catch_test_macros.hpp>

#include "mmu/profile.h"
#include "mmu/process-object.h"
#include "rikerio/data-provider.h"

using namespace automation;

TEST_CASE("MMU Data Provider")
{

    static unsigned int id = 0;
    const std::string name = "mmu-data-provider-" + std::to_string(::getpid()) + "-" + std::to_string(id++);

    CHECK_NOTHROW(mmu::Profile::create(name, 512));

    SECTION("Create Unique")
    {
        mmu::Profile profile(name);

        rikerio::DataProvider provider(profile, "alloc");

        provider.create<uint32_t>("data.1")
            .create<uint64_t>("data.2");

        CHECK(mmu::ProcessObject().load(profile, "data.1").is_attached() == false);
        CHECK(mmu::ProcessObject().load(profile, "data.2").is_attached() == false);

        provider.init();

        CHECK(mmu::ProcessObject().load(profile, "data.1").is_attached() == true);
        CHECK(mmu::ProcessObject().load(profile, "data.1").byte_offset() == 0);
        CHECK(mmu::ProcessObject().load(profile, "data.2").is_attached() == true);
        CHECK(mmu::ProcessObject().load(profile, "data.2").byte_offset() == 4);

        provider.quit();

        CHECK(mmu::ProcessObject().load(profile, "data.1").is_attached() == false);
        CHECK(mmu::ProcessObject().load(profile, "data.2").is_attached() == false);
    }

    SECTION("Create Non Unique")
    {
        mmu::Profile profile(name);

        rikerio::DataProvider provider(profile, "alloc");

        provider.create<uint32_t>("data.1");
        CHECK_THROWS(provider.create<uint64_t>("data.1"));
    }

    CHECK_NOTHROW(mmu::Profile::remove(name));
}
