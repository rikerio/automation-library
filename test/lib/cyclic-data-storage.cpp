#include <catch2/catch_test_macros.hpp>
#include <unistd.h>

#include "utils/scoped-cyclic-storage.h"
#include "cyclic/storage.h"
#include "cyclic/memory-segment.h"
#include "cyclic/meta.h"
#include "utils/temp-folder.h"

using namespace automation;

SCENARIO("cyclic::Storage")
{
    TemporaryFolder temp_folder("cyclic_data_storage");

    GIVEN("No existing Storage")
    {
        WHEN("Attaching to a Storage")
        {
            auto exp_storage = cyclic::Storage::attach(temp_folder.path());
            THEN("it is not valid")
            {
                CHECK(!exp_storage.has_value());
            }
        }
    }

    GIVEN("A newly created storage")
    {
        auto storage_exp = cyclic::Storage::create(temp_folder.path(), 1024);
        REQUIRE(storage_exp.has_value());

        auto& storage = storage_exp.value();
        auto shm_path = std::filesystem::path("/dev/shm") / std::string("rikerio-shm-" + storage.shm_name());

        WHEN("created")
        {
            auto storage = cyclic::Storage::create(temp_folder.path(), 1024);
            CHECK(storage.has_value() == false);
            CHECK(cyclic::Storage::remove(temp_folder.path(), true));
        }

        WHEN("removed and not purged")
        {
            REQUIRE(cyclic::Storage::remove(temp_folder.path(), false));

            THEN("every temporary asset is removed")
            {
                CHECK(std::filesystem::exists(storage.root_path()));
                CHECK(std::filesystem::exists(storage.data_path()) == false);
                CHECK(std::filesystem::exists(storage.link_path()));
                CHECK(std::filesystem::exists(storage.lock_path()));
            }
        }

        WHEN("purged a reused storage")
        {
            REQUIRE(cyclic::Storage::remove(temp_folder.path(), true));

            THEN("every asset is removed")
            {
                CHECK(std::filesystem::exists(storage.root_path()) == false);
            }
        }

        WHEN("allocating meta data")
        {

            auto meta = storage.allocate<uint32_t>("test");

            THEN("meta data is valid")
            {
                REQUIRE(meta.has_value());
                CHECK(storage.deallocate(meta->handle()).has_value());
            }
        }

        WHEN("allocating buffer meta data")
        {
            auto meta = storage.allocate("test", 32);

            THEN("meta data is valid")
            {
                CHECK(meta.has_value());
            }

            AND_THEN("deallocating is fine")
            {
                CHECK(storage.deallocate(meta->handle()).has_value());
            }
        }

        WHEN("allocating memory segment")
        {
            auto ptr = storage.allocate(32);

            THEN("meta data is valid")
            {
                CHECK(*ptr != nullptr);
            }

            AND_THEN("deallocating is fine")
            {
                CHECK(storage.deallocate(*ptr).has_value());
            }
        }
    }
}
