#include <catch2/catch_test_macros.hpp>
#include "utils/bitstring.h"

using namespace automation::utils;

TEST_CASE("BitString")
{
    SECTION("Malformated")
    {
        CHECK_THROWS(BitString::from_string("abc"));
        CHECK_THROWS(BitString::from_string("0,0"));
        CHECK_THROWS(BitString::from_string("   "));
        CHECK_THROWS(BitString::from_string("1.8"));
        CHECK_NOTHROW(BitString::from_string("-3.7"));
    }

    SECTION("Interpreted")
    {
        CHECK(BitString::from_string("0.0").equals({}));
        CHECK(BitString::from_string("-0.0").equals({}));
        CHECK(BitString::from_string("0.1").equals({ 0, 1 }));
        CHECK(BitString::from_string("1").equals({ 1, 0 }));
        CHECK(BitString::from_string("1.1").equals({ 1, 1 }));
        CHECK(BitString::from_string("64.3").equals({ 64, 3 }));
    }
}
