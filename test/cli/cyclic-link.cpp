#include <catch2/catch_test_macros.hpp>

#include "application.h"
#include "cyclic/storage.h"
#include "cyclic/link.h"
#include "utils/reset-env.h"
#include "utils/scoped-application.h"
#include "utils/build.h"
#include <fmt/core.h>
#include <boost/process.hpp>
#include <boost/asio.hpp>

using namespace automation;
namespace bp = boost::process;
namespace bio = boost::asio;

SCENARIO("CLI memory link")
{
    bio::io_service service;
    std::future<std::string> capture_stdout;
    GIVEN("NO application given")
    {
        WHEN("trying to get a link")
        {
            auto process = bp::child {
                bp::exe = build_dir("src/cli/rio").string(),
                bp::args = { "cyclic", "link", "get", "unexisting" },
                bp::std_out > capture_stdout,
                service
            };

            service.run();
            process.wait();

            THEN("the process fails")
            {
                CHECK(process.running() == false);
                CHECK(process.native_exit_code() != EXIT_SUCCESS);
                CHECK(capture_stdout.get() == "Application 'default' not found\n");
            }
        }
    }

    GIVEN("A application given")
    {
        ResetEnv reset_env({ "XDG_RUNTIME_DIR" });

        ScopedApplication scoped_app("memory-link", { 1024 });

        WHEN("rio cyclic link inspect")
        {
            auto application = Application::attach(scoped_app.name());

            REQUIRE(application.has_value());

            auto storage = cyclic::Storage::attach(application->cyclic_data_path());

            REQUIRE(storage.has_value());

            THEN("non existing link id")
            {
                auto process = bp::child {
                    bp::exe = build_dir("src/cli/rio").string(),
                    bp::args = { "cyclic", "link", "get", "unexisting" },
                    bp::env["RIO_APPLICATION"] = scoped_app.name(),
                    bp::std_out > capture_stdout,
                    service
                };

                service.run();
                process.wait();
                CHECK(process.running() == false);
                CHECK(process.native_exit_code() == EXIT_SUCCESS);
                CHECK(capture_stdout.get() == "");
            }

            THEN("existing link id")
            {
                const std::string data_name = "dataname";
                const std::string link_name = "linkname";

                CHECK(storage->link(link_name, data_name).has_value());

                auto process = bp::child {
                    bp::exe = build_dir("src/cli/rio").string(),
                    bp::args = { "cyclic", "link", "get", link_name },
                    bp::env["RIO_APPLICATION"] = scoped_app.name(),
                    bp::std_out > capture_stdout,
                    service
                };

                service.run();
                process.wait();
                CHECK(process.running() == false);
                CHECK(process.native_exit_code() == EXIT_SUCCESS);
                CHECK(capture_stdout.get() == data_name + "+0.0" + "\n");
            }
        }

        WHEN("rio link list")
        {
            auto application = Application::attach(scoped_app.name());

            REQUIRE(application.has_value());

            auto storage = cyclic::Storage::attach(application->cyclic_data_path());

            REQUIRE(storage.has_value());

            THEN("no link")
            {
                auto process = bp::child {
                    bp::exe = build_dir("src/cli/rio").string(),
                    bp::args = { "cyclic", "link", "list" },
                    bp::env["RIO_APPLICATION"] = scoped_app.name(),
                    bp::std_out > capture_stdout,
                    service
                };

                service.run();
                process.wait();
                CHECK(process.running() == false);
                CHECK(process.native_exit_code() == EXIT_SUCCESS);
                CHECK(capture_stdout.get() == "");
            }

            THEN("some links")
            {
                CHECK(storage->link("a", "1").has_value());
                CHECK(storage->link("b", "2").has_value());

                auto process = bp::child {
                    bp::exe = build_dir("src/cli/rio").string(),
                    bp::args = { "cyclic", "link", "list" },
                    bp::env["RIO_APPLICATION"] = scoped_app.name(),
                    bp::std_out > capture_stdout,
                    service
                };

                service.run();
                process.wait();
                CHECK(process.running() == false);
                CHECK(process.native_exit_code() == EXIT_SUCCESS);
                CHECK(capture_stdout.get() == "a\nb\n");
            }
        }

        WHEN("rio link add")
        {
            auto application = Application::attach(scoped_app.name());

            REQUIRE(application.has_value());

            auto storage = cyclic::Storage::attach(application->cyclic_data_path());

            REQUIRE(storage.has_value());

            THEN("missing link argument")
            {
                auto process = bp::child {
                    bp::exe = build_dir("src/cli/rio").string(),
                    bp::args = { "cyclic", "link", "add" },
                    bp::env["RIO_APPLICATION"] = scoped_app.name(),
                    bp::std_out > capture_stdout,
                    service
                };

                service.run();
                process.wait();
                CHECK(process.running() == false);
                CHECK(process.native_exit_code() != EXIT_SUCCESS);
                CHECK(capture_stdout.get() == "");
            }

            THEN("missing data argument")
            {
                const std::string link_name = "linkname";

                auto process = bp::child {
                    bp::exe = build_dir("src/cli/rio").string(),
                    bp::args = { "cyclic", "link", "add", link_name },
                    bp::env["RIO_APPLICATION"] = scoped_app.name(),
                    bp::std_out > capture_stdout,
                    service
                };

                service.run();
                process.wait();
                CHECK(process.running() == false);
                CHECK(process.native_exit_code() != EXIT_SUCCESS);
                CHECK(capture_stdout.get() == "");
            }

            THEN("one data name")
            {
                auto process_a = bp::child {
                    bp::exe = build_dir("src/cli/rio").string(),
                    bp::args = { "cyclic", "link", "set", "a", "1" },
                    bp::env["RIO_APPLICATION"] = scoped_app.name(),
                    bp::std_out > capture_stdout,
                    service
                };

                service.run();
                process_a.wait();
                CHECK(process_a.running() == false);
                CHECK(process_a.native_exit_code() == EXIT_SUCCESS);

                service.reset();

                auto process_b = bp::child {
                    bp::exe = build_dir("src/cli/rio").string(),
                    bp::args = { "cyclic", "link", "set", "a", "2", "--offset=2" },
                    bp::env["RIO_APPLICATION"] = scoped_app.name(),
                    bp::std_out > capture_stdout,
                    service
                };

                service.run();
                process_b.wait();
                CHECK(process_b.running() == false);
                CHECK(process_b.native_exit_code() == EXIT_SUCCESS);

                service.reset();

                auto process_c = bp::child {
                    bp::exe = build_dir("src/cli/rio").string(),
                    bp::args = { "cyclic", "link", "set", "a", "3", "--convert=test;0.0;10.0;" },
                    bp::env["RIO_APPLICATION"] = scoped_app.name(),
                    bp::std_out > capture_stdout,
                    service
                };

                service.run();
                process_c.wait();
                CHECK(process_c.running() == false);
                CHECK(process_c.native_exit_code() == EXIT_SUCCESS);
                CHECK(capture_stdout.get() == "");
                service.reset();

                auto link_res = storage->link("a");
                auto& link = link_res.value();

                REQUIRE(link_res.has_value());
                REQUIRE(link.size() == 3);
                CHECK(link.at(0).name == "1");
                CHECK(link.at(0).options.offset.offset == 0);
                CHECK(link.at(0).options.offset.index == 0);
                CHECK(link.at(0).options.converter.has_value() == false);
                CHECK(link.at(1).name == "2");
                CHECK(link.at(1).options.offset.offset == 2);
                CHECK(link.at(1).options.offset.index == 0);
                CHECK(link.at(1).options.converter.has_value() == false);
                CHECK(link.at(2).name == "3");
                CHECK(link.at(2).options.offset.offset == 0);
                CHECK(link.at(2).options.offset.index == 0);
                REQUIRE(link.at(2).options.converter.has_value() == true);
                CHECK(link.at(2).options.converter->name == "test");
                REQUIRE(link.at(2).options.converter->args.size() == 2);
                CHECK(link.at(2).options.converter->args[0] == 0.0f);
                CHECK(link.at(2).options.converter->args[1] == 10.0f);
            }
        }

        WHEN("rio mmu link rm")
        {
            auto application = Application::attach(scoped_app.name());

            REQUIRE(application.has_value());

            auto storage = cyclic::Storage::attach(application->cyclic_data_path());

            REQUIRE(storage.has_value());

            THEN("missing link argument")
            {
                auto process = bp::child {
                    bp::exe = build_dir("src/cli/rio").string(),
                    bp::args = { "cyclic", "link", "rm" },
                    bp::env["RIO_APPLICATION"] = scoped_app.name(),
                    bp::std_out > capture_stdout,
                    service
                };

                service.run();
                process.wait();
                CHECK(process.running() == false);
                CHECK(process.native_exit_code() != EXIT_SUCCESS);
                CHECK(capture_stdout.get() == "");
            }

            THEN("no data name")
            {
                CHECK(storage->link("a", "1").has_value());
                CHECK(storage->link("b", "2").has_value());

                auto process = bp::child {
                    bp::exe = build_dir("src/cli/rio").string(),
                    bp::args = { "cyclic", "link", "remove", "a" },
                    bp::env["RIO_APPLICATION"] = scoped_app.name(),
                    bp::std_out > capture_stdout,
                    service
                };

                service.run();
                process.wait();
                CHECK(process.running() == false);
                CHECK(process.native_exit_code() == EXIT_SUCCESS);
                CHECK(capture_stdout.get() == "");

                auto link_res = storage->link("a");

                REQUIRE(link_res.has_value());
                CHECK(link_res.value().size() == 0);
            }

            THEN("one data name")
            {
                CHECK(storage->link("a", "1").has_value());
                CHECK(storage->link("b", "2").has_value());

                auto process = bp::child {
                    bp::exe = build_dir("src/cli/rio").string(),
                    bp::args = { "cyclic", "link", "remove", "a", "2" },
                    bp::env["RIO_APPLICATION"] = scoped_app.name(),
                    bp::std_out > capture_stdout,
                    service
                };

                service.run();
                process.wait();
                CHECK(process.running() == false);
                CHECK(process.native_exit_code() == EXIT_SUCCESS);
                CHECK(capture_stdout.get() == "");

                auto link_res = storage->link("a");

                REQUIRE(link_res.has_value());
                REQUIRE(link_res.value().size() == 1);
                CHECK(link_res.value().at(0).name == "1");
                CHECK(link_res.value().at(0).options.offset.offset == 0);
                CHECK(link_res.value().at(0).options.offset.index == 0);
            }
        }
    }
}
