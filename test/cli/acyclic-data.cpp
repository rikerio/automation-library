#include <catch2/catch_test_macros.hpp>

#include <boost/asio.hpp>
#include <boost/process.hpp>
#include "utils/scoped-application.h"
#include "utils/reset-env.h"
#include "utils/build.h"
#include "acyclic/storage.h"

using namespace automation;
namespace bp = boost::process;

SCENARIO("CLI acyclic data")
{
    ResetEnv reset_env({ "XDG_DATA_DIR" });
    boost::asio::io_service service;
    std::future<std::string> stdout_capture;

    GIVEN("no application")
    {
        WHEN("rio data inspect")
        {
            auto process = bp::child {
                bp::exe = build_dir("src/cli/rio").string(),
                bp::args = { "acyclic", "data", "get", "unexisting" },
                bp::std_out > stdout_capture,
                service
            };

            service.run();
            process.wait();
            THEN("it fails")
            {
                CHECK(process.running() == false);
                CHECK(process.native_exit_code() != EXIT_SUCCESS);
            }
        }
    }

    GIVEN("an application")
    {
        ScopedApplication scoped_app("rio_acyclic_data", { 1024 });
        auto application = Application::attach(scoped_app.name());

        REQUIRE(application);

        auto storage = acyclic::Storage::attach(application->acyclic_data_path());

        REQUIRE(storage.has_value());

        WHEN("rio acylic data list")
        {
            THEN("no data")
            {
                auto process = bp::child {
                    bp::exe = build_dir("src/cli/rio").string(),
                    bp::args = { "acyclic", "data", "list" },
                    bp::env["RIO_APPLICATION]"] = scoped_app.name(),
                    bp::std_out > stdout_capture,
                    service
                };

                service.run();
                process.wait();
                CHECK(process.running() == false);
                CHECK(process.native_exit_code() != EXIT_SUCCESS);
                CHECK(stdout_capture.get() == "Application 'default' not found\n");
            }

            THEN("some data")
            {
                auto meta_1 = acyclic::Data::create<uint32_t>(0);
                auto meta_2 = acyclic::Data::create<uint16_t>(0);
                storage->store("data.1", meta_1);
                storage->store("data.2", meta_2);

                auto process = bp::child {
                    bp::exe = build_dir("src/cli/rio").string(),
                    bp::args = { "acyclic", "data", "list" },
                    bp::env["RIO_APPLICATION"] = scoped_app.name(),
                    bp::std_out > stdout_capture,
                    service
                };

                service.run();
                process.wait();
                CHECK(process.running() == false);
                CHECK(process.native_exit_code() == EXIT_SUCCESS);
                CHECK(stdout_capture.get() == "\"data.1\"\n\"data.2\"\n");
            }
        }

        WHEN("rio acyclic data get")
        {
            THEN("no data")
            {
                auto process = bp::child(
                    bp::exe = build_dir("src/cli/rio").string(),
                    bp::args = { "acyclic", "data", "get" },
                    bp::env["RIO_APPLICATION"] = scoped_app.name(),
                    bp::std_out > stdout_capture,
                    service);

                service.run();
                process.wait();
                CHECK(process.running() == false);
                CHECK(process.native_exit_code() != EXIT_SUCCESS);
                CHECK(stdout_capture.get() == "");
            }

            THEN("some data (bool)")
            {
                auto meta = acyclic::Data::create<bool>(true);
                storage->store("data.1", meta);

                auto process = bp::child(
                    bp::exe = build_dir("src/cli/rio").string(),
                    bp::args = { "acyclic", "data", "get", "data.1" },
                    bp::env["RIO_APPLICATION"] = scoped_app.name(),
                    bp::std_out > stdout_capture,
                    service);

                service.run();
                process.wait();
                CHECK(process.running() == false);
                CHECK(process.native_exit_code() == EXIT_SUCCESS);
                CHECK(stdout_capture.get() == "data.1,Bool,1\n");
            }

            THEN("some data (uint8)")
            {
                auto meta = acyclic::Data::create<uint8_t>(42);
                storage->store("data.1", meta);

                auto process = bp::child {
                    bp::exe = build_dir("src/cli/rio").string(),
                    bp::args = { "acyclic", "data", "get", "data.1" },
                    bp::env["RIO_APPLICATION"] = scoped_app.name(),
                    bp::std_out > stdout_capture,
                    service
                };

                service.run();
                process.wait();
                CHECK(process.running() == false);
                CHECK(process.native_exit_code() == EXIT_SUCCESS);
                CHECK(stdout_capture.get() == "data.1,Uint8,42\n");
            }

            THEN("some data (int8)")
            {
                auto meta = acyclic::Data::create<int8_t>(-42);
                storage->store("data.1", meta);

                auto process = bp::child {
                    bp::exe = build_dir("src/cli/rio").string(),
                    bp::args = { "acyclic", "data", "get", "data.1" },
                    bp::env["RIO_APPLICATION"] = scoped_app.name(),
                    bp::std_out > stdout_capture,
                    service
                };

                service.run();
                process.wait();
                CHECK(process.running() == false);
                CHECK(process.native_exit_code() == EXIT_SUCCESS);
                CHECK(stdout_capture.get() == "data.1,Int8,-42\n");
            }

            THEN("some data (uint16)")
            {
                auto meta = acyclic::Data::create<uint16_t>(42);
                storage->store("data.1", meta);

                auto process = bp::child {
                    bp::exe = build_dir("src/cli/rio").string(),
                    bp::args = { "acyclic", "data", "get", "data.1" },
                    bp::env["RIO_APPLICATION"] = scoped_app.name(),
                    bp::std_out > stdout_capture,
                    service
                };

                service.run();
                process.wait();
                CHECK(process.running() == false);
                CHECK(process.native_exit_code() == EXIT_SUCCESS);
                CHECK(stdout_capture.get() == "data.1,Uint16,42\n");
            }

            THEN("some data (int16)")
            {
                auto meta = acyclic::Data::create<int16_t>(-42);
                storage->store("data.1", meta);

                auto process = bp::child {
                    bp::exe = build_dir("src/cli/rio").string(),
                    bp::args = { "acyclic", "data", "get", "data.1" },
                    bp::env["RIO_APPLICATION"] = scoped_app.name(),
                    bp::std_out > stdout_capture,
                    service
                };

                service.run();
                process.wait();
                CHECK(process.running() == false);
                CHECK(process.native_exit_code() == EXIT_SUCCESS);
                CHECK(stdout_capture.get() == "data.1,Int16,-42\n");
            }

            THEN("some data (uint32_t)")
            {
                auto meta = acyclic::Data::create<uint32_t>(42);
                storage->store("data.1", meta);

                auto process = bp::child {
                    bp::exe = build_dir("src/cli/rio").string(),
                    bp::args = { "acyclic", "data", "get", "data.1" },
                    bp::env["RIO_APPLICATION"] = scoped_app.name(),
                    bp::std_out > stdout_capture,
                    service
                };

                service.run();
                process.wait();
                CHECK(process.running() == false);
                CHECK(process.native_exit_code() == EXIT_SUCCESS);
                CHECK(stdout_capture.get() == "data.1,Uint32,42\n");
            }

            THEN("some data (int32_t)")
            {
                auto meta = acyclic::Data::create<int32_t>(-42000);
                storage->store("data.1", meta);

                auto process = bp::child {
                    bp::exe = build_dir("src/cli/rio").string(),
                    bp::args = { "acyclic", "data", "get", "data.1" },
                    bp::env["RIO_APPLICATION"] = scoped_app.name(),
                    bp::std_out > stdout_capture,
                    service
                };

                service.run();
                process.wait();
                CHECK(process.running() == false);
                CHECK(process.native_exit_code() == EXIT_SUCCESS);
                CHECK(stdout_capture.get() == "data.1,Int32,-42000\n");
            }

            THEN("some data (uint64_t)")
            {
                auto meta = acyclic::Data::create<uint64_t>(42000000);
                storage->store("data.1", meta);

                auto process = bp::child {
                    bp::exe = build_dir("src/cli/rio").string(),
                    bp::args = { "acyclic", "data", "get", "data.1" },
                    bp::env["RIO_APPLICATION"] = scoped_app.name(),
                    bp::std_out > stdout_capture,
                    service
                };

                service.run();
                process.wait();
                CHECK(process.running() == false);
                CHECK(process.native_exit_code() == EXIT_SUCCESS);
                CHECK(stdout_capture.get() == "data.1,Uint64,42000000\n");
            }

            THEN("some data (int64_t)")
            {
                auto meta = acyclic::Data::create<int64_t>(-42000000);
                storage->store("data.1", meta);

                auto process = bp::child {
                    bp::exe = build_dir("src/cli/rio").string(),
                    bp::args = { "acyclic", "data", "get", "data.1" },
                    bp::env["RIO_APPLICATION"] = scoped_app.name(),
                    bp::std_out > stdout_capture,
                    service
                };

                service.run();
                process.wait();
                CHECK(process.running() == false);
                CHECK(process.native_exit_code() == EXIT_SUCCESS);
                CHECK(stdout_capture.get() == "data.1,Int64,-42000000\n");
            }

            THEN("some data (float)")
            {
                auto meta = acyclic::Data::create<float>(0.42);
                storage->store("data.1", meta);

                auto process = bp::child {
                    bp::exe = build_dir("src/cli/rio").string(),
                    bp::args = { "acyclic", "data", "get", "data.1" },
                    bp::env["RIO_APPLICATION"] = scoped_app.name(),
                    bp::std_out > stdout_capture,
                    service
                };

                service.run();
                process.wait();
                CHECK(process.running() == false);
                CHECK(process.native_exit_code() == EXIT_SUCCESS);
                CHECK(stdout_capture.get() == "data.1,Float,0.420000\n");
            }

            THEN("some data (double)")
            {
                auto meta = acyclic::Data::create<double>(-0.42);
                storage->store("data.1", meta);

                auto process = bp::child {
                    bp::exe = build_dir("src/cli/rio").string(),
                    bp::args = { "acyclic", "data", "get", "data.1" },
                    bp::env["RIO_APPLICATION"] = scoped_app.name(),
                    bp::std_out > stdout_capture,
                    service
                };

                service.run();
                process.wait();
                CHECK(process.running() == false);
                CHECK(process.native_exit_code() == EXIT_SUCCESS);
                CHECK(stdout_capture.get() == "data.1,Double,-0.420000\n");
            }

            THEN("string data")
            {
                auto meta = acyclic::Data::create("Hastalavista");
                storage->store("data.1", meta);

                auto process = bp::child {
                    bp::exe = build_dir("src/cli/rio").string(),
                    bp::args = { "acyclic", "data", "get", "data.1" },
                    bp::env["RIO_APPLICATION"] = scoped_app.name(),
                    bp::std_out > stdout_capture,
                    service
                };

                service.run();
                process.wait();
                CHECK(process.running() == false);
                CHECK(process.native_exit_code() == EXIT_SUCCESS);
                CHECK(stdout_capture.get() == "data.1,String,Hastalavista\n");
            }
        }

        WHEN("rio acyclic data set")
        {

            THEN("some data bool")
            {
                auto process = bp::child {
                    bp::exe = build_dir("src/cli/rio").string(),
                    bp::args = { "acyclic", "data", "set", "data.2", "1", "--type=Bool" },
                    bp::env["RIO_APPLICATION"] = scoped_app.name(),
                    bp::std_out > stdout_capture,
                    service
                };

                service.run();
                process.wait();
                CHECK(process.running() == false);
                CHECK(process.native_exit_code() == EXIT_SUCCESS);
                CHECK(stdout_capture.get() == "");

                bool comp_data = true;
                auto data = storage->load("data.2");

                REQUIRE(data.has_value());
                REQUIRE(data->type().name() == "Bool");
                CHECK(std::memcmp(&comp_data, data->data(), sizeof(comp_data)) == 0);
            }

            THEN("some data uint8")
            {
                using type = uint8_t;
                auto process = bp::child {
                    bp::exe = build_dir("src/cli/rio").string(),
                    bp::args = { "acyclic", "data", "set", "data.2", "42", "--type=Uint8" },
                    bp::env["RIO_APPLICATION"] = scoped_app.name(),
                    bp::std_out > stdout_capture,
                    service
                };

                service.run();
                process.wait();
                CHECK(process.running() == false);
                CHECK(process.native_exit_code() == EXIT_SUCCESS);
                CHECK(stdout_capture.get() == "");

                type comp_data = 42;
                auto data = storage->load("data.2");

                REQUIRE(data.has_value());
                REQUIRE(data->type().name() == "Uint8");
                CHECK(std::memcmp(&comp_data, data->data(), sizeof(type)) == 0);
            }

            THEN("some data int8")
            {
                using type = int8_t;
                auto process = bp::child {
                    bp::exe = build_dir("src/cli/rio").string(),
                    bp::args = { "acyclic", "data", "set", "data.2", "-42", "--type=Int8" },
                    bp::env["RIO_APPLICATION"] = scoped_app.name(),
                    bp::std_out > stdout_capture,
                    service
                };

                service.run();
                process.wait();
                CHECK(process.running() == false);
                CHECK(process.native_exit_code() == EXIT_SUCCESS);
                CHECK(stdout_capture.get() == "");

                type comp_data = -42;
                auto data = storage->load("data.2");

                REQUIRE(data.has_value());
                REQUIRE(data->type().name() == "Int8");
                CHECK(std::memcmp(&comp_data, data->data(), sizeof(type)) == 0);
            }

            THEN("some data uint16")
            {
                using type = uint16_t;
                auto process = bp::child {
                    bp::exe = build_dir("src/cli/rio").string(),
                    bp::args = { "acyclic", "data", "set", "data.2", "42", "--type=Uint16" },
                    bp::env["RIO_APPLICATION"] = scoped_app.name(),
                    bp::std_out > stdout_capture,
                    service
                };

                service.run();
                process.wait();
                CHECK(process.running() == false);
                CHECK(process.native_exit_code() == EXIT_SUCCESS);
                CHECK(stdout_capture.get() == "");

                type comp_data = 42;
                auto data = storage->load("data.2");

                REQUIRE(data.has_value());
                REQUIRE(data->type().name() == "Uint16");
                CHECK(std::memcmp(&comp_data, data->data(), sizeof(type)) == 0);
            }

            THEN("some data int16")
            {
                using type = int16_t;
                auto process = bp::child {
                    bp::exe = build_dir("src/cli/rio").string(),
                    bp::args = { "acyclic", "data", "set", "data.2", "-42", "--type=Int16" },
                    bp::env["RIO_APPLICATION"] = scoped_app.name(),
                    bp::std_out > stdout_capture,
                    service
                };

                service.run();
                process.wait();
                CHECK(process.running() == false);
                CHECK(process.native_exit_code() == EXIT_SUCCESS);
                CHECK(stdout_capture.get() == "");

                type comp_data = -42;
                auto data = storage->load("data.2");

                REQUIRE(data.has_value());
                REQUIRE(data->type().name() == "Int16");
                CHECK(std::memcmp(&comp_data, data->data(), sizeof(type)) == 0);
            }

            THEN("some data uint32")
            {
                using type = uint32_t;
                auto process = bp::child {
                    bp::exe = build_dir("src/cli/rio").string(),
                    bp::args = { "acyclic", "data", "set", "data.2", "42", "--type=Uint32" },
                    bp::env["RIO_APPLICATION"] = scoped_app.name(),
                    bp::std_out > stdout_capture,
                    service
                };

                service.run();
                process.wait();
                CHECK(process.running() == false);
                CHECK(process.native_exit_code() == EXIT_SUCCESS);
                CHECK(stdout_capture.get() == "");

                type comp_data = 42;
                auto data = storage->load("data.2");

                REQUIRE(data.has_value());
                REQUIRE(data->type().name() == "Uint32");
                CHECK(std::memcmp(&comp_data, data->data(), sizeof(type)) == 0);
            }

            THEN("some data int32")
            {
                using type = int32_t;
                auto process = bp::child {
                    bp::exe = build_dir("src/cli/rio").string(),
                    bp::args = { "acyclic", "data", "set", "data.2", "-42", "--type=Int32" },
                    bp::env["RIO_APPLICATION"] = scoped_app.name(),
                    bp::std_out > stdout_capture,
                    service
                };

                service.run();
                process.wait();
                CHECK(process.running() == false);
                CHECK(process.native_exit_code() == EXIT_SUCCESS);
                CHECK(stdout_capture.get() == "");

                type comp_data = -42;
                auto data = storage->load("data.2");

                REQUIRE(data.has_value());
                REQUIRE(data->type().name() == "Int32");
                CHECK(std::memcmp(&comp_data, data->data(), sizeof(type)) == 0);
            }

            THEN("some data uint64")
            {
                using type = uint64_t;
                auto process = bp::child {
                    bp::exe = build_dir("src/cli/rio").string(),
                    bp::args = { "acyclic", "data", "set", "data.2", "42", "--type=Uint64" },
                    bp::env["RIO_APPLICATION"] = scoped_app.name(),
                    bp::std_out > stdout_capture,
                    service
                };

                service.run();
                process.wait();
                CHECK(process.running() == false);
                CHECK(process.native_exit_code() == EXIT_SUCCESS);
                CHECK(stdout_capture.get() == "");

                type comp_data = 42;
                auto data = storage->load("data.2");

                REQUIRE(data.has_value());
                REQUIRE(data->type().name() == "Uint64");
                CHECK(std::memcmp(&comp_data, data->data(), sizeof(type)) == 0);
            }

            THEN("some data int64")
            {
                using type = int64_t;
                auto process = bp::child {
                    bp::exe = build_dir("src/cli/rio").string(),
                    bp::args = { "acyclic", "data", "set", "data.2", "-42", "--type=Int64" },
                    bp::env["RIO_APPLICATION"] = scoped_app.name(),
                    bp::std_out > stdout_capture, service
                };

                service.run();
                process.wait();
                CHECK(process.running() == false);
                CHECK(process.native_exit_code() == EXIT_SUCCESS);
                CHECK(stdout_capture.get() == "");

                type comp_data = -42;
                auto data = storage->load("data.2");

                REQUIRE(data.has_value());
                REQUIRE(data->type().name() == "Int64");
                CHECK(std::memcmp(&comp_data, data->data(), sizeof(type)) == 0);
            }

            THEN("some data float")
            {
                using type = float;
                auto process = bp::child {
                    bp::exe = build_dir("src/cli/rio").string(),
                    bp::args = { "acyclic", "data", "set", "data.2", "0.42", "--type=Float" },
                    bp::env["RIO_APPLICATION"] = scoped_app.name(),
                    bp::std_out > stdout_capture,
                    service
                };

                service.run();
                process.wait();
                CHECK(process.running() == false);
                CHECK(process.native_exit_code() == EXIT_SUCCESS);
                CHECK(stdout_capture.get() == "");

                type comp_data = 0.42;
                auto data = storage->load("data.2");

                REQUIRE(data.has_value());
                REQUIRE(data->type().name() == "Float");
                CHECK(std::memcmp(&comp_data, data->data(), sizeof(type)) == 0);
            }

            THEN("some data double")
            {
                using type = double;
                auto process = bp::child {
                    bp::exe = build_dir("src/cli/rio").string(),
                    bp::args = { "acyclic", "data", "set", "data.2", "-0.42", "--type=Double" },
                    bp::env["RIO_APPLICATION"] = scoped_app.name(),
                    bp::std_out > stdout_capture,
                    service
                };

                service.run();
                process.wait();
                CHECK(process.running() == false);
                CHECK(process.native_exit_code() == EXIT_SUCCESS);
                CHECK(stdout_capture.get() == "");

                type comp_data = -0.42;
                auto data = storage->load("data.2");

                REQUIRE(data.has_value());
                REQUIRE(data->type().name() == "Double");
                CHECK(std::memcmp(&comp_data, data->data(), sizeof(type)) == 0);
            }
        }
    }
}
