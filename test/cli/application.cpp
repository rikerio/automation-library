#include <catch2/catch_test_macros.hpp>
#include <string.h>
#include <iostream>
#include <filesystem>
#include <thread>
#include <chrono>
#include <future>
#include <boost/process.hpp>

#include "utils/scoped-application.h"
#include "utils/reset-env.h"
#include "utils/build.h"
#include "cyclic/storage.h"
#include "application.h"

using namespace automation;
using namespace std::literals::chrono_literals;
namespace bp = boost::process;

TEST_CASE("CLI application")
{
    ResetEnv env({ "XDG_DATA_DIR" });

    static uint32_t id = 0;
    const std::string name = "test-" + std::to_string(getpid()) + "-" + std::to_string(id++);

    auto default_envs = bp::env["XDG_DATA_DIR"] = "";

    SECTION("rio application create")
    {
        SECTION("all defaults (failure)")
        {
            auto command = bp::child {
                bp::exe = build_dir("src/cli/rio").string(),
                bp::args = { "app", "create" }
            };

            command.wait();
            CHECK(command.running() == false);
            CHECK(command.native_exit_code() != EXIT_SUCCESS);
            CHECK(Application::exists(name) == false);
        }

        SECTION("default size")
        {
            auto command = bp::child {
                bp::exe = build_dir("src/cli/rio").string(),
                bp::args = { "app", "create", "--name=" + name },
                default_envs
            };

            command.wait();
            CHECK(command.running() == false);
            CHECK(command.native_exit_code() == EXIT_SUCCESS);

            REQUIRE(Application::exists(name));

            auto application_exp = Application::attach(name);
            auto& application = application_exp.value();
            auto storage = cyclic::Storage::attach(application.cyclic_data_path());

            REQUIRE(storage.has_value());

            namespace bip = boost::interprocess;
            auto shm = bip::managed_shared_memory(bip::open_only, storage->shm_name().c_str());

            CHECK(shm.get_size() == 4000);

            Application::remove(name, true);
        }

        SECTION("custom size")
        {
            auto command = bp::child {
                bp::exe = build_dir("src/cli/rio").string(),
                bp::args = { "app", "create", "--name=" + name, "--shm-size=2k" },
                default_envs
            };

            command.wait();

            auto application = Application::attach(name);

            REQUIRE(application.has_value());

            auto exp_storage = cyclic::Storage::attach(application->cyclic_data_path());

            REQUIRE(exp_storage.has_value());

            namespace bip = boost::interprocess;
            auto& storage = exp_storage.value();
            auto shm = bip::managed_shared_memory(bip::open_only, storage.shm_name().c_str());

            CHECK(command.running() == false);
            CHECK(command.native_exit_code() == EXIT_SUCCESS);
            CHECK(Application::exists(name));
            CHECK(shm.get_size() == 2000);

            Application::remove(name, true);
        }

        SECTION("create existing")
        {
            ScopedApplication scoped_app("application", { 2000 });

            auto command = bp::child {
                bp::exe = build_dir("src/cli/rio").string(),
                bp::args = { "app", "create", scoped_app.name(), "--shm-size=2k" },
                default_envs
            };

            command.wait();
            CHECK(command.running() == false);
            CHECK(command.native_exit_code() != EXIT_SUCCESS);
        }

        SECTION("create and keep")
        {
            auto command = bp::child {
                bp::exe = build_dir("src/cli/rio").string(),
                bp::args = { "app", "create", "--name=" + name, "--shm-size=2k", "--keep" },
                default_envs
            };

            auto killer = std::async([&]() {
                std::this_thread::sleep_for(200ms);
                ::kill(command.id(), SIGINT);
            });

            command.wait();
            CHECK(command.running() == false);
            CHECK(command.native_exit_code() == EXIT_SUCCESS);

            CHECK(Application::exists(name) == false);
        }
    }

    SECTION("rio application remove")
    {
        SECTION("missing name")
        {
            auto command = bp::child {
                bp::exe = build_dir("src/cli/rio").string(),
                bp::args = { "app", "remove" },
                default_envs
            };

            command.wait();
            CHECK(command.running() == false);
            CHECK(command.native_exit_code() != EXIT_SUCCESS);
        }

        SECTION("missing application")
        {

            auto command = bp::child {
                bp::exe = build_dir("src/cli/rio").string(),
                bp::args = { "app", "remove", "--name=abc" },
                default_envs
            };

            command.wait();
            CHECK(command.running() == false);
            CHECK(command.native_exit_code() != EXIT_SUCCESS);
        }

        SECTION("successfull")
        {
            ScopedApplication scoped_app(name, { 1024 });

            auto command = bp::child {
                bp::exe = build_dir("src/cli/rio").string(),
                bp::args = { "app", "remove", "--name", scoped_app.name() },
                default_envs
            };

            command.wait();
            CHECK(command.running() == false);
            CHECK(command.native_exit_code() == EXIT_SUCCESS);

            CHECK(Application::exists(name) == false);
        }

        SECTION("successfull purge")
        {
            ScopedApplication scoped_app("application", { 1024 });

            auto command = bp::child {
                bp::exe = build_dir("src/cli/rio").string(),
                bp::args = { "app", "remove", "--name", scoped_app.name(), "--purge" },
                default_envs
            };

            command.wait();
            CHECK(command.running() == false);
            CHECK(command.native_exit_code() == EXIT_SUCCESS);
            CHECK(std::filesystem::exists(Application::build_root_path(scoped_app.name())) == false);
        }
    }
}
