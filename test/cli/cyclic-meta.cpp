#include <catch2/catch_test_macros.hpp>

#include "utils/reset-env.h"
#include "utils/scoped-application.h"
#include "utils/build.h"
#include "cyclic/meta.h"
#include "cyclic/storage.h"
#include "application.h"
#include <iostream>
#include <boost/process.hpp>
#include <boost/asio.hpp>

using namespace automation;
namespace bio = boost::asio;
namespace bp = boost::process;

TEST_CASE("CLI memory data")
{
    ResetEnv reset_env({ "XDG_DATA_DIR" });
    bio::io_service service;
    std::future<std::string> capture_stdout;

    SECTION("Without Application")
    {
        SECTION("no such application")
        {
            auto process = bp::child {
                bp::exe = build_dir("src/cli/rio").string(),
                bp::args = { "cyclic", "meta", "inspect", "unexisting" }
            };

            process.wait();
            CHECK(process.running() == false);
            CHECK(process.native_exit_code() != EXIT_SUCCESS);
        }
    }

    SECTION("With Application")
    {
        ScopedApplication scoped_app("cyclic_meta", { 1024 });

        SECTION("rio cyclic inspect")
        {
            SECTION("non existing data id")
            {
                auto process = bp::child {
                    bp::exe = build_dir("src/cli/rio").string(),
                    bp::args = { "cyclic", "meta", "inspect", "unexisting" },
                    bp::env["RIO_APPLICATION"] = scoped_app.name()
                };

                process.wait();
                CHECK(process.running() == false);
                CHECK(process.native_exit_code() != EXIT_SUCCESS);
            }

            SECTION("existing data id")
            {
                const std::string data_name = "dataid";

                auto meta = cyclic::Meta::create<uint32_t>({ 0, 0 });
                auto storage = cyclic::Storage::attach(scoped_app.app().cyclic_data_path());

                REQUIRE(storage.has_value());
                REQUIRE(storage->store(data_name, meta).has_value());

                auto process = bp::child {
                    bp::exe = build_dir("src/cli/rio").string(),
                    bp::args = { "cyclic", "meta", "inspect", data_name },
                    bp::env["RIO_APPLICATION"] = scoped_app.name(),
                    bp::std_out > capture_stdout,
                    service
                };

                service.run();
                process.wait();
                CHECK(process.running() == false);
                CHECK(process.native_exit_code() == EXIT_SUCCESS);
                CHECK(capture_stdout.get() == data_name + ",Uint32,0.0\n");
            }
        }

        SECTION("rio memory meta list")
        {

            SECTION("no data")
            {
                auto process = bp::child {
                    bp::exe = build_dir("src/cli/rio").string(),
                    bp::args = { "cyclic", "meta", "list" },
                    bp::env["RIO_APPLICATION"] = scoped_app.name(),
                    bp::std_out > capture_stdout,
                    service
                };

                service.run();
                process.wait();
                CHECK(process.running() == false);
                CHECK(process.native_exit_code() == EXIT_SUCCESS);
                CHECK(capture_stdout.get() == "");
            }

            SECTION("some data")
            {
                auto storage = cyclic::Storage::attach(scoped_app.app().cyclic_data_path());
                REQUIRE(storage.has_value());

                CHECK(storage->allocate<uint32_t>("data.1").has_value());
                CHECK(storage->allocate<uint16_t>("data.2").has_value());

                auto process = bp::child {
                    bp::exe = build_dir("src/cli/rio").string(),
                    bp::args = { "cyclic", "meta", "list" },
                    bp::env["RIO_APPLICATION"] = scoped_app.name(),
                    bp::std_out > capture_stdout,
                    service
                };

                service.run();
                process.wait();
                CHECK(process.running() == false);
                CHECK(process.native_exit_code() == EXIT_SUCCESS);
                CHECK(capture_stdout.get() == "data.1\ndata.2\n");
            }
        }
    }
}
