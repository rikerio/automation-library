#pragma once

#include "acyclic/storage.h"
#include "utils/temp-folder.h"

using namespace automation;

class ScopedStorage {
public:
    ScopedStorage(const std::string& prefix)
        : _temp_folder(prefix)
    {
        acyclic::Storage::create(_temp_folder.path());
    }

    const std::filesystem::path& path() const
    {
        return _temp_folder.path();
    }

private:
    TemporaryFolder _temp_folder;
};
