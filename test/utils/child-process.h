#pragma once
#include <string>
#include <functional>
#include <unistd.h>
#include <wait.h>
#include <system_error>
#include <optional>
#include "scoped-pipe.h"

class CaptureOutput {
public:
    CaptureOutput()
    {
    }

    void read(ScopedPipe pipe)
    {
        ::close(pipe.write_fd());

        do {
            char tmp_char[512] = { 0 };
            size_t size = ::read(pipe.read_fd(), &tmp_char, sizeof(tmp_char));

            if (size > 0) {
                _content.append(tmp_char, size);
            } else {
                break;
            }
        } while (1);
    }

    const std::string& content() const
    {
        return _content;
    }

private:
    std::string _content = "";
};

class ChildProcess {
public:
    using ChildHandler = std::function<void()>;
    using CaptureReference = std::optional<std::reference_wrapper<CaptureOutput>>;

    ChildProcess() = default;
    ~ChildProcess();

    ChildProcess(const ChildProcess&) = delete;
    ChildProcess& operator=(const ChildProcess&) = delete;

    ChildProcess(ChildProcess&&) = default;
    ChildProcess& operator=(ChildProcess&&) = default;

    ChildProcess(ChildHandler child_handler, CaptureReference capture = std::nullopt);

    void kill(int signal = SIGTERM);
    void wait();
    bool exited() const;
    int exit_status() const;

    pid_t pid() const;

private:
    pid_t _pid = -1;
    int _child_status = -1;
    ScopedPipe _stdout_pipe;
    std::optional<std::reference_wrapper<CaptureOutput>> _capture;
};
