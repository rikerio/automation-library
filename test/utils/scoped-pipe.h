#pragma once

class ScopedPipe {
public:
    ScopedPipe(const ScopedPipe&) = delete;
    ScopedPipe& operator=(const ScopedPipe&) = delete;

    ScopedPipe(ScopedPipe&&) = default;
    ScopedPipe& operator=(ScopedPipe&&) = default;

    ScopedPipe();
    ~ScopedPipe();
    int write_fd() const;
    int read_fd() const;

private:
    int _fd[2] = { -1, -1 };
};
