#pragma once
#include "application.h"
#include <unistd.h>
#include <catch2/catch_test_macros.hpp>

using namespace automation;

namespace {
static int id = 0;
}

class ScopedApplication {
public:
    ScopedApplication(const std::string& prefix, const Application::Config& config)
        : _name { prefix + "_" + std::to_string(::getpid()) + "_" + std::to_string(id++) }
    {
        auto exp = Application::create(_name, config);

        REQUIRE(exp.has_value());
        _app = std::move(exp.value());
    }

    ~ScopedApplication()
    {
        Application::remove(_name, true);
    }

    const std::string& name() const
    {
        return _name;
    }

    Application& app()
    {
        return _app;
    }

private:
    std::string _name;
    Application _app;
};
