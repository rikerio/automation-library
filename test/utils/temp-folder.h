#pragma once

#include <string>
#include <filesystem>
#include <unistd.h>

class TemporaryFolder {
public:
    TemporaryFolder(const std::string& prefix);
    ~TemporaryFolder();

    const std::filesystem::path& path() const;

    void write_file(const std::string& filename, const std::string& content);

private:
    std::string _name;
    std::filesystem::path _path;
};
