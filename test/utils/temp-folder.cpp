#include "temp-folder.h"
#include <fstream>

TemporaryFolder::TemporaryFolder(const std::string& prefix)
    : _name(prefix + "-" + std::to_string(::getpid()))
    , _path(std::filesystem::temp_directory_path() / _name)
{
    std::filesystem::create_directory(_path);
}

TemporaryFolder::~TemporaryFolder()
{
    std::filesystem::remove_all(_path);
}

const std::filesystem::path& TemporaryFolder::path() const
{
    return _path;
}

void TemporaryFolder::write_file(const std::string& filename, const std::string& content)
{
    std::ofstream(_path / filename) << content;
}
