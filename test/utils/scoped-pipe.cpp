#include "scoped-pipe.h"
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <system_error>

ScopedPipe::ScopedPipe()
{
    ::pipe(_fd);
}

ScopedPipe::~ScopedPipe()
{
    if (_fd[0] != -1) {
        ::close(_fd[0]);
    }

    if (_fd[1] != -1) {
        ::close(_fd[1]);
    }
}

int ScopedPipe::write_fd() const
{
    return _fd[1];
}

int ScopedPipe::read_fd() const
{
    return _fd[0];
}
