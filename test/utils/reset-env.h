#pragma once

#include <algorithm>
#include <string>
#include <vector>
#include <map>
#include <stdlib.h>

class ResetEnv {
public:
    ResetEnv(std::vector<std::string> env_list)
    {
        std::for_each(env_list.begin(), env_list.end(), [&](auto& key) {
            auto value = ::getenv(key.c_str());
            if (value == nullptr) {
                return;
            }
            _value_map.insert(make_pair(key, std::string(value)));
            ::unsetenv(key.c_str());
        });
    }

    ~ResetEnv()
    {

        std::for_each(_value_map.begin(), _value_map.end(), [&](auto it) {
            ::setenv(it.first.c_str(), it.second.c_str(), 1);
        });
    }

private:
    std::map<std::string, std::string> _value_map;
};
