#pragma once
#include <filesystem>
#include <string>

std::filesystem::path build_dir(const std::string& file);
