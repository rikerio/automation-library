#include "child-process.h"
#include <iostream>
#include <stdio.h>

namespace {

void redirect_output(ScopedPipe pipe, int target_fd)
{
    if (::dup2(pipe.write_fd(), target_fd) == -1) {
        throw std::system_error(errno, std::system_category(), "dup2");
    }
}

}

ChildProcess::ChildProcess(ChildProcess::ChildHandler child_handler, ChildProcess::CaptureReference capture)
    : _capture(std::move(capture))
{
    _pid = fork();

    if (_pid == 0 && child_handler) {
        if (_capture) {
            redirect_output(std::move(_stdout_pipe), STDOUT_FILENO);
            // redirect_output(std::move(_stderr_pipe), STDERR_FILENO);
        }

        child_handler();
        exit(EXIT_SUCCESS);
    } else if (_pid > 0) {

    } else {
        throw std::system_error(errno, std::system_category());
    }
}

ChildProcess::~ChildProcess()
{
    if (exited()) {
        return;
    }
    kill();
    wait();
}

void ChildProcess::kill(int signal)
{
    ::kill(_pid, signal);
}

void ChildProcess::wait()
{
    if (_capture) {
        _capture->get().read(std::move(_stdout_pipe));
    }

    if (::waitpid(_pid, &_child_status, 0) == -1) {
        throw std::system_error(errno, std::system_category());
    }
}

bool ChildProcess::exited() const
{
    return WIFEXITED(_child_status);
}

int ChildProcess::exit_status() const
{
    return WEXITSTATUS(_child_status);
}

pid_t ChildProcess::pid() const
{
    return _pid;
}
