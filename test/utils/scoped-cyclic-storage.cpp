#include "scoped-cyclic-storage.h"
#include <iostream>

using namespace automation;

ScopedStorage::ScopedStorage(const std::string& name, std::size_t size)
    : _folder(name)
{
    auto exp = cyclic::Storage::create(_folder.path(), size);

    if (!exp) {
        std::cout << exp.error().message() << std::endl;
    }
}

ScopedStorage::~ScopedStorage()
{
    auto res = cyclic::Storage::remove(_folder.path(), true);

    if (!res) {
        std::cout << res.error().message() << std::endl;
    }
}

const std::filesystem::path& ScopedStorage::path() const
{
    return _folder.path();
}
