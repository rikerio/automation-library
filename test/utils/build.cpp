#include "build.h"

std::filesystem::path build_dir(const std::string& file)
{
    return std::filesystem::path(BUILD_FOLDER) / file;
}
