#pragma once

#include "cyclic/storage.h"
#include "temp-folder.h"
#include <filesystem>

class ScopedStorage {
public:
    ScopedStorage(const std::string& name, std::size_t size);
    ~ScopedStorage();

    const std::filesystem::path& path() const;

private:
    TemporaryFolder _folder;
};
