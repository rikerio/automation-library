#include <catch2/catch_test_macros.hpp>
#include "server.h"
#include "bridge.h"

using namespace automation;

namespace {

class ServerMock : public scheduler::AbstractServer {
public:
    unsigned int call_count = 0;
    int call_argument = 0;

    void trigger_event(scheduler::Server::Event event, int fd, scheduler::Message msg)
    {
        emit(event, fd, msg);
    }

    void send_loop_request(int fd)
    {
        call_count += 1;
        call_argument = fd;
    }
};
} // namespace

SCENARIO("Scheduler::Bridge")
{
    auto new_task_event = scheduler::Server::Event::NewTask;
    auto task_done_event = scheduler::Server::Event::TaskDone;

    GIVEN("A Bridge")
    {
        ServerMock server_mock;
        scheduler::Bridge bridge(server_mock, 16);

        WHEN("A new task is created")
        {
            int fd = 2;
            uint16_t task_id = 3;
            uint16_t new_task_id = 0;

            bridge.on(new_task_event, [&new_task_id](auto tid) {
                new_task_id = tid;
            });

            server_mock.trigger_event(
                new_task_event,
                fd,
                { scheduler::Function::Assign, task_id });

            THEN("the fd is assigned to the task_id")
            {
                CHECK(bridge.get_task_id(1) == 0);
                CHECK(bridge.get_task_id(fd) == task_id);
                CHECK(bridge.get_fd(2) == -1);
                CHECK(bridge.get_fd(task_id) == fd);
            }

            THEN("a handle with the task_id is called")
            {
                CHECK(new_task_id == task_id);
            }

            WHEN("A task handles a done call")
            {
                unsigned int call_count = 0;
                uint16_t new_task_id = 0;

                bridge.on(task_done_event, [&](auto tid) {
                    new_task_id = tid;
                    call_count += 1;
                });

                server_mock.trigger_event(task_done_event, fd, {});
                server_mock.trigger_event(task_done_event, 3, {});

                THEN("a handle with the task_id is called")
                {
                    CHECK(new_task_id == task_id);
                    CHECK(call_count == 1);
                }

                WHEN("A task handles a done call")
                {
                    bridge.send_loop_request(new_task_id);

                    THEN("a handle with the task_id is called")
                    {
                        CHECK(server_mock.call_count == 1);
                        CHECK(server_mock.call_argument == fd);
                    }
                }
            }
        }
    }
}
