#include <catch2/catch_test_macros.hpp>
#include "config.h"
#include "utils/temp-folder.h"

using namespace automation::scheduler;

SCENARIO("scheduler::ConfigParser")
{
    TemporaryFolder temp_folder("config");

    GIVEN("A Simple Configuration File")
    {
        temp_folder.write_file("config.yaml", "laps:\n  - [1]\n  - [2, 3]\n  - [4]\n");

        WHEN("reading configuration file")
        {
            auto config = ConfigParser::parse_file(temp_folder.path() / "config.yaml");

            THEN("it is a valid configuration")
            {
                REQUIRE(config.has_value() == true);
                REQUIRE(config->task_count() == 4);
                REQUIRE(config->sprint_count() == 3);
                CHECK(config->at(0) == std::vector<uint16_t>({ 1 }));
                CHECK(config->at(1) == std::vector<uint16_t>({ 2, 3 }));
                CHECK(config->at(2) == std::vector<uint16_t>({ 4 }));
            }
        }
    }

    GIVEN("A Simple corrupt file")
    {
        temp_folder.write_file("config.yaml", "laps: 1\n");

        WHEN("reading configuration file")
        {
            auto config = ConfigParser::parse_file(temp_folder.path() / "config.yaml");

            THEN("the configuration is not valid")
            {
                CHECK(config.has_value() == false);
            }
        }

        temp_folder.write_file("config.yaml", "laps:\n - abc\n");

        WHEN("reading configuration file")
        {
            auto config = ConfigParser::parse_file(temp_folder.path() / "config.yaml");

            THEN("the configuration is not valid")
            {
                CHECK(config.has_value() == false);
            }
        }

        temp_folder.write_file("config.yaml", "laps:\n - [abc]\n");

        WHEN("reading configuration file")
        {
            auto config = ConfigParser::parse_file(temp_folder.path() / "config.yaml");

            THEN("the configuration is not valid")
            {
                CHECK(config.has_value() == false);
            }
        }

        temp_folder.write_file("config.yaml", "laps:\n - 1\n - [1, 2]\n - 3\n");

        WHEN("reading configuration file")
        {
            auto config = ConfigParser::parse_file(temp_folder.path() / "config.yaml");

            THEN("the configuration is not valid")
            {
                CHECK(config.has_value() == false);
            }
        }
    }
}
