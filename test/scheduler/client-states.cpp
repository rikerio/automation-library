#include <catch2/catch_test_macros.hpp>
#include "statemachine.h"
#include <functional>
#include <map>

namespace {

class CounterMap {
public:
    unsigned int get_count(uint16_t task_id)
    {
        auto it = _map.find(task_id);
        if (it == _map.end()) {
            return 0;
        }
        return it->second;
    }

    void increase(uint16_t task_id)
    {
        auto it = _map.find(task_id);

        if (it == _map.end()) {
            _map.insert(std::make_pair(task_id, 1));
            return;
        }

        it->second += 1;
    }

    std::function<void(uint16_t)> increase_handler()
    {
        return std::bind(&CounterMap::increase, this, std::placeholders::_1);
    }

private:
    std::map<uint16_t, unsigned int> _map;
};

} // namespace

SCENARIO("Scheduler::Client::States")
{
    using namespace automation::scheduler;
    using namespace std::placeholders;

    GIVEN("A client statemachine")
    {

        StateMachine sm {
            { { 1 },
                { 2, 3 },
                { 4 } }
        };
        CounterMap loop_map;
        CounterMap done_map;
        CounterMap stale_map;

        WHEN("started with no attached tasks")
        {
            sm.on(StateMachine::Event::Loop, std::bind(&CounterMap::increase, &loop_map, _1));
            sm.start();

            THEN("no task handler will be called")
            {
                CHECK(loop_map.get_count(1) == 0);
                CHECK(loop_map.get_count(2) == 0);
                CHECK(loop_map.get_count(3) == 0);
                CHECK(loop_map.get_count(4) == 0);
            }
        }

        WHEN("started with some attached tasks")
        {
            sm.on(StateMachine::Event::Loop, [&](auto task_id) {
                loop_map.increase(task_id);
                sm.report_done(task_id);
            });
            sm.attach(1);
            sm.attach(3);
            sm.attach(4);
            sm.start();

            THEN("some task handler will be called")
            {
                CHECK(loop_map.get_count(1) == 1);
                CHECK(loop_map.get_count(2) == 0);
                CHECK(loop_map.get_count(3) == 1);
                CHECK(loop_map.get_count(4) == 1);
            }
        }

        WHEN("all tasks are attached")
        {
            sm.attach(1);
            sm.attach(2);
            sm.attach(3);
            sm.attach(4);

            WHEN("started")
            {
                sm.on(StateMachine::Event::Loop, std::bind(&CounterMap::increase, &loop_map, _1));
                sm.start();

                THEN("the handler for task.a will be called once")
                {
                    CHECK(loop_map.get_count(1) == 1);
                }
            }

            WHEN("started and reported back")
            {
                sm.on(StateMachine::Event::Loop, std::bind(&CounterMap::increase, &loop_map, _1));
                sm.start();
                sm.report_done(1);

                THEN("the handler for task.a, b and c will be called once")
                {
                    CHECK(loop_map.get_count(1) == 1);
                    CHECK(loop_map.get_count(2) == 1);
                    CHECK(loop_map.get_count(3) == 1);
                    CHECK(loop_map.get_count(4) == 0);
                }
            }

            WHEN("started and did not reported back")
            {
                sm.on(StateMachine::Event::Loop, std::bind(&CounterMap::increase, &loop_map, _1));
                sm.on(StateMachine::Event::Stale, std::bind(&CounterMap::increase, &stale_map, _1));
                sm.start();
                sm.start();

                THEN("the handler for task.a, b and c will be called once")
                {
                    CHECK(stale_map.get_count(1) == 1);
                    CHECK(loop_map.get_count(1) == 1);
                    CHECK(loop_map.get_count(2) == 1);
                    CHECK(loop_map.get_count(3) == 1);
                    CHECK(loop_map.get_count(4) == 0);
                }
            }

            WHEN("iterated a whole loop")
            {
                sm.on(StateMachine::Event::Loop, [&](auto task_id) {
                    loop_map.increase(task_id);
                    sm.report_done(task_id);
                });
                sm.start();

                THEN("the handler will be called for all tasks once")
                {
                    CHECK(loop_map.get_count(1) == 1);
                    CHECK(loop_map.get_count(2) == 1);
                    CHECK(loop_map.get_count(3) == 1);
                    CHECK(loop_map.get_count(4) == 1);
                }
            }

            WHEN("iterated two whole loops")
            {
                sm.on(StateMachine::Event::Loop, [&](auto task_id) {
                    loop_map.increase(task_id);
                    sm.report_done(task_id);
                });
                sm.start();
                sm.start();

                THEN("the handler will be called for all tasks once")
                {
                    CHECK(loop_map.get_count(1) == 2);
                    CHECK(loop_map.get_count(2) == 2);
                    CHECK(loop_map.get_count(3) == 2);
                    CHECK(loop_map.get_count(4) == 2);
                }
            }

            WHEN("iterated two whole loop with stale tasks")
            {
                sm.on(StateMachine::Event::Loop, [&](auto task_id) {
                    loop_map.increase(task_id);
                    if (task_id == 3) {
                        return;
                    }
                    sm.report_done(task_id);
                });
                sm.start();
                sm.start();

                THEN("the handler will be called for all tasks once")
                {
                    CHECK(loop_map.get_count(1) == 2);
                    CHECK(loop_map.get_count(2) == 2);
                    CHECK(loop_map.get_count(3) == 1);
                    CHECK(loop_map.get_count(4) == 1);
                }
            }

            WHEN("iterated some loop with a stale task")
            {
                unsigned int sprint = 0;

                sm.on(StateMachine::Event::Stale, stale_map.increase_handler());
                sm.on(StateMachine::Event::Done, done_map.increase_handler());
                sm.on(StateMachine::Event::Loop, loop_map.increase_handler());
                sm.on(StateMachine::Event::Loop, [&](auto task_id) {
                    if (sprint == 0 && task_id != 3) {
                        sm.report_done(task_id);
                        return;
                    }

                    if (sprint == 1 && task_id == 1) {
                        sm.report_done(3);
                        sm.report_done(task_id);
                        return;
                    }

                    if (sprint > 0) {
                        sm.report_done(task_id);
                    }
                });

                for (sprint = 0; sprint < 10; sprint += 1) {
                    sm.start();
                }

                THEN("the handler will be called")
                {
                    CHECK(loop_map.get_count(1) == 10);
                    CHECK(loop_map.get_count(2) == 10);
                    CHECK(loop_map.get_count(3) == 9);
                    CHECK(loop_map.get_count(4) == 9);
                }
            }

            WHEN("iterated some loop with a detached task")
            {
                unsigned int sprint = 0;

                sm.on(StateMachine::Event::Stale, stale_map.increase_handler());
                sm.on(StateMachine::Event::Done, done_map.increase_handler());
                sm.on(StateMachine::Event::Loop, loop_map.increase_handler());
                sm.on(StateMachine::Event::Loop, [&](auto task_id) {
                    if (sprint == 1 && task_id == 2) {
                        sm.detach(3);
                    }

                    if (sprint > 0 && task_id == 3) {

                    } else {
                        sm.report_done(task_id);
                    }
                });

                for (sprint = 0; sprint < 10; sprint += 1) {
                    sm.start();
                }

                THEN("the handler will be called")
                {
                    CHECK(loop_map.get_count(1) == 10);
                    CHECK(loop_map.get_count(2) == 10);
                    CHECK(loop_map.get_count(3) == 1);
                    CHECK(loop_map.get_count(4) == 10);
                }
            }
        }
    }
}
