#include <catch2/catch_test_macros.hpp>

#include <boost/process.hpp>
#include <iostream>
#include <condition_variable>
#include "automation.h"
#include "utils/temp-folder.h"
#include "utils/build.h"

using namespace automation;
using namespace std::chrono_literals;
namespace bp = boost::process;

namespace {

cyclic::Thread create_thread(std::filesystem::path& socket_path, uint16_t task_id)
{
    return cyclic::Thread::create<cyclic::strategy::RikerIO>(socket_path, task_id, 0);
}

class ProcessSync {
public:
    ProcessSync(int signal = SIGUSR1)
        : _signal({ signal })
    {
    }

    bool wait()
    {
        bool result = false;
        auto timeout = utils::TimerDescriptor::create_oneshot(2s);

        utils::EventService service;
        service.register_handler(_signal.fd(), [&]() {
            _signal.read();
            service.stop();
            result = true;
        });

        service.register_handler(timeout.fd(), [&]() {
            timeout.read();
            service.stop();
        });

        service.start();

        return result;
    }

private:
    utils::SignalDescriptor _signal;
};

static std::condition_variable cond;
static std::mutex mutex;

bool thread_wait()
{
    std::unique_lock lock(mutex);
    return cond.wait_for(lock, 2s) != std::cv_status::timeout;
}

void thread_notify()
{
    cond.notify_one();
}

class Task {
    static std::atomic<unsigned int> task_counter;

public:
    unsigned int id = 0;
    unsigned int counter = 0;

    Task()
    {
        static unsigned int _id = 1;
        id = _id++;

        task_counter++;
    }

    ~Task()
    {
        task_counter--;
    }

    void loop()
    {
        counter += 1;

        if (counter == 10) {
            task_counter--;

            if (task_counter.load() == 0) {
                thread_notify();
            }
        }
    }
};

std::atomic<unsigned int> Task::task_counter(0);

} // namespace

SCENARIO("Scheduler Integration")
{
    TemporaryFolder temp_folder("scheduler-integration");

    auto env = bp::env["RIO_SCHEDULER_NOTIFY"] = "parent";
    auto config_path = temp_folder.path() / "config.yaml";
    auto socket_path = temp_folder.path() / "socket";

    GIVEN("a scheduler server with a simple configuration")
    {

        ProcessSync process_sync;

        temp_folder.write_file("config.yaml", "laps: [ 1 ]");

        auto process = bp::child {
            build_dir("src/scheduler/rio-scheduler").string(),
            std::vector<std::string> {
                "--config",
                config_path,
                "--socket",
                socket_path,
                "--priority",
                "0",
                "--duration",
                "100000",
            },
            env
        };

        CHECK(process_sync.wait());

        WHEN("starting a task")
        {
            Task task;

            auto thread = create_thread(socket_path, 1);
            thread.use(task);
            thread.start();

            CHECK(thread_wait());

            thread.stop();

            THEN("the tasker counter applies")
            {
                CHECK(task.counter >= 10);
            }
        }
    }

    GIVEN("a scheduler server with a complexe configuration")
    {
        ProcessSync process_sync;
        temp_folder.write_file("config.yaml", "laps: [ 1, [2,3], 4 ]");

        auto process = bp::child {
            build_dir("src/scheduler/rio-scheduler").string(),
            std::vector<std::string> {
                "--config",
                config_path,
                "--socket",
                socket_path,
                "--priority",
                "0",
                "--duration",
                "100000" },
            env
        };

        CHECK(process_sync.wait());

        WHEN("starting a task")
        {
            Task task[4];

            auto thread_a = create_thread(socket_path, 1);
            auto thread_b = create_thread(socket_path, 2);
            auto thread_c = create_thread(socket_path, 3);
            auto thread_d = create_thread(socket_path, 4);

            thread_a.use(task[0]);
            thread_b.use(task[1]);
            thread_c.use(task[2]);
            thread_d.use(task[3]);

            thread_a.start();
            thread_b.start();
            thread_c.start();
            thread_d.start();

            CHECK(thread_wait());

            thread_a.stop();
            thread_b.stop();
            thread_c.stop();
            thread_d.stop();

            THEN("the task counter applies")
            {
                CHECK(task[0].counter >= 10);
                CHECK(task[1].counter >= 10);
                CHECK(task[2].counter >= 10);
                CHECK(task[3].counter >= 10);
            }
        }
    }
}
