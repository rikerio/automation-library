#include <catch2/catch_test_macros.hpp>
#include <thread>
#include <condition_variable>

#include "utils/temp-folder.h"
#include "utils/event-service.h"
#include "utils/timer-descriptor.h"
#include "server.h"
#include "scheduler/client.h"

using namespace automation;
using namespace automation::scheduler;
using namespace std::chrono_literals;

SCENARIO("scheduler::Server")
{
    TemporaryFolder temp_folder("scheduler-server");
    auto socket_path = temp_folder.path() / "scheduler.sock";
    utils::EventService service;

    GIVEN("A server")
    {
        scheduler::Message received_msg;
        utils::UnixDomainSocket server_socket(socket_path);
        scheduler::Server server(service, std::move(server_socket), 16);

        WHEN("a client lifetime")
        {
            server.on(scheduler::Server::Event::NewTask, [&](auto fd, auto msg) {
                received_msg = msg;
                CHECK(server.connections() == 1);
            });

            server.on(scheduler::Server::Event::TaskExited, [&](auto fd, auto) {
                CHECK(server.connections() == 0);
                service.stop();
            });

            scheduler::Client {
                service,
                utils::UnixDomainSocket::connect(socket_path),
                1
            };

            auto timeout_event = utils::TimerDescriptor::create_oneshot(200ms);

            service.register_handler(timeout_event.fd(), [&]() {
                service.stop();
                CHECK(false);
            });

            service.start();
        }

        WHEN("the client gets looped")
        {

            server.on(scheduler::Server::Event::NewTask, [&](auto fd, auto) {
                server.send_loop_request(fd);
            });

            server.on(scheduler::Server::Event::TaskDone, [&](auto, auto) {
                service.stop();
            });

            scheduler::Client client {
                service,
                utils::UnixDomainSocket::connect(socket_path),
                1
            };

            client.on(scheduler::Client::Event::Loop, [&]() {
                client.send_done();
            });

            auto timeout_event = utils::TimerDescriptor::create_oneshot(200ms);

            service.register_handler(timeout_event.fd(), [&]() {
                service.stop();
                CHECK(false);
            });

            service.start();
        }
    }
}
