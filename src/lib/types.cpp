#include "types.h"
#include <algorithm>

using namespace automation;

namespace {

const std::string& to_string(DataType datatype)
{
    static const std::map<DataType, std::string> map {
        { DataType::Undefined, "Undefined" },
        { DataType::Bit, "Bit" },
        { DataType::Bool, "Bool" },
        { DataType::Uint8, "Uint8" },
        { DataType::Int8, "Int8" },
        { DataType::Uint16, "Uint16" },
        { DataType::Int16, "Int16" },
        { DataType::Uint32, "Uint32" },
        { DataType::Int32, "Int32" },
        { DataType::Uint64, "Uint64" },
        { DataType::Int64, "Int64" },
        { DataType::Float, "Float" },
        { DataType::Double, "Double" },
        { DataType::String, "String" },
        { DataType::Buffer, "Buffer" }
    };

    return map.find(datatype)->second;
}

DataType string_to_datatype(const std::string& value)
{
    static const std::map<std::string, DataType> map {
        { "Undefined", DataType::Undefined },
        { "Bit", DataType::Bit },
        { "Bool", DataType::Bool },
        { "Uint8", DataType::Uint8 },
        { "Int8", DataType::Int8 },
        { "Uint16", DataType::Uint16 },
        { "Int16", DataType::Int16 },
        { "Uint32", DataType::Uint32 },
        { "Int32", DataType::Int32 },
        { "Uint64", DataType::Uint64 },
        { "Int64", DataType::Int64 },
        { "Float", DataType::Float },
        { "Double", DataType::Double },
        { "String", DataType::String },
        { "Buffer", DataType::Buffer }
    };

    auto it = map.find(value);

    if (it == map.end()) {
        return DataType::Undefined;
    }

    return it->second;
}

} // namespace

BaseType BaseType::type_from_name(const std::string& name, std::size_t size)
{
    return BaseType::type_from_name(string_to_datatype(name), size);
}

BaseType BaseType::type_from_name(DataType datatype, std::size_t size)
{

    static const std::map<DataType, std::size_t> map {
        { DataType::Undefined, 0 },
        { DataType::Bit, 1 },
        { DataType::Bool, 1 },
        { DataType::Uint8, 1 },
        { DataType::Int8, 1 },
        { DataType::Uint16, 2 },
        { DataType::Int16, 2 },
        { DataType::Uint32, 4 },
        { DataType::Int32, 4 },
        { DataType::Uint64, 8 },
        { DataType::Int64, 8 },
        { DataType::Float, 4 },
        { DataType::Double, 8 },
        { DataType::String, 0 },
        { DataType::Buffer, 0 }
    };

    auto it = map.find(datatype);

    if (it == map.end()) {
        throw std::runtime_error("undefine type.");
    }

    if (datatype == DataType::Bit) {
        return { datatype, SizingStrategy::Bits, it->second };
    }

    if (it->second == 0) {
        return { datatype, SizingStrategy::Bytes, size };
    }

    return { datatype, SizingStrategy::Bytes, it->second };
}

BaseType::BaseType(DataType datatype, SizingStrategy sizing_strategy, std::size_t size)
    : _datatype(datatype)
    , _sizing_strategy(sizing_strategy)
    , _size(size)
{
}

bool BaseType::operator==(const BaseType& rhs) const
{
    if (_datatype == DataType::Undefined || rhs._datatype == DataType::Undefined) {
        return false;
    }

    if (_datatype != rhs._datatype) {
        return false;
    }

    return _size == rhs._size;
}

bool BaseType::operator!=(const BaseType& rhs) const
{
    return !this->operator==(rhs);
}

const std::string& BaseType::name() const
{
    return to_string(_datatype);
}

DataType BaseType::datatype() const
{
    return _datatype;
}

SizingStrategy BaseType::sizing_strategy() const
{
    return _sizing_strategy;
}

std::size_t BaseType::size() const
{
    return _size;
}

bool BaseType::is_undefined() const
{
    return _datatype == DataType::Undefined;
}
