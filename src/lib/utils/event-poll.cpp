#include "utils/event-poll.h"

#include <sys/epoll.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <algorithm>
#include <stdexcept>

using namespace automation::utils;

EventPoll::EventPoll()
    : _fd { ::epoll_create1(0) }
{
    if (_fd.fd() == -1) {
        throw std::system_error(errno, std::system_category(), "EventPoll::EventPoll");
    }
}

void EventPoll::reg(int fd)
{
    struct epoll_event event = {};

    event.data.fd = fd;
    event.events = EPOLLIN | EPOLLET;

    if (::epoll_ctl(_fd.fd(), EPOLL_CTL_ADD, fd, &event) == -1) {
        throw std::system_error(errno, std::system_category(), "EventPoll::reg");
    }
}

void EventPoll::unreg(int fd)
{
    int ret = ::epoll_ctl(_fd.fd(), EPOLL_CTL_DEL, fd, NULL);

    if (ret == -1) {
        throw std::system_error(errno, std::system_category(), "EventPoll::unreg");
    }
}
