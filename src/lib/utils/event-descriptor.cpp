#include "utils/event-descriptor.h"

#include <sys/eventfd.h>
#include <unistd.h>
#include <system_error>

using namespace automation::utils;

EventDescriptor EventDescriptor::create(int flags)
{
    int fd = ::eventfd(0, flags);

    if (fd == -1) {
        throw std::system_error(errno, std::system_category(), "EventDescriptor::create");
    }

    return EventDescriptor(fd);
}

EventDescriptor::EventDescriptor(EventDescriptor&& other)
{
    std::swap(_fd, other._fd);
}

EventDescriptor& EventDescriptor::operator=(EventDescriptor&& other)
{
    std::swap(_fd, other._fd);
    return *this;
}

EventDescriptor::EventDescriptor(int fd)
    : _fd(fd)
{
}

EventDescriptor::read_type EventDescriptor::read()
{
    return DescriptorEvent<event_type>::read(_fd.fd());
}

void EventDescriptor::write(EventDescriptor::event_type value)
{
    if (::eventfd_write(_fd.fd(), value) == -1) {
        throw std::system_error(errno, std::system_category(), "eventfd_write");
    };
};

int EventDescriptor::fd() const
{
    return _fd.fd();
}
