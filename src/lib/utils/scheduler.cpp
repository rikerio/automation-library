#include "utils/scheduler.h"

#include <system_error>
#include <cstring>
#include "utils/linux.h"

using namespace automation::utils;

void Scheduler::set_other()
{
    struct sched_attr attr;
    std::memset(&attr, 0, sizeof(attr));
    attr.size = sizeof(attr);
    attr.sched_policy = SCHED_OTHER;

    int err_set_sched = sched_setattr(0, &attr, 0);

    if (err_set_sched != 0) {
        // spdlog::warn("Error setting scheduling policy 'OTHER'.");
    }
}

void Scheduler::set_fifo(int priority)
{
    struct sched_attr attr;
    std::memset(&attr, 0, sizeof(attr));
    attr.size = sizeof(attr);
    attr.sched_policy = SCHED_FIFO;
    attr.sched_flags = 0;
    attr.sched_nice = 0;
    attr.sched_priority = priority;

    int err_set_sched = sched_setattr(0, &attr, 0);

    if (err_set_sched != 0) {
        // spdlog::warn("Error setting scheduling policy 'FIFO'.");
    }
}

void Scheduler::set_rr(int priority)
{
    struct sched_attr attr;
    std::memset(&attr, 0, sizeof(attr));
    attr.size = sizeof(attr);
    attr.sched_policy = SCHED_RR;
    attr.sched_flags = 0;
    attr.sched_nice = 0;
    attr.sched_priority = priority;

    int err_set_sched = sched_setattr(0, &attr, 0);

    if (err_set_sched != 0) {
        // spdlog::warn("Error setting scheduling policy 'RR'.");
    }
}

void Scheduler::set_deadline(int runtime, int period, int deadline)
{
    struct sched_attr attr;
    std::memset(&attr, 0, sizeof(attr));
    attr.size = sizeof(attr);
    attr.sched_flags = 0;
    attr.sched_nice = 0;
    attr.sched_priority = 0;

    attr.sched_policy = SCHED_DEADLINE;
    attr.sched_runtime = runtime;
    attr.sched_period = period;
    attr.sched_deadline = deadline;

    int err_set_sched = sched_setattr(0, &attr, 0);

    if (err_set_sched != 0) {
        // spdlog::warn("Error setting scheduling policy 'DEADLINE'.");
    }
}
