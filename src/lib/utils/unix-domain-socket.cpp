#include "utils/unix-domain-socket.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <cstring>

using namespace automation::utils;

UnixDomainSocket::Connection::Connection(Connection&& other)
{
    std::swap(_fd, other._fd);
}

UnixDomainSocket::Connection& UnixDomainSocket::Connection::operator=(Connection&& other)
{
    std::swap(_fd, other._fd);
    return *this;
}

UnixDomainSocket::Connection::Connection(int fd)
    : _fd(fd)
{
}

void UnixDomainSocket::Connection::write(const void* buf, std::size_t size)
{
    // Using send instead of write cause in some cases
    // the socket could be closed and we get a SIGPIPE Signal thrown.
    // With send and MSG_NOSIGNAL we get an EPIPE errno in return instead
    // of a signal
    int ret = ::send(_fd.fd(), buf, size, MSG_NOSIGNAL);

    if (ret == -1 && errno == EAGAIN) {
        return;
    }

    if (ret == -1 && errno == EPIPE) {
        return;
    }

    if (ret == -1) {
        throw std::system_error(errno, std::system_category(), "write");
    }
}

void UnixDomainSocket::Connection::write(const std::string& message)
{
    write(std::data(message), message.length());
}

void UnixDomainSocket::Connection::write(const std::vector<char>& buffer)
{
    write(std::data(buffer), buffer.size());
}

DescriptorEvent<std::string> UnixDomainSocket::Connection::read_as_string()
{
    return DescriptorEvent<std::string>::read(_fd.fd(), 4096);
}

DescriptorEvent<std::vector<char>> UnixDomainSocket::Connection::read_as_buffer()
{
    return DescriptorEvent<std::vector<char>>::read(_fd.fd(), 4096);
}

int UnixDomainSocket::Connection::fd() const
{
    return _fd.fd();
}

UnixDomainSocket::Connection UnixDomainSocket::connect(const std::filesystem::path& socket_path)
{
    int fd = ::socket(AF_UNIX, SOCK_STREAM | SOCK_NONBLOCK | SOCK_CLOEXEC, 0);

    if (fd == -1) {
        throw std::system_error(errno, std::system_category(), "socket");
    }

    sockaddr_un addr;
    std::memset(&addr, 0, sizeof(sockaddr_un));
    addr.sun_family = AF_UNIX;
    std::strcpy(addr.sun_path, socket_path.c_str());

    if (::connect(fd, (sockaddr*)&addr, sizeof(sockaddr_un)) == -1) {
        throw std::system_error(errno, std::system_category(), "connect");
    }

    return UnixDomainSocket::Connection(fd);
}

UnixDomainSocket::Connection UnixDomainSocket::accept(UnixDomainSocket& socket)
{
    return UnixDomainSocket::Connection(socket.accept());
}

UnixDomainSocket::UnixDomainSocket(const std::filesystem::path socket_path)
    : _fd { ::socket(AF_UNIX, SOCK_STREAM | SOCK_NONBLOCK, 0) }
{
    if (_fd.fd() == -1) {
        throw std::system_error(errno, std::system_category(), "socket");
    }

    ::unlink(socket_path.c_str());

    sockaddr_un addr;
    std::memset(&addr, 0, sizeof(sockaddr_un));

    addr.sun_family = AF_UNIX;
    std::strcpy(addr.sun_path, socket_path.c_str());

    if (::bind(_fd.fd(), (sockaddr*)(&addr), sizeof(addr)) == -1) {
        throw std::system_error(errno, std::system_category(), "bind");
    }

    if (::listen(_fd.fd(), 512) == -1) {
        throw std::system_error(errno, std::system_category(), "listen");
    }
}

UnixDomainSocket::UnixDomainSocket(int fd)
    : _fd(fd)
{
}

int UnixDomainSocket::fd() const
{
    return _fd.fd();
}

int UnixDomainSocket::accept() const
{
    int client_fd = ::accept4(_fd.fd(), NULL, NULL, SOCK_NONBLOCK);

    if (client_fd <= 0) {
        if (errno == EAGAIN || errno == EWOULDBLOCK) {
            return -1;
        }
        throw std::system_error(errno, std::system_category(), "accept4");
    }

    return client_fd;
}
