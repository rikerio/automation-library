#include "utils/inotify-descriptor.h"
#include <system_error>

using namespace automation::utils;

InotifyDescriptor::InotifyDescriptor(const std::filesystem::path& path, uint32_t mask)
    : _fd { ::inotify_init1(IN_NONBLOCK) }
    , _watch { ::inotify_add_watch(_fd.fd(), path.c_str(), mask) }
{

    if (_fd.fd() == -1) {
        throw std::system_error(errno, std::system_category(), "InotifyDescriptor");
    }

    if (_watch == -1) {
        throw std::system_error(errno, std::system_category(), "InotifyDescriptor");
    }
}

InotifyDescriptor::~InotifyDescriptor()
{
    if (_fd.fd() == -1) {
        return;
    }
    ::inotify_rm_watch(_fd.fd(), _watch);
}

InotifyDescriptor::EventVector InotifyDescriptor::read()
{
    InotifyDescriptor::EventVector result;
    while (1) {

        char buffer[4096] = { 0 };
        int len = ::read(_fd.fd(), buffer, 4096);

        if (len == -1 && errno != EAGAIN) {
            throw std::system_error(errno, std::system_category(), "InotifyDescriptor::handle");
        }

        if (len <= 0) {
            break;
        }

        char* ptr = buffer;
        int cntr = 0;
        while (cntr < len) {
            inotify_event* event = reinterpret_cast<inotify_event*>(ptr);
            cntr += sizeof(inotify_event) + event->len;
            ptr = buffer + cntr;
            result.emplace_back(event);
        };
    }

    return result;
}

int InotifyDescriptor::fd() const
{
    return _fd.fd();
}
