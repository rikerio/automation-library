#include "utils/semver.h"
#include "regex"
#include "iostream"
#include "iomanip"

using namespace automation::utils;

SemanticVersion::SemanticVersion(const std::string& v)
    : major(0)
    , minor(0)
    , patch(0)
    , commit(0)
    , hash("")
{

    static const std::regex re("^v?(\\d+).(\\d+).(\\d+)(-(\\d+)-(\\w+))?$");

    std::smatch match;
    if (!std::regex_match(v, match, re)) {
        throw std::invalid_argument("String '" + v + "' musst be of form x.y.z or x.y.z-comit-hash");
    }

    major = stoul(match[1]);
    minor = stoul(match[2]);
    patch = stoul(match[3]);

    if (match.size() >= 5 && match[5] != "") {
        try {
            commit = stoul(match[5]);
        } catch (...) {
            commit = 0;
        }
    }

    if (match.size() >= 6 && match[6] != "") {
        hash = match[6];
    }

    version = std::to_string(major) + "." + std::to_string(minor) + "." + std::to_string(patch);
    extended_version = version + "-" + std::to_string(commit) + "-" + hash;
}

unsigned int SemanticVersion::get_major() const
{
    return major;
}

unsigned int SemanticVersion::get_minor() const
{
    return minor;
}

unsigned int SemanticVersion::get_patch() const
{
    return patch;
}

unsigned int SemanticVersion::get_commit() const
{
    return commit;
}

const std::string& SemanticVersion::get_hash() const
{
    return hash;
}

const std::string& SemanticVersion::get_version() const
{
    return version;
}

const std::string& SemanticVersion::get_extended_version() const
{
    return extended_version;
}
