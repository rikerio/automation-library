#include "utils/lock-file.h"

using namespace automation::utils;

#include <cstring>
#include <algorithm>
#include <fcntl.h>
#include <unistd.h>
#include <sys/file.h>

using namespace automation::utils;

namespace {

bool set_ofd(int fd, int macro, short type)
{
    struct flock flock;
    std::memset(&flock, 0, sizeof(flock));
    flock.l_whence = SEEK_SET;
    flock.l_type = type;

    if (::fcntl(fd, macro, &flock) == -1) {
        return false;
    }

    return true;
}

} // namespace

LockFile LockFile::exclusive(const std::filesystem::path& path)
{
    return LockFile(path, LockFile::Type::Exclusive);
}

LockFile LockFile::shared(const std::filesystem::path& path)
{
    return LockFile(path, LockFile::Type::Shared);
}

LockFile::LockFile(const std::filesystem::path& path, LockFile::Type type)
    : _type(type)
    , _path(path)
{
    int flags = O_CREAT | (_type == Type::Exclusive ? O_RDWR : O_RDONLY);

    _fd = FileDescriptor {
        ::open(path.c_str(), flags, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP)
    };
}

LockFile::~LockFile()
{
    ::unlink(_path.c_str());
}

bool LockFile::valid() const
{
    return _fd.fd() != -1;
}

void LockFile::lock()
{
    set_ofd(_fd.fd(), F_OFD_SETLKW, _type == Type::Exclusive ? F_WRLCK : F_RDLCK);
}

void LockFile::unlock()
{
    if (_fd.fd() == -1) {
        return;
    }

    set_ofd(_fd.fd(), F_OFD_SETLK, F_UNLCK);
}
