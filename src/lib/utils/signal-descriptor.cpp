#include "utils/signal-descriptor.h"
#include <system_error>
#include <unistd.h>

using namespace automation::utils;

SignalDescriptor::SignalDescriptor(std::initializer_list<int> s, int flags)
{

    /* setup signal */
    sigset_t mask;

    sigemptyset(&mask);

    for (auto signal : s) {
        ::sigaddset(&mask, signal);
    }

    if (::sigprocmask(SIG_BLOCK, &mask, NULL) < 0) {
        throw std::system_error(errno, std::system_category(), "sigprocmask");
    }

    _fd = utils::FileDescriptor(::signalfd(-1, &mask, flags));

    if (_fd.fd() < 0) {
        throw std::system_error(errno, std::system_category(), "signalfd");
    }
};

Expected<SignalDescriptor::read_type, SignalDescriptor::read_type::Type> SignalDescriptor::read()
{
    return DescriptorEvent<event_type>::read(_fd.fd());
}

int SignalDescriptor::fd() const
{
    return _fd.fd();
}
