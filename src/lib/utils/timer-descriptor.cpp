#include "utils/timer-descriptor.h"

using namespace automation::utils;

namespace {

timespec calc_timerspec(TimerDescriptor::Duration value)
{
    using std::chrono::nanoseconds;
    using std::chrono::seconds;

    return {
        .tv_sec = std::chrono::duration_cast<seconds>(value).count(),
        .tv_nsec = std::chrono::duration_cast<nanoseconds>(value).count() % nanoseconds::period::den
    };
}

} // namespace

TimerDescriptor TimerDescriptor::create_oneshot(TimerDescriptor::Duration duration)
{
    return TimerDescriptor(duration, static_cast<Duration>(0));
}

TimerDescriptor TimerDescriptor::create_cyclic(TimerDescriptor::Duration duration)
{
    return TimerDescriptor(duration, duration);
}

TimerDescriptor::TimerDescriptor(TimerDescriptor::Duration initial, TimerDescriptor::Duration cycle_time)
    : _fd { ::timerfd_create(CLOCK_MONOTONIC, TFD_NONBLOCK) }
{
    if (_fd.fd() == -1) {
        throw std::system_error(errno, std::system_category(), "timerfd_create");
    }

    itimerspec tspec = {
        .it_interval = calc_timerspec(cycle_time),
        .it_value = calc_timerspec(initial)
    };

    if (timerfd_settime(_fd.fd(), 0, &tspec, NULL) == -1) {
        throw std::system_error(errno, std::system_category(), "timerfd_settime");
    }
}

TimerDescriptor::read_type TimerDescriptor::read()
{
    return DescriptorEvent<event_type>::read(_fd.fd());
}

int TimerDescriptor::fd() const
{
    return _fd.fd();
}
