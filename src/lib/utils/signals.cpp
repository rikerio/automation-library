#include "utils/signals.h"

using namespace automation::utils;

Signals Signals::block(std::initializer_list<int> signals)
{
    sigset_t mask;

    ::sigemptyset(&mask);
    std::for_each(signals.begin(), signals.end(), [&](int signal) {
        ::sigaddset(&mask, signal);
    });

    if (::sigprocmask(SIG_BLOCK, &mask, NULL) < 0) {
        throw std::system_error(errno, std::system_category(), "sigprocmask");
    }

    return Signals(Mode::Block, mask);
}

Signals::~Signals()
{
    if (_mode == Mode::Block) {
        ::sigprocmask(SIG_UNBLOCK, &_mask, NULL);
    }
}

Signals::Signals(Mode mode, sigset_t mask)
    : _mode(mode)
    , _mask(mask)
{
}
