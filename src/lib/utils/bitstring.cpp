#include "utils/bitstring.h"

#include <regex>
#include <boost/lexical_cast.hpp>

automation::MemoryAddress automation::utils::BitString::from_string(const std::string& str)
{
    static std::regex reg("^(-?[0-9]|[1-9][0-9]*)(?:\\.([0-7]))?$", std::regex_constants::ECMAScript);

    std::smatch match;
    if (!std::regex_search(str, match, reg)) {
        throw std::runtime_error("BitString does not match.");
    }

    byte_size_type byte_result = 0;
    bit_offset_type bit_result = 0;

    if (match.size() >= 2) {
        byte_result = boost::lexical_cast<byte_size_type>(match[1].str());
    }

    if (match.size() >= 3 && match[2].str() != "") {
        bit_result = boost::lexical_cast<uint16_t>(match[2].str()); // cast to uint16_t bit_offset_type is actually a char
    }

    return MemoryAddress { byte_result, bit_result };
};
