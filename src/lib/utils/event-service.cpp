#include "utils/event-service.h"
#include <iostream>

using namespace automation::utils;

EventService::EventService(EventService::Strategy strategy, std::size_t max_size)
    : _running(false)
    , _strategy(strategy)
    , _exit_event(utils::EventDescriptor::create())
    , _buffer(max_size)
{

    if (_strategy == Strategy::Dynamic) {
        _driver = std::make_unique<DefaultDriver>();
    } else if (_strategy == Strategy::Fixed) {
        _driver = std::make_unique<FixedSizeDriver>(max_size);
    }

    register_handler(_exit_event.fd(), [&]() {
        _exit_event.read();
        _running = false;
    });
}

EventService::~EventService()
{
    stop();
}

EventService::EventService(EventService&& other)
{
    swap(other);
}

EventService& EventService::operator=(EventService&& other)
{
    swap(other);
    return *this;
}

void EventService::start()
{
    if (_running) {
        throw std::runtime_error("EventService cannot be started twice.");
    }
    _running = true;
    while (_running) {
        auto event_count = _epoll.wait(_buffer);
        for (unsigned int i = 0; i < event_count; i += 1) {
            auto handler = _driver->get(_buffer[i].data.fd);
            if (handler) {
                handler();
            }
        }
        auto handler = _driver->get(-1); // loop handler
        if (handler) {
            handler();
        }
    }
}

void EventService::stop()
{
    if (!_running) {
        return;
    }
    _exit_event.write();
}

void EventService::swap(EventService& other)
{
    std::swap(_running, other._running);
    std::swap(_epoll, other._epoll);
    std::swap(_exit_event, other._exit_event);
    std::swap(_driver, other._driver);
}
