#include "acyclic/storage.h"
#include "application.h"
#include "utils/lock-file.h"
#include <mutex>
#include <fmt/core.h>

using namespace automation;

utils::Expected<acyclic::Storage> acyclic::Storage::create(const std::filesystem::path& root_folder)
{
    std::error_code ec;
    std::filesystem::create_directories(root_folder, ec);

    if (ec) {
        return utils::Error(fmt::format("Error creating directory {} : {}", root_folder.string(), ec.message()));
    }

    auto data_folder = root_folder / data_folder_name;
    auto lock_folder = root_folder / lock_folder_name;

    std::filesystem::create_directories(data_folder, ec);

    if (ec) {
        return utils::Error(fmt::format("Error creating directory {} : {}", data_folder.string(), ec.message()));
    }

    std::filesystem::create_directories(lock_folder, ec);

    if (ec) {
        return utils::Error(fmt::format("Error creating directory {} : {}", lock_folder.string(), ec.message()));
    }

    return acyclic::Storage(root_folder);
}

utils::Expected<acyclic::Storage> acyclic::Storage::attach(const std::filesystem::path& root_folder)
{
    if (!std::filesystem::exists(root_folder)) {
        return utils::Error(fmt::format("Acyclic Storage folder {} does not exist", root_folder.string()));
    }

    return acyclic::Storage(root_folder);
}

utils::Expected<acyclic::Storage> acyclic::Storage::attach(const Application& application)
{
    return acyclic::Storage::attach(application.acyclic_data_path());
}

utils::Expected<utils::Success> acyclic::Storage::remove(const std::filesystem::path& root_folder)
{
    std::error_code ec;
    std::filesystem::remove_all(root_folder, ec);

    if (ec) {
        return utils::Error(ec.message());
    };

    return utils::Success {};
}

acyclic::Storage::Storage(const std::filesystem::path& root_folder)
    : _root_folder(root_folder)
    , _data_folder(_root_folder / data_folder_name)
    , _lock_folder(_root_folder / lock_folder_name)
{
}

const std::filesystem::path& acyclic::Storage::root_folder() const
{
    return _root_folder;
}

const std::filesystem::path& acyclic::Storage::data_folder() const
{
    return _data_folder;
}

const std::filesystem::path& acyclic::Storage::lock_folder() const
{
    return _lock_folder;
}

utils::Expected<acyclic::Data> acyclic::Storage::load(const std::string& name) const
{
    auto meta_path = data_folder() / name;
    auto lock_path = lock_folder() / name;
    auto lock = utils::LockFile::shared(lock_path);
    auto guard = std::scoped_lock(lock);
    auto result = acyclic::Data::deserialize(meta_path);

    if (!result) {
        return std::move(result.error());
    }

    return std::move(result.value());
}

utils::Expected<utils::Success> acyclic::Storage::store(const std::string& name, const acyclic::Data& meta) const
{
    auto meta_path = data_folder() / name;
    auto lock_path = lock_folder() / name;
    auto lock = utils::LockFile::exclusive(lock_path);
    auto guard = std::scoped_lock(lock);

    return acyclic::Data::serialize(meta_path, meta);
}

utils::Expected<utils::Success> acyclic::Storage::reset(const std::string& name) const
{
    auto meta_path = data_folder() / name;
    auto lock_path = lock_folder() / name;
    auto lock = utils::LockFile::exclusive(lock_path);
    auto guard = std::scoped_lock(lock);

    std::error_code ec;
    if (!std::filesystem::remove(meta_path, ec)) {
        return utils::Error(fmt::format("Error removing acyclic meta file {} : {}", meta_path.string(), ec.message()));
    }

    return utils::Success {};
}
