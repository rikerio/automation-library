#include "acyclic/data.h"

#include <fstream>
#include <string.h>
#include <cstring>
#include <mutex>
#include <algorithm>
#include <fmt/core.h>
#include "utils/file.h"

using namespace automation;

acyclic::Data acyclic::Data::create(BaseType type, const void* ptr)
{
    std::vector<char> buffer(type.size());
    std::memcpy(std::data(buffer), ptr, type.size());
    return Data(type, buffer);
}

utils::Expected<utils::Success> acyclic::Data::serialize(const std::filesystem::path& path, const acyclic::Data& meta)
{
    std::ofstream file(path, std::ofstream::out | std::ofstream::trunc | std::ofstream::binary);

    if (!file.is_open()) {
        return utils::Error(fmt::format("Error creating acylcic meta file {}", path.string()));
    }

    utils::file_write(file, meta.type().datatype());
    utils::file_write(file, meta.buffer());

    return utils::Success {};
}

utils::Expected<acyclic::Data> acyclic::Data::deserialize(const std::filesystem::path& path)
{
    std::ifstream file(path, std::ofstream::in | std::ofstream::binary);

    if (!file.is_open()) {
        return utils::Error(fmt::format("Error opening acyclic meta file {}", path.string()));
    }

    auto datatype = utils::file_read<DataType>(file);
    auto type = BaseType::type_from_name(datatype);
    auto data = utils::file_read<std::vector<char>>(file);

    return acyclic::Data(type, std::move(data));
}

acyclic::Data::Data(BaseType type, std::vector<char> buffer)
    : _buffer(std::move(buffer))
    , _type(type)
{
}

const BaseType& acyclic::Data::type() const
{
    return _type;
}

const char* acyclic::Data::data() const
{
    return std::data(_buffer);
}

const std::vector<char>& acyclic::Data::buffer() const
{
    return _buffer;
}
