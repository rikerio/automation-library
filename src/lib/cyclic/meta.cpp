#include "cyclic/meta.h"
#include "utils/pointer.h"
#include "utils/file.h"
#include "utils/lock-file.h"

#include <fmt/core.h>
#include <cstring>
#include <fstream>

using namespace automation;

utils::Expected<utils::Success> cyclic::Meta::serialize(const std::filesystem::path& path, const cyclic::Meta& meta)
{
    std::ofstream file(path, std::ios::out | std::ios::trunc | std::ios::binary);

    if (!file.is_open()) {
        return utils::Error(fmt::format("Error creating file {}", path.string()));
    }

    utils::file_write(file, meta.type().datatype());
    utils::file_write(file, meta.adr().offset);
    utils::file_write(file, meta.adr().index);
    utils::file_write(file, meta.type().size());

    return utils::Success {};
}

utils::Expected<cyclic::Meta> cyclic::Meta::deserialize(const std::filesystem::path& path)
{
    std::ifstream file(path, std::ios::in | std::ios::binary);

    if (!file.is_open()) {
        return utils::Error(fmt::format("Error opening file {}", path.string()));
    }

    auto datatype = utils::file_read<DataType>(file);
    auto adr_offset = utils::file_read<decltype(MemoryAddress::offset)>(file);
    auto adr_index = utils::file_read<decltype(MemoryAddress::index)>(file);
    auto size = utils::file_read<std::size_t>(file);
    auto type = BaseType::type_from_name(datatype, size);

    return Meta(type, { adr_offset, adr_index });
}

cyclic::Meta::Meta(BaseType type, MemoryAddress adr)
    : _type(type)
    , _adr(adr)
    , _handle(adr.offset)
{
}

const BaseType& cyclic::Meta::type() const
{
    return _type;
}

const MemoryAddress& cyclic::Meta::adr() const
{
    return _adr;
}

memory_handle_type cyclic::Meta::handle() const
{
    return _handle;
}
