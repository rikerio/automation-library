#include "cyclic/link.h"

#include "utils/file.h"
#include <fmt/core.h>
#include <fstream>
#include <algorithm>

using namespace automation;

utils::Expected<cyclic::LinkList> cyclic::LinkList::deserialize(const std::filesystem::path& path)
{
    std::ifstream file(path, std::ios::in | std::ios::binary);

    if (!file.is_open()) {
        return LinkList {};
    }

    LinkList link_list;

    auto tmp_size = utils::file_read<cyclic::LinkList::size_type>(file);

    while (tmp_size > 0) {
        Link entry;
        entry.name = utils::file_read<std::string>(file);
        auto adr_offset = utils::file_read<decltype(MemoryAddress::offset)>(file);
        auto adr_index = utils::file_read<decltype(MemoryAddress::index)>(file);
        auto has_converter = utils::file_read<bool>(file);

        if (!has_converter) {
            entry.options = LinkOptions({ adr_offset, adr_index });
        } else {
            auto converter_name = utils::file_read<std::string>(file);
            auto converter_args = utils::file_read<std::vector<double>>(file);
            entry.options = LinkOptions({ adr_offset, adr_index }, converter_name, converter_args);
        }

        link_list.push_back(entry);
        tmp_size -= 1;
    };

    return link_list;
}

utils::Expected<utils::Success> cyclic::LinkList::serialize(const std::filesystem::path& path, const cyclic::LinkList& link_list)
{
    std::ofstream file(path, std::ios::out | std::ios::trunc | std::ios::binary);

    if (!file.is_open()) {
        return utils::Error(fmt::format("Error creating file {}", path.string()));
    }

    utils::file_write(file, link_list.size());
    std::for_each(link_list.begin(), link_list.end(), [&](auto& entry) {
        bool has_converter = entry.options.converter.has_value();

        utils::file_write(file, entry.name);
        utils::file_write(file, entry.options.offset.offset);
        utils::file_write(file, entry.options.offset.index);
        utils::file_write(file, has_converter);

        if (has_converter) {
            utils::file_write(file, entry.options.converter->name);
            utils::file_write(file, entry.options.converter->args);
        }
    });

    return utils::Success {};
}
