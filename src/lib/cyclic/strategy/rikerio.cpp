#include "cyclic/strategy/rikerio.h"

using namespace automation::cyclic::strategy;

RikerIO::RikerIO(const std::filesystem::path& socket_path, uint16_t task_id, int8_t priority)
    : _socket_path(socket_path)
    , _task_id(task_id)
    , _priority(priority)
    , _service(utils::EventService::Strategy::Fixed, 4)
    , _loop_event(utils::EventDescriptor::create())
{
    _epoll.reg(_loop_event.fd());
}

void RikerIO::setup()
{
    if (_helper_thread) {
        throw std::runtime_error("RikerIO Cyclic Strategy cannot be started twice");
    }

    _helper_thread = std::make_unique<HelperThread>(*this);
}

void RikerIO::yield()
{
    if (!_first_yield_call) {
        _helper_thread->send_done();
    }

    _first_yield_call = false;
    std::array<utils::EventPoll::Event, 4> events;
    _epoll.wait(events, -1);
    _loop_event.read();
}

void RikerIO::quit()
{
    if (!_first_yield_call) {
        _helper_thread->send_done();
    }
    _helper_thread = nullptr;
    _first_yield_call = true;
}
