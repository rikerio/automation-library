#include "cyclic/strategy/default.h"
#include "utils/scheduler.h"

using namespace automation::cyclic::strategy;

Default::Default(utils::TimerDescriptor::Duration dur)
    : _timer_event(utils::TimerDescriptor::create_cyclic(dur))
{
    _event_poll.reg(_timer_event.fd());
}

Default::~Default()
{
}

void Default::setup()
{
    utils::Scheduler::set_other();
}

void Default::yield()
{
    std::array<utils::EventPoll::Event, 1> events;
    _event_poll.wait(events);
    _timer_event.read();
}

void Default::quit()
{
}
