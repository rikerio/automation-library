#include "cyclic/strategy/realtime.h"
#include "utils/scheduler.h"

using namespace automation::cyclic::strategy;

Realtime::Realtime(Realtime::Type type, int8_t priority, utils::TimerDescriptor::Duration duration)
    : _timer_event(utils::TimerDescriptor::create_cyclic(duration))
    , _notify_event(utils::EventDescriptor::create())
    , _aux_exit_event(utils::EventDescriptor::create())
    , _type(type)
    , _priority(priority)
    , _thread(std::bind(&Realtime::aux_thread_handler, this))

{
    _aux_eventbus.reg(_timer_event.fd());
    _aux_eventbus.reg(_aux_exit_event.fd());
    _worker_eventbus.reg(_notify_event.fd());
}

Realtime::~Realtime()
{
    quit();
    _thread.join();
}

void Realtime::setup()
{
    setup_scheduler();
}

void Realtime::yield()
{
    std::array<utils::EventPoll::Event, 2> events;
    _worker_eventbus.wait(events);
    _notify_event.read();
}

void Realtime::quit()
{
    _aux_exit_event.write();
}

void Realtime::aux_thread_handler()
{
    if (_type == Type::none) {
        return;
    }

    setup_scheduler();

    while (1) {
        std::array<utils::EventPoll::Event, 2> events;
        _aux_eventbus.wait(events);

        _timer_event.read();

        if (_aux_exit_event.read()) {
            break;
        }

        _notify_event.write();
    };
}

void Realtime::setup_scheduler()
{
    if (_type == Realtime::Type::fifo) {
        utils::Scheduler::set_fifo(_priority);
    } else if (_type == Realtime::Type::rr) {
        utils::Scheduler::set_rr(_priority);
    }
}
