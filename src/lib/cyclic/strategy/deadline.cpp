#include "cyclic/strategy/deadline.h"
#include "utils/scheduler.h"

using namespace automation::cyclic::strategy;

Deadline::Deadline(uint32_t runtime, uint32_t deadline, uint32_t period)
    : _runtime(runtime)
    , _deadline(deadline)
    , _period(period)
{
}

Deadline::~Deadline()
{
    quit();
}

void Deadline::setup()
{
    utils::Scheduler::set_deadline(_runtime * 1000, _period * 1000, _deadline * 1000);
}

void Deadline::yield()
{
    sched_yield();
}

void Deadline::quit()
{
}
