#include "cyclic/storage.h"
#include "cyclic/link.h"
#include "cyclic/meta.h"
#include "cyclic/linker.h"
#include "cyclic/memory-segment.h"
#include "utils/lock-file.h"
#include "utils/file.h"
#include "application.h"
#include <fmt/core.h>
#include <fstream>
#include <random>
#include <mutex>

namespace bip = boost::interprocess;
using namespace automation;
namespace fs = std::filesystem;

namespace {

static const std::string partition_filename = "segments";
static const std::string data_folder = "data";
static const std::string link_folder = "links";
static const std::string lock_folder = "locks";
static const std::string shm_info_filename = "shm_info";

bool follows_naming_policy(const std::string& name)
{
    return std::none_of(name.begin(), name.end(), [](auto c) {
        return c == ';';
    });
}

std::string lock_id(const std::string& shm_id)
{
    return shm_id + ".lock";
}

template <typename T = std::mt19937>
auto random_generator() -> T
{
    auto constexpr seed_bytes = sizeof(typename T::result_type) * T::state_size;
    auto constexpr seed_len = seed_bytes / sizeof(std::seed_seq::result_type);
    auto seed = std::array<std::seed_seq::result_type, seed_len>();
    auto dev = std::random_device();
    std::generate_n(begin(seed), seed_len, std::ref(dev));
    auto seed_seq = std::seed_seq(begin(seed), end(seed));
    return T { seed_seq };
}

std::string create_id(std::size_t len = 16)
{
    static constexpr auto chars = "0123456789"
                                  "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                  "abcdefghijklmnopqrstuvwxyz";
    thread_local auto rng = random_generator<>();
    auto dist = std::uniform_int_distribution { {}, std::strlen(chars) - 1 };
    auto result = std::string(len, '\0');
    std::generate_n(begin(result), len, [&]() { return chars[dist(rng)]; });
    return result;
}

std::string create_unique_id()
{
    return "rio_" + create_id();
}

utils::Expected<utils::Success> serialize(const std::string& shm_name, const fs::path& path)
{
    std::ofstream file(path, std::ios::out | std::ios::trunc | std::ios::binary);

    if (!file.is_open()) {
        return utils::Error(fmt::format("Error creating file {}", path.string()));
    }

    utils::file_write(file, shm_name);

    return utils::Success {};
}

utils::Expected<std::string> deserialize(const fs::path& path)
{

    std::ifstream file(path, std::ios::in | std::ios::binary);

    if (!file.is_open()) {
        return utils::Error(fmt::format("Error opening file {}", path.string()));
    }

    std::string shm_name = utils::file_read<std::string>(file);

    return shm_name;
}

} // namespace

utils::Expected<cyclic::Storage, utils::Error> cyclic::Storage::create(const fs::path& path, size_t size)
{
    auto info_file = path / shm_info_filename;
    if (fs::exists(info_file)) {
        return utils::Error(fmt::format("shm_info file {} already exists", info_file.string()));
    }

    std::error_code ec;
    fs::create_directories(path / data_folder, ec);

    if (ec) {
        return utils::Error(fmt::format("Error creating data folder: {}", ec.message()));
    }

    fs::create_directories(path / link_folder, ec);

    if (ec) {
        return utils::Error(fmt::format("Error creating link folder: {}", ec.message()));
    }

    fs::create_directories(path / lock_folder, ec);

    if (ec) {
        return utils::Error(fmt::format("Error creating lock folder: {}", ec.message()));
    }

    auto shm_id = create_unique_id();

    try {
        bip::managed_shared_memory(bip::create_only, shm_id.c_str(), size);
    } catch (...) {
        return utils::Error(fmt::format("Error setting up managed_shared_memory {}", shm_id));
    }

    try {
        bip::named_upgradable_mutex(bip::create_only, lock_id(shm_id).c_str());
    } catch (...) {
        return utils::Error(fmt::format("Error setting up named_upgradable_mutex {}", shm_id));
    }

    serialize(shm_id, path / shm_info_filename);

    return Storage { path, shm_id };
}

utils::Expected<cyclic::Storage> cyclic::Storage::attach(const fs::path& path)
{
    if (!fs::exists(path)) {
        return utils::Error(fmt::format("Cyclic Data Storage folder at {} not found", path.string()));
    }

    auto info_path = path / shm_info_filename;
    auto shm_id = deserialize(info_path);

    if (!shm_id) {
        return utils::Error(fmt::format("Missing info file in cyclic storage folder {}", info_path.string()));
    }

    return cyclic::Storage { path, *shm_id };
}

utils::Expected<cyclic::Storage> cyclic::Storage::attach(const Application& app)
{
    return cyclic::Storage::attach(app.cyclic_data_path());
}

utils::Expected<utils::Success> cyclic::Storage::remove(const fs::path& path, bool purge)
{
    if (!fs::exists(path)) {
        return utils::Success {};
    }

    auto shm_id = deserialize(path / shm_info_filename);

    if (!shm_id) {
        return utils::Error(fmt::format("Error reading info file {}", shm_id.error().message()));
    }

    if (!bip::named_upgradable_mutex::remove(lock_id(*shm_id).c_str())) {
        return utils::Error(fmt::format("Error removing shared lock object {}", lock_id(*shm_id).c_str()));
    }

    if (!bip::shared_memory_object::remove(shm_id->c_str())) {
        return utils::Error(fmt::format("Error removing shared memory object {}", *shm_id));
    }

    std::error_code ec;
    if (purge) {
        fs::remove_all(path, ec);

        if (ec) {
            return utils::Error(ec.message());
        }
        return utils::Success {};
    }

    fs::remove_all(path / data_folder, ec);

    if (ec) {
        return utils::Error(ec.message());
    }

    if (!fs::remove(path / shm_info_filename, ec)) {
        return utils::Error(ec.message());
    }

    return utils::Success {};
}

cyclic::Storage::Storage(const fs::path& path, const std::string& shm_name)
    : _root_path(path)
    , _data_path(path / data_folder)
    , _link_path(path / link_folder)
    , _lock_path(path / lock_folder)
    , _shm_name(shm_name)
    , _shm(bip::managed_shared_memory(bip::open_only, shm_name.c_str()))
{
    _mutex = std::make_unique<bip::named_upgradable_mutex>(bip::open_only, lock_id(shm_name).c_str());
}

const fs::path& cyclic::Storage::root_path() const
{
    return _root_path;
}

const fs::path& cyclic::Storage::data_path() const
{
    return _data_path;
}

const fs::path& cyclic::Storage::link_path() const
{
    return _link_path;
}

const fs::path& cyclic::Storage::lock_path() const
{
    return _lock_path;
}

const std::string& cyclic::Storage::shm_name() const
{
    return _shm_name;
}

char* cyclic::Storage::shm() const
{
    return (char*)&_shm;
}

bip::named_upgradable_mutex& cyclic::Storage::mutex()
{
    return *_mutex;
}

void* cyclic::Storage::get_address_from_handle(memory_handle_type handle)
{
    return _shm.get_address_from_handle(handle);
}

utils::Expected<cyclic::LinkList> cyclic::Storage::link(const std::string& name) const
{
    auto link_path = _link_path / name;
    auto lock_path = _lock_path / (name + ".link");
    auto lock = utils::LockFile::shared(lock_path);
    auto guard = std::scoped_lock(lock);
    return cyclic::LinkList::deserialize(link_path);
}

utils::Expected<cyclic::LinkList> cyclic::Storage::link(const std::string& link_name, const std::string& meta_name, MemoryAddress offset) const
{
    auto link_path = _link_path / link_name;
    auto lock_path = _lock_path / (link_name + ".link");
    auto lock = utils::LockFile::exclusive(lock_path);
    auto guard = std::scoped_lock(lock);
    auto link = cyclic::LinkList::deserialize(link_path);

    if (!link) {
        return std::move(link.error());
    }

    auto new_entry = Link { meta_name, offset };
    auto entry = std::find(link->begin(), link->end(), new_entry);

    if (entry == link->end()) {
        link->push_back(new_entry);

        auto res = cyclic::LinkList::serialize(link_path, *link);

        if (!res) {
            return std::move(res.error());
        }
    }

    return std::move(*link);
}

utils::Expected<cyclic::LinkList> cyclic::Storage::link(const std::string& link_name, const std::string& meta_name, LinkOptions link_options) const
{

    auto link_path = _link_path / link_name;
    auto lock_path = _lock_path / (link_name + ".link");
    auto lock = utils::LockFile::exclusive(lock_path);
    auto guard = std::scoped_lock(lock);
    auto link = cyclic::LinkList::deserialize(link_path);

    if (!link) {
        return std::move(link.error());
    }

    auto new_entry = Link { meta_name, link_options };
    auto entry = std::find(link->begin(), link->end(), new_entry);

    if (entry == link->end()) {
        link->push_back(new_entry);

        auto res = cyclic::LinkList::serialize(link_path, *link);

        if (!res) {
            return std::move(res.error());
        }
    }

    return std::move(*link);
}

utils::Expected<cyclic::LinkList> cyclic::Storage::unlink(const std::string& link_name, const std::string& meta_name, MemoryAddress offset) const
{
    auto link_path = _link_path / link_name;
    auto lock_path = _lock_path / (link_name + ".link");
    auto lock = utils::LockFile::exclusive(lock_path);
    auto guard = std::scoped_lock(lock);
    auto link = cyclic::LinkList::deserialize(link_path);

    if (!link) {
        return std::move(link.error());
    }

    auto new_entry = Link { meta_name, offset };
    auto entry_it = std::find(link->begin(), link->end(), new_entry);

    if (entry_it != link->end()) {
        link->erase(entry_it);
        auto res = cyclic::LinkList::serialize(link_path, *link);

        if (!res) {
            return std::move(res.error());
        }
    }

    return std::move(link.value());
}

utils::Expected<cyclic::LinkList> cyclic::Storage::unlink(const std::string& link_name) const
{
    auto link_path = _link_path / link_name;
    auto lock_path = _lock_path / (link_name + ".link");
    auto lock = utils::LockFile::exclusive(lock_path);
    auto guard = std::scoped_lock(lock);

    std::error_code ec;
    if (!std::filesystem::remove(link_path, ec)) {
        return utils::Error(fmt::format("Error removing link {} : {}", link_name, ec.message()));
    }

    return LinkList {};
}

utils::Expected<utils::Success> cyclic::Storage::store(const std::string& name, const cyclic::Meta& meta) const
{
    if (follows_naming_policy(name) == false) {
        return utils::Error(fmt::format("Meta name '{}' violates naming policy", name));
    }

    auto meta_filename = data_path() / name;
    auto lock_filename = name + ".meta";
    auto lock = utils::LockFile::shared(lock_path() / lock_filename);
    auto guard = std::scoped_lock(lock);

    return cyclic::Meta::serialize(meta_filename, meta);
}

utils::Expected<cyclic::Meta> cyclic::Storage::load(const std::string& name) const
{
    auto meta_filename = data_path() / name;
    auto lock_filename = name + ".meta";
    auto lock = utils::LockFile::shared(lock_path() / lock_filename);
    auto guard = std::scoped_lock(lock);

    return cyclic::Meta::deserialize(meta_filename);
}

utils::Expected<utils::Success> cyclic::Storage::remove(const std::string& name) const
{
    auto meta_filename = data_path() / name;
    auto lock_filename = name + ".meta";
    auto lock = utils::LockFile::exclusive(lock_path() / lock_filename);
    auto guard = std::scoped_lock(lock);

    std::error_code ec;
    if (!std::filesystem::remove(meta_filename, ec)) {
        return utils::Error(ec.message());
    }

    return utils::Success {};
}

utils::Expected<cyclic::Meta> cyclic::Storage::allocate(const std::string& name, BaseType type)
{
    auto ptr = allocate(type.size());

    if (!ptr) {
        return std::move(ptr.error());
    }

    cyclic::Meta meta(type, _shm.get_handle_from_address(*ptr));

    auto exp = store(name, meta);

    if (!exp) {
        return std::move(exp.error());
    }
    return meta;
}

utils::Expected<cyclic::Meta> cyclic::Storage::allocate(const std::string& name, byte_size_type size)
{
    auto ptr = allocate(size);

    if (!ptr) {
        return std::move(ptr.error());
    }

    cyclic::Meta meta(Type<Buffer>(size), _shm.get_handle_from_address(*ptr));

    auto exp = store(name, meta);

    if (exp) {
        return meta;
    }

    return std::move(exp.error());
}

utils::Expected<void*> cyclic::Storage::allocate(byte_size_type size)
{
    char* ptr = (char*)_shm.allocate(size, std::nothrow);

    if (!ptr) {
        return utils::Error(fmt::format("Error allocating {} bytes of data", size));
    }

    return ptr;
}

utils::Expected<utils::Success> cyclic::Storage::deallocate(void* ptr)
{
    _shm.deallocate(ptr);
    return utils::Success {};
}

utils::Expected<utils::Success> cyclic::Storage::deallocate(memory_handle_type handle)
{
    _shm.deallocate(_shm.get_address_from_handle(handle));
    return utils::Success {};
}
