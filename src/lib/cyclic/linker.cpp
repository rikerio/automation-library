#include "cyclic/link.h"
#include "cyclic/linker.h"
#include "application.h"
#include <boost/interprocess/sync/sharable_lock.hpp>
#include <boost/interprocess/sync/scoped_lock.hpp>

using namespace boost::interprocess;
using namespace automation;

std::map<std::string, cyclic::ConverterFactory::SetupHandler> cyclic::ConverterFactory::_setup_map;

cyclic::Linker::Linker(
    cyclic::Storage& storage,
    const std::string& prefix)
    : _prefix(prefix)
    , _storage(storage)
    , _converter_factory(storage)
    , _job_factory(storage)
{
}

void cyclic::Linker::pre_loop()
{
    sharable_lock<named_upgradable_mutex> lock(_storage.mutex());

    std::for_each(_job_list.begin(), _job_list.end(), [](auto& job) {
        job->execute_read();
    });
}

void cyclic::Linker::post_loop()
{
    scoped_lock<named_upgradable_mutex> lock(_storage.mutex());

    std::for_each(_job_list.begin(), _job_list.end(), [](auto& job) {
        job->execute_write();
    });
}

void cyclic::Linker::quit()
{
    post_loop();
}
