#include "cyclic/thread.h"
#include <string.h>
#include <unistd.h>

using namespace automation::cyclic;

Thread::Thread(std::unique_ptr<Strategy> strategy)
    : _strategy(std::move(strategy))
    , _running(false)
{
    _emitter.on(Thread::Event::Loop, std::bind(&Thread::Task::loop, &_task));
    _emitter.on(Thread::Event::Quit, std::bind(&Thread::Task::stop, &_task));
    _emitter.on(Thread::Event::Quit, std::bind(&Thread::Task::loop, &_task));
}

Thread::~Thread()
{
    stop();
}

void Thread::start()
{
    if (_running) {
        throw std::runtime_error("CyclicThread cannot be started twice.");
    }

    _running = true;
    _thread_ptr = std::make_unique<std::thread>(std::bind(&Thread::thread_handler, this));
}

void Thread::stop()
{
    if (!_thread_ptr) {
        return;
    }

    if (!_running.load()) {
        return;
    }

    _running = false;
    if (_thread_ptr->joinable()) {
        _thread_ptr->join();
    }
}

void Thread::thread_handler()
{
    _strategy->setup();

    while (_running) {
        _strategy->yield();
        _emitter.emit(Event::Loop);
    }

    _strategy->quit();
    _emitter.emit(Event::Quit);
}
