#include "cyclic/thread.h"
#include "utils/scheduler.h"

using namespace automation::cyclic;

Thread::Task::Task()
    : _init_event(utils::EventDescriptor::create())
    , _done_event(utils::EventDescriptor::create())
    , _init_thread(std::bind(&Task::init, this))
{
}

Thread::Task::~Task()
{
    _service.stop();
    _init_thread.join();
}

void Thread::Task::loop()
{
    State next_state = _state;
    auto init_done = _done_event.read();

    switch (_state) {

    case (State::Start):

        emit(Event::PreLoop);
        _init_event.write();

        next_state = State::Init;
        break;

    case (State::Init):

        if (!init_done) {
            break;
        }

        next_state = State::Loop;

        emit(Event::PreLoop);
        emit(Event::Loop);
        emit(Event::PostLoop);

        break;

    case (State::Loop):

        if (_stop_request) {
            _stop_request = false;
            next_state = State::Stopped;
            emit(Event::PreLoop);
            emit(Event::Quit);
            emit(Event::PostLoop);
            break;
        }

        emit(Event::PreLoop);
        emit(Event::Loop);
        emit(Event::PostLoop);

        break;

    case (State::Stopped):

        if (_start_request) {
            _start_request = false;
            break;
        }
    };

    _state = next_state;
}

void Thread::Task::stop()
{
    _stop_request = true;
}

void Thread::Task::init()
{
    utils::Scheduler::set_other();

    _service.register_handler(_init_event.fd(), [&]() {
        if (_init_event.read()) {
            emit(Event::Init);
            _done_event.write();
        }
    });

    _service.start();
}
