#include "cyclic/provider.h"
#include <algorithm>
#include <mutex>
#include <boost/interprocess/sync/sharable_lock.hpp>

namespace bip = boost::interprocess;
using namespace automation::cyclic;

Provider::Provider(Storage& storage, const std::string& prefix)
    : _storage(storage)
    , _prefix(prefix)
{
}

void Provider::pre_loop()
{
    bip::sharable_lock lock(_storage.mutex());
    std::for_each(_job_list.begin(), _job_list.end(), [](auto& job) {
        job->execute_read();
    });
}

void Provider::post_loop()
{
    std::scoped_lock lock(_storage.mutex());

    std::for_each(_job_list.begin(), _job_list.end(), [](auto& job) {
        job->execute_write();
    });
}
