#include "application.h"

#include "cyclic/storage.h"
#include "acyclic/storage.h"
#include "utils/lock-file.h"
#include <fmt/core.h>
#include <mutex>
#include <filesystem>
#include <fstream>

using namespace automation;

namespace {

static const std::string base = "rikerio";
static const std::string acyclic_folder = "acyclic";
static const std::string cyclic_folder = "cyclic";
static const std::string status_filename = "state";

std::string _getenv(const std::string& key)
{
    const char* env_value = ::getenv(key.c_str());
    if (env_value == nullptr || strlen(env_value) == 0) {
        return {};
    }
    return std::string(env_value);
}

} // namespace

std::filesystem::path Application::build_root_path(const std::string& name)
{
    const std::string custom_folder = _getenv("RIO_FOLDER");
    if (custom_folder.length() > 0) {
        return std::filesystem::path(custom_folder) / base / name;
    }

    const std::string default_folder = _getenv("XDG_DATA_DIR");
    if (default_folder.length() > 0) {
        return std::filesystem::path(default_folder) / base / name;
    }

    return std::filesystem::temp_directory_path() / base / name;
}

utils::Expected<Application, utils::Error> Application::create(const std::string& name, Application::Config config)
{
    auto root_path = build_root_path(name);

    if (exists(name)) {
        return utils::Error(fmt::format("Application root folder ({}) already exists", root_path.string()));
    }

    std::error_code ec;
    if (!std::filesystem::create_directories(root_path, ec)) {
        return utils::Error(fmt::format("Error creating application root path: {}", ec.message()));
    }

    auto cyclic_storage = cyclic::Storage::create(root_path / cyclic_folder, config.shared_memory_size);

    if (!cyclic_storage) {
        return std::move(cyclic_storage.error());
    }

    auto acyclic_storage = acyclic::Storage::create(root_path / acyclic_folder);

    if (!acyclic_storage) {
        return std::move(acyclic_storage.error());
    }

    std::ofstream(root_path / status_filename);

    return Application { name };
}

utils::Expected<Application> Application::attach(const std::string& name)
{
    if (!exists(name)) {
        return utils::Error(fmt::format("Application '{}' not found", name));
    }

    return Application(name);
}

utils::Expected<utils::Success> Application::remove(const std::string& name, bool purge)
{
    if (!exists(name)) {
        return utils::Error(fmt::format("Application with name {} does not exist", name));
    }

    auto root_path = build_root_path(name);
    std::error_code ec;

    std::filesystem::remove(root_path / status_filename, ec);

    if (ec) {
        return utils::Error(ec.message());
    }

    auto purge_cyclic = cyclic::Storage::remove(root_path / cyclic_folder, purge);

    if (!purge_cyclic) {
        return utils::Error(purge_cyclic.error().message());
    }

    if (purge) {

        auto purge_acyclic = acyclic::Storage::remove(root_path / acyclic_folder);

        if (!purge_acyclic) {
            return utils::Error(purge_acyclic.error().message());
        }

        std::filesystem::remove_all(root_path, ec);

        if (ec) {
            return utils::Error(ec.message());
        }
    }

    return acyclic::Storage::remove(root_path / acyclic_folder);
}

bool Application::exists(const std::string& name)
{
    return std::filesystem::exists(build_root_path(name) / status_filename);
}

Application::Application(const std::string& name)
    : _name(name)
    , _root_path(build_root_path(name))
    , _cyclic_data_path(_root_path / cyclic_folder)
    , _acyclic_data_path(_root_path / acyclic_folder)
{
}

Application::Application(Application&& other)
{
    swap(other);
}

Application& Application::operator=(Application&& other)
{
    swap(other);
    return *this;
}

const std::string& Application::name() const
{
    return _name;
}

const std::filesystem::path& Application::root_path() const
{
    return _root_path;
}

const std::filesystem::path& Application::cyclic_data_path() const
{
    return _cyclic_data_path;
}

const std::filesystem::path& Application::acyclic_data_path() const
{
    return _acyclic_data_path;
}

void Application::swap(Application& other)
{
    std::swap(_name, other._name);
    std::swap(_root_path, other._root_path);
    std::swap(_cyclic_data_path, other._cyclic_data_path);
    std::swap(_acyclic_data_path, other._acyclic_data_path);
}
