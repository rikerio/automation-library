#include "memory_address.h"

using namespace automation;

MemoryAddress::MemoryAddress()
    : offset(0)
    , index(0)
{
}

MemoryAddress::MemoryAddress(byte_size_type _offset, uint16_t _index)
    : offset(_offset)
    , index(_index)
{
    offset += _index / 8;
    index = _index % 8;
}

bool MemoryAddress::equals(const MemoryAddress& other) const
{
    return offset == other.offset && index == other.index;
}

bool operator==(const MemoryAddress& a, const MemoryAddress& b)
{
    return a.equals(b);
}

MemoryAddress operator+(const MemoryAddress& a, const MemoryAddress& b)
{
    return MemoryAddress { a.offset + b.offset, uint16_t(a.index + b.index) };
}
