#include "master.h"

using namespace automation;

SwitchMaster::SwitchMaster(cyclic::Provider& provider)
    : _signal({ SIGUSR1 })
{
    provider.create(out_switch, "out.switch");
    provider.create(in_bulb, "in.bulb");
}

void SwitchMaster::init()
{
    printf("Starting master a pid %d\n", ::getpid());
}

void SwitchMaster::loop()
{
    if (_signal.read()) {
        printf("received signal, switching %d\n", cntr % 2 == 0);
        out_switch.value((cntr % 2) == 0);
        cntr += 1;
    }

    if (in_bulb.changed()) {
        printf("Light bulb state changed to %d\n", in_bulb.value());
    }
}
