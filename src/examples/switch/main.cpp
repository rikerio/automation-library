
#include <unistd.h>
#include <getopt.h>
#include <iostream>

#include "automation.h"
#include "master.h"
#include "task.h"

using namespace std;
using namespace automation;

class ScopedApplication {
public:
    ScopedApplication()
        : _name { "switch_example_" + std::to_string(::getpid()) }
    {
        auto exp = Application::create(_name, { 4096 });

        if (!exp) {
            throw std::runtime_error(exp.error().message());
        }

        _app = exp.value();
    }

    ~ScopedApplication()
    {
        Application::remove(_name);
    }

    Application& app()
    {
        return _app;
    }

private:
    std::string _name;
    Application _app;
};

int main(int argc, char** argv)
{
    ScopedApplication scoped_app;

    auto sig_user = utils::Signals::block({ SIGUSR1, SIGTERM, SIGINT });
    auto storage = cyclic::Storage::attach(scoped_app.app());

    if (!storage) {
        std::cerr << storage.error().message() << std::endl;
        std::exit(EXIT_FAILURE);
    }

    // setting up links
    storage->link("in.switch", "out.switch");
    storage->link("out.bulb", "in.bulb");

    // setting up provider and linker
    cyclic::Provider provider(*storage);
    cyclic::Linker linker(*storage);

    // setting up master and task
    SwitchMaster master(provider);
    SwitchTask task(linker);

    auto thread = cyclic::Thread::create<cyclic::strategy::Default>(100ms);
    thread.use(provider);
    thread.use(linker);
    thread.use(master);
    thread.use(task);
    thread.start();

    utils::SignalDescriptor({ SIGTERM, SIGINT }, SFD_CLOEXEC).read();

    return EXIT_SUCCESS;
}
