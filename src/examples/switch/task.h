#pragma once
#include "automation.h"

using namespace automation;

class SwitchTask {

public:
    SwitchTask(cyclic::Linker& linker);

    void loop();

private:
    cd::ro::Bit in_switch;
    cd::wo::Bit out_bulb;
};
