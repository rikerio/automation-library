#pragma once

#include "automation.h"

using namespace automation;

class SwitchMaster {
public:
    SwitchMaster(cyclic::Provider& provider);

    void init();
    void loop();

private:
    cd::wo::Bit out_switch;
    cd::ro::Bit in_bulb;

    uint64_t cntr = 0;
    utils::SignalDescriptor _signal;
};
