#include "task.h"

using namespace automation;

SwitchTask::SwitchTask(cyclic::Linker& linker)
{
    linker.link("in.switch", in_switch);
    linker.link("out.bulb", out_bulb);
}

void SwitchTask::loop()
{
    if (!in_switch.linked()) {
        out_bulb.value(false);
        return;
    }

    if (in_switch.changed()) {
        out_bulb.value(!out_bulb.value());
        printf("Bulb state changed to %d\n", out_bulb.value());
    }
}
