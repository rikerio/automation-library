# Example for Analog Data Convertion

The cyclic linker can convert analog IO Data into floats and doubles. The analog data representation is highly dependent on the specific IO Data. There a Converter can be setup and the task need only use a double or a float for the representation.

When linking the cyclic data a converter musst be specified by name and with the converter specific arguments.
