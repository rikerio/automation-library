
#include <unistd.h>
#include <getopt.h>
#include <iostream>
#include <cmath>

#include "automation.h"

using namespace std;
using namespace automation;

namespace {

class TestConverter : public cyclic::AbstractConverter {
public:
    TestConverter(double min, double max)
        : _min(min)
        , _max(max)
    {
    }

    double convert(void* source) const
    {
        int16_t* source_value = (int16_t*)source;

        double tmp = (double)*source_value / (double)0x7FFF;
        return tmp * (_max - _min) + _min;
    }

    void deconvert(double source, void* target) const
    {
        double tmp_a = (source - _min) / (_max - _min);
        double tmp_b = tmp_a * (double)0x7fff;
        int16_t* target_value = (int16_t*)target;

        *target_value = tmp_b;
    }

private:
    double _min = 0.0f;
    double _max = 0.0f;
};

}

class AnalogMaster {
public:
    AnalogMaster(cyclic::Provider& provider)
        : _converter(0.0f, 10.0f)
    {
        provider.create(out_analog, "out.analog");
    }

    void loop()
    {
        float value = std::abs((std::sin(degree)) * 10.0f); // always positive

        printf("Master value %f\n", value);

        int16_t tmp_out = 0;

        degree += 0.1f;

        _converter.deconvert(value, &tmp_out);
        out_analog.value(tmp_out);
    }

private:
    float degree = 0.0f;
    TestConverter _converter;
    cd::wo::Int16 out_analog;
};

class AnalogTask {

public:
    AnalogTask(cyclic::Linker& linker)
    {
        linker.link("in.analog", in_analog);
    }

    void loop()
    {
        printf(" - Task value %f\n", in_analog.value());
    }

private:
    cd::ro::Float in_analog;
};

class ScopedApplication {
public:
    ScopedApplication()
        : _name { "analog_example_" + std::to_string(::getpid()) }
    {
        auto exp = Application::create(_name, { 4096 });

        if (!exp) {
            throw std::runtime_error(exp.error().message());
        }

        _app = exp.value();
    }

    ~ScopedApplication()
    {
        Application::remove(_name);
    }

    Application& app()
    {
        return _app;
    }

private:
    std::string _name;
    Application _app;
};

int main(int argc, char** argv)
{
    ScopedApplication scoped_app;

    auto sig_user = utils::Signals::block({ SIGTERM, SIGINT });
    auto storage = cyclic::Storage::attach(scoped_app.app());

    if (!storage) {
        std::cerr << storage.error().message() << std::endl;
        std::exit(EXIT_FAILURE);
    }

    // adding converter (this will be done automatically later on by
    // dynamic linking)

    using converter_ptr = std::unique_ptr<cyclic::AbstractConverter>;
    auto converter = [](const std::vector<double>& args) -> converter_ptr {
        return std::make_unique<TestConverter>(args[0], args[1]);
    };

    cyclic::ConverterFactory::setup("test", converter);

    // setting up links

    automation::cyclic::LinkOptions analog_options {
        0,
        "test",
        std::vector<double> { 0.0f, 10.0f }
    };

    storage->link("in.analog", "out.analog", analog_options);

    // setting up provider and linker

    cyclic::Provider provider(*storage);
    cyclic::Linker linker(*storage);

    // setting up master and task

    AnalogMaster master(provider);
    AnalogTask task(linker);

    auto thread_master = cyclic::Thread::create<cyclic::strategy::Default>(100ms);
    auto thread_task = cyclic::Thread::create<cyclic::strategy::Default>(100ms);

    thread_master.use(provider);
    thread_master.use(master);
    thread_master.start();

    sleep(1);

    thread_task.use(linker);
    thread_task.use(task);
    thread_task.start();

    utils::SignalDescriptor({ SIGTERM, SIGINT }, SFD_CLOEXEC).read();

    return EXIT_SUCCESS;
}
