#include <unistd.h>
#include <getopt.h>
#include <iostream>
#include <random>

#include "automation.h"

using namespace std;
using namespace automation;

uint32_t create_random(uint32_t max)
{
    static std::random_device rd;
    std::mt19937::result_type seed = rd();
    std::mt19937 gen(seed);
    std::uniform_int_distribution<uint32_t> distrib(0, max);
    return distrib(gen);
}

class SwitchMaster {
public:
    SwitchMaster(cyclic::Provider& provider)
    {
        provider.create(out_value, "out.value");
    }

    void loop()
    {
        auto random_number = create_random(32000);
        out_value.value(random_number);
    }

private:
    cd::wo::UInt32 out_value;
};

class SwitchTask {

public:
    SwitchTask(cyclic::Linker& cd_linker, acyclic::Linker& ad_linker, float treshold)
        : treshold(treshold)
    {
        cd_linker.link("in.value", in_value);
        ad_linker.link("counter", counter);
    }

    void loop()
    {
        if (in_value.value() > treshold) {
            counter.value(counter.value() + 1);
            printf("hit no %d : %d > %f\n", counter.value(), in_value.value(), treshold);
        }
    }

private:
    cd::ro::UInt32 in_value;
    ad::rw::UInt32 counter;

    float treshold = 0.0f;
};

class ScopedApplication {
public:
    ScopedApplication()
        : _name { "acyclic_example_" + std::to_string(::getpid()) }
    {
        auto exp = Application::create(_name, { 4096 });

        if (!exp) {
            throw std::runtime_error(exp.error().message());
        }

        _app = exp.value();
    }

    ~ScopedApplication()
    {
        Application::remove(_name);
    }

    Application& app()
    {
        return _app;
    }

private:
    std::string _name;
    Application _app;
};

int main(int argc, char** argv)
{
    ScopedApplication scoped_app;

    auto sig_user = utils::Signals::block({ SIGTERM, SIGINT });
    auto cyclic_storage = cyclic::Storage::attach(scoped_app.app());

    if (!cyclic_storage) {
        std::cerr << cyclic_storage.error().message() << std::endl;
        std::exit(EXIT_FAILURE);
    }

    auto acyclic_storage = acyclic::Storage::attach(scoped_app.app());

    if (!acyclic_storage) {
        std::cerr << acyclic_storage.error().message() << std::endl;
        std::exit(EXIT_FAILURE);
    }

    // setting up links
    cyclic_storage->link("in.value", "out.value");

    // setting up provider and linker
    cyclic::Provider provider(*cyclic_storage);
    cyclic::Linker cd_linker(*cyclic_storage);
    acyclic::Linker ad_linker(*acyclic_storage);

    // setting up master and task
    SwitchMaster master(provider);
    SwitchTask task(cd_linker, ad_linker, 16000);

    auto thread = cyclic::Thread::create<cyclic::strategy::Default>(100ms);
    thread.use(provider);
    thread.use(cd_linker);
    thread.use(ad_linker);
    thread.use(master);
    thread.use(task);
    thread.start();

    utils::SignalDescriptor({ SIGTERM, SIGINT }, SFD_CLOEXEC).read();

    thread.stop();

    sleep(1);

    auto meta = acyclic_storage->load("counter");
    uint32_t tmp = 0;
    std::memcpy(&tmp, meta->buffer().data(), meta->type().size());

    std::cout << "counter value = " << tmp << std::endl;

    return EXIT_SUCCESS;
}
