#include "statemachine.h"
#include "utils/log.h"

using namespace automation::scheduler;

StateMachine::Config::Config(std::initializer_list<std::vector<uint16_t>> _init_list)
    : std::vector<std::vector<uint16_t>>(_init_list)
{
}

std::size_t StateMachine::Config::task_count() const
{
    return _task_ids.size();
};

std::size_t StateMachine::Config::sprint_count() const
{
    return size();
}

void StateMachine::Config::add_lap()
{
    emplace_back();
}

void StateMachine::Config::add_task(uint16_t id)
{
    if (sprint_count() == 0) {
        throw std::runtime_error("No sprint in statemachine config when adding task");
    }

    if (_task_ids.find(id) != _task_ids.end()) {
        throw std::runtime_error("Task ID inserted twice.");
    }

    back().push_back(id);
    _task_ids.insert(id);
}

StateMachine::StateMachine(StateMachine::Config config)
    : _config(std::move(config))
    , _sprint(_config)
    , _cur_lap(_sprint.end())
{
}

void StateMachine::start()
{
    Log::debug("scheduler::StateMachine starting new sprint");

    _sprint.mark_stale_tasks(
        std::bind(
            &EventMap::execute,
            &_event_map,
            Event::Stale,
            std::placeholders::_1));

    _sprint.reset();

    _cur_lap = _sprint.begin();
    notify();
}

void StateMachine::attach(uint16_t task_id)
{
    _sprint.for_each_task([&task_id](Task& task) {
        if (task.id == task_id && task.state == StateMachine::State::Offline) {
            task.state = StateMachine::State::Ready;
        }
    });
}

void StateMachine::detach(uint16_t task_id)
{
    _sprint.for_each_task([&](Task& task) {
        if (task.id != task_id) {
            return;
        }
        if (task.state == StateMachine::State::Offline) {
            return;
        }

        if (task.state == StateMachine::State::Ready) {
            task.state = StateMachine::State::Offline;
            return;
        }

        if (task.state == StateMachine::State::Done) {
            task.state = StateMachine::State::Offline;
            return;
        }

        report_done(task_id);
    });
}

void StateMachine::report_done(uint16_t id)
{
    auto next = _cur_lap + 1;

    _sprint.reset_task(id);

    if (next == _sprint.end() || !_cur_lap->tasks_done()) {
        return;
    }

    _cur_lap = next;
    notify();
}

void StateMachine::on(Event event, Handler handler)
{
    _event_map.add(event, handler);
}

StateMachine::Task::Task(uint16_t id, State state)
    : id(id)
    , state(state)
{
}

void StateMachine::EventMap::add(Event event, Handler handler)
{
    insert(std::make_pair(event, handler));
}

void StateMachine::EventMap::execute(Event event, uint16_t id)
{
    std::for_each(lower_bound(event), upper_bound(event), [&id](auto& it) {
        it.second(id);
    });
}

bool StateMachine::Lap::tasks_done() const
{
    long unsigned int count = std::count_if(tasks.begin(), tasks.end(), [&](const Task& task) {
        return task.state == StateMachine::State::Done || task.state == StateMachine::State::Stale || task.state == StateMachine::State::Offline;
    });

    return count == tasks.size();
}

StateMachine::Sprint::Sprint(const Config& config)
{
    std::for_each(config.begin(), config.end(), [&](auto& vector) {
        Lap current_lap;
        std::for_each(vector.begin(), vector.end(), [&](uint16_t id) {
            current_lap.tasks.emplace_back(id, StateMachine::State::Offline);
        });
        push_back(current_lap);
    });
}

void StateMachine::Sprint::mark_stale_tasks(Handler handler)
{
    for_each_task([&](auto& task) {
        if (task.state == StateMachine::State::InLoop) {
            Log::debug("scheduler::StateMachine marking task {} as stale", task.id);
            task.state = StateMachine::State::Stale;
            handler(task.id);
        }
    });
}

void StateMachine::Sprint::reset_task(uint16_t task_id)
{
    for_each_task([&task_id](Task& task) {
        if (task.id == task_id) {
            task.state = StateMachine::State::Done;
        }
    });
}

void StateMachine::Sprint::for_each_task(std::function<void(Task&)> handler)
{
    std::for_each(begin(), end(), [&handler](auto& lap) {
        std::for_each(lap.tasks.begin(), lap.tasks.end(), [&handler](auto& task) {
            handler(task);
        });
    });
}

void StateMachine::Sprint::reset()
{
    for_each_task([](auto& task) {
        if (task.state == StateMachine::State::Offline) {
            return;
        }

        if (task.state == StateMachine::State::Stale) {
            return;
        }

        task.state = StateMachine::State::Ready;
    });
}

void StateMachine::notify()
{
    while (_cur_lap != _sprint.end()) {

        if (_cur_lap->tasks_done()) {
            _cur_lap += 1;
            continue;
        }

        std::for_each(_cur_lap->tasks.begin(), _cur_lap->tasks.end(), [&](Task& task) {
            if (task.state == StateMachine::State::Ready) {
                task.state = StateMachine::State::InLoop;
                _event_map.execute(Event::Loop, task.id);
            }
        });

        break;
    }
}
