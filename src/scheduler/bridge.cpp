#include "bridge.h"
#include <stdexcept>

using namespace automation::scheduler;

Bridge::Bridge(scheduler::AbstractServer& server, std::size_t size)
    : _server(server)
    , _list(size)
{
    using namespace std::placeholders;

    server.on(
        scheduler::Server::Event::NewTask,
        std::bind(&Bridge::_handle_event, this, scheduler::Server::Event::NewTask, _1, _2));

    server.on(
        scheduler::Server::Event::TaskDone,
        std::bind(&Bridge::_handle_event, this, scheduler::Server::Event::TaskDone, _1, _2));

    server.on(
        scheduler::Server::Event::TaskExited,
        std::bind(&Bridge::_handle_event, this, scheduler::Server::Event::TaskExited, _1, _2));
}

void Bridge::send_loop_request(int task_id)
{
    auto fd = get_fd(task_id);

    if (fd == -1) {
        return;
    }

    _server.send_loop_request(fd);
}

uint16_t Bridge::get_task_id(int fd)
{
    auto it = std::find_if(_list.begin(), _list.end(), [&](auto& entry) {
        return entry.fd == fd;
    });

    if (it == _list.end()) {
        return 0;
    }

    return it->task_id;
}

int Bridge::get_fd(int task_id)
{
    auto it = std::find_if(_list.begin(), _list.end(), [&](auto& entry) {
        return entry.task_id == task_id;
    });

    if (it == _list.end()) {
        return -1;
    }

    return it->fd;
}

void Bridge::_assign(int fd, uint16_t task_id)
{
    auto it = _list.begin();
    while (it++ != _list.end()) {
        if (it->fd == -1) {
            *it = { fd, task_id };
            return;
        }
    }

    throw std::runtime_error("scheduler::Bridge out of free translation slots");
}

void Bridge::_unassign(int fd, uint16_t task_id)
{
    auto it = _list.begin();
    while (it++ != _list.end()) {
        if (it->fd == fd && it->task_id == task_id) {
            *it = {};
            break;
        }
    }
}

void Bridge::_handle_event(scheduler::Server::Event event, int fd, scheduler::Message msg)
{
    if (event == scheduler::Server::Event::NewTask) {
        _assign(fd, msg.argument);
        emit(scheduler::Server::Event::NewTask, msg.argument);
    } else {
        auto task_id = get_task_id(fd);
        if (task_id == 0) {
            return;
        }
        emit(event, task_id);
        if (event == scheduler::Server::Event::TaskExited) {
            _unassign(fd, task_id);
        }
    }
}
