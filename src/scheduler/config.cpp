#include "config.h"
#include <yaml-cpp/yaml.h>
#include <stdint.h>

using namespace automation::scheduler;

utils::Expected<StateMachine::Config> ConfigParser::parse_file(const std::filesystem::path& path)
{
    YAML::Node config = YAML::LoadFile(path);

    StateMachine::Config result;

    auto laps_node = config["laps"];

    if (!laps_node.IsSequence()) {
        return utils::Error("Laps Node is not a sequence");
    }

    for (std::size_t lap_index = 0; lap_index < laps_node.size(); lap_index += 1) {
        auto lap_node = laps_node[lap_index];
        result.add_lap();

        if (lap_node.IsScalar()) {
            try {
                auto task_id = lap_node.as<uint16_t>();
                result.add_task(task_id);
                continue;
            } catch (const std::exception& e) {
                return utils::Error(e.what());
            }
        } else if (lap_node.IsSequence()) {

            for (std::size_t task_index = 0; task_index < lap_node.size(); task_index += 1) {
                try {
                    auto task_node = lap_node[task_index];
                    auto task_id = task_node.as<uint16_t>();
                    result.add_task(task_id);
                    continue;
                } catch (const std::exception& e) {
                    return utils::Error(e.what());
                }
            };
        }
    };

    return result;
}
