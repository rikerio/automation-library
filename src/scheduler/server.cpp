#include "server.h"
#include "utils/log.h"

using namespace automation::scheduler;

Server::Server(utils::EventService& service, utils::UnixDomainSocket socket, std::size_t pool_size)
    : _service(service)
    , _pool(pool_size)
    , _socket(std::move(socket))
{
    _service.register_handler(
        _socket.fd(),
        std::bind(&Server::_handle_connection, this));
}

Server::~Server()
{
    _service.deregister_descriptor(_socket.fd());
}

/**
 * @brief Returns number of current connections
 */
std::size_t Server::connections() const
{
    return std::count_if(_pool.begin(), _pool.end(), [](auto& con) {
        return con.fd() != -1;
    });
}

/**
 * @brief Send loop request to connection specified by fd
 */
void Server::send_loop_request(int fd)
{
    auto it = _find_by_fd(fd);

    if (it == _pool.end()) {
        return;
    }

    Log::debug("scheduler::Server sending 'loop' message to fd {}", fd);

    it->write(Message { Function::Loop, 0 });
}

void Server::_handle_connection()
{
    while (1) {
        int connection_fd = _socket.accept();
        if (connection_fd == -1) {
            return;
        }

        Log::debug("scheduler::Server handling new connection with fd {}", connection_fd);

        auto& connection = _emplace_connection(connection_fd);
        _service.register_handler(
            connection_fd,
            std::bind(&Server::_handle_message, this, std::ref(connection)));
    }
}

void Server::_handle_message(utils::UnixDomainSocket::Connection& connection)
{
    do {
        auto msg = connection.read<Message>();

        if (msg.type() == utils::DescriptorEvent<Message>::Type::closed) {
            Log::debug("scheduler::Server received close message from fd {}", connection.fd());

            _service.deregister_descriptor(connection.fd());
            _reset_connection(connection.fd());
            emit(Event::TaskExited, connection.fd(), Message {});

            return;
        }

        if (msg.type() == utils::DescriptorEvent<Message>::Type::again) {
            Log::debug("scheduler::Server received no new message from fd {}", connection.fd());

            break;
        }

        if (msg.type() == utils::DescriptorEvent<Message>::Type::error) {
            Log::debug("scheduler::Server received error while reading message from fd {}", connection.fd());

            break;
        }

        if (msg.value().fc == Function::Assign) {
            Log::debug("scheduler::Server received 'assign' message from fd {}", connection.fd());

            emit(Event::NewTask, connection.fd(), msg.value());
        } else if (msg.value().fc == Function::Done) {
            Log::debug("scheduler::Server received 'done' message from fd {}", connection.fd());

            emit(Event::TaskDone, connection.fd(), msg.value());
        }
    } while (1);
}

void Server::_reset_connection(int fd)
{
    auto it = _find_by_fd(fd);

    if (it == _pool.end()) {
        return;
    }

    *it = utils::UnixDomainSocket::Connection {};
}

Server::ConnectionPool::iterator Server::_find_by_fd(int fd)
{
    return std::find_if(_pool.begin(), _pool.end(), [&fd](auto& con) {
        return con.fd() == fd;
    });
}

utils::UnixDomainSocket::Connection& Server::_emplace_connection(int fd)
{
    auto it = _pool.begin();

    while (it++ != _pool.end()) {
        if (it->fd() == -1) {
            *it = utils::UnixDomainSocket::Connection(fd);
            return *it;
        }
    }

    throw std::runtime_error("scheduler::Server Connection Pools size to small");
}
