#include "automation.h"
#include "statemachine.h"
#include "bridge.h"
#include "server.h"
#include "config.h"
#include <CLI/CLI.hpp>

using namespace automation;
using namespace std::chrono_literals;

namespace {

class CustomCyclicThread {
public:
    class AbstractTask {
    public:
        virtual ~AbstractTask() {};
        virtual void loop() = 0;
    };

    CustomCyclicThread(uint8_t priority, utils::TimerDescriptor::Duration duration)
        : _priority(priority)
        , _duration(duration)
        , _service(utils::EventService::Strategy::Fixed, 32)
    {
    }

    ~CustomCyclicThread()
    {
        _service.stop();
        _thread->join();
    }

    void start()
    {
        _thread = std::make_unique<std::thread>(std::bind(&CustomCyclicThread::handler, this));
    }

    template <typename T, typename... Args>
    void add(Args... args)
    {
        _task = std::make_unique<T>(_service, std::forward<Args>(args)...);
    }

private:
    uint8_t _priority;
    utils::TimerDescriptor::Duration _duration;
    utils::EventService _service;
    std::unique_ptr<std::thread> _thread;
    std::unique_ptr<AbstractTask> _task;

    void handler()
    {
        if (_priority == 0) {
            utils::Scheduler::set_other();
        } else {
            utils::Scheduler::set_rr(_priority);
        }

        auto timer = utils::TimerDescriptor::create_cyclic(_duration);

        _service.register_handler(timer.fd(), [&]() {
            timer.read();
            if (_task) {
                _task->loop();
            }
        });

        _service.start();
    }
};

class TestTask : public CustomCyclicThread::AbstractTask {
public:
    TestTask(utils::EventService& service, const std::string& prefix)
        : _prefix(prefix)
        , _timer(utils::TimerDescriptor::create_cyclic(200ms))
    {
        service.register_handler(_timer.fd(), [&]() {
            _timer.read();
            std::cout << "jojo" << std::endl;
        });
    }

    void loop() override
    {
        std::cout << _prefix << " : " << _round++ << std::endl;
    }

private:
    std::string _prefix;
    unsigned int _round = 0;
    utils::TimerDescriptor _timer;
};

class SchedulerTask : public CustomCyclicThread::AbstractTask {
public:
    // Config parse missing
    SchedulerTask(utils::EventService& service, const scheduler::StateMachine::Config& config, const std::filesystem::path& socket_path)
        : _socket_path(socket_path)
        , _server(service, utils::UnixDomainSocket(socket_path), config.task_count() * 2)
        , _bridge(_server, config.task_count() * 2)
        , _statemachine(config)
    {
        _bridge.on(scheduler::Server::Event::NewTask, [&](auto task_id) {
            _statemachine.attach(task_id);
        });

        _bridge.on(scheduler::Server::Event::TaskExited, [&](auto task_id) {
            _statemachine.detach(task_id);
        });

        _bridge.on(scheduler::Server::Event::TaskDone, [&](auto task_id) {
            _statemachine.report_done(task_id);
        });

        _statemachine.on(scheduler::StateMachine::Event::Loop, [&](auto task_id) {
            _bridge.send_loop_request(task_id);
        });
    }

    ~SchedulerTask()
    {
        std::filesystem::remove(_socket_path);
    }

    void loop()
    {
        _statemachine.start();
    }

private:
    std::filesystem::path _socket_path;
    scheduler::Server _server;
    scheduler::Bridge _bridge;
    scheduler::StateMachine _statemachine;
};
} // namespace

int main(int argc, char** argv)
{
    std::filesystem::path socket_path;
    std::filesystem::path config_path;
    unsigned long duration = 10000; // us;
    uint8_t priority = 0;

    CLI::App app("RikerIO Scheduler");
    app.add_option("-s,--socket", socket_path, "Server Socket Path")->required();
    app.add_option("-c,--config", config_path, "Configuration Name")->required();
    app.add_option("-d,--duration", duration, "Cycle Duration in us");
    app.add_option("-p,--priority", priority, "Scheduler Priority")->envname("RIO_SCHEDULER_PRIORITY");

    CLI11_PARSE(app, argc, argv);

    auto cycle_duration = std::chrono::microseconds(duration);
    auto config = scheduler::ConfigParser::parse_file(config_path);

    if (!config) {
        std::cerr << "Error parsing configuration file : " << config.error().message() << std::endl;
        return EXIT_FAILURE;
    }

    utils::SignalDescriptor signals({ SIGTERM, SIGINT }, SFD_CLOEXEC);
    utils::EventService service;

    service.register_handler(signals.fd(), [&service, &signals]() {
        signals.read();
        service.stop();
    });

    CustomCyclicThread thread(priority, cycle_duration);

    thread.add<SchedulerTask>(*config, socket_path);
    thread.start();

    auto notify_env = ::getenv("RIO_SCHEDULER_NOTIFY");
    auto notify_str = notify_env ? std::string(notify_env) : "none";

    if (notify_str == "parent") {
        ::kill(getppid(), SIGUSR1);
    }

    service.start();

    return EXIT_SUCCESS;
}
