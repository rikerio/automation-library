#include <CLI/CLI.hpp>
#include "automation.h"
#include <fmt/core.h>
#include <cstring>

using namespace automation;

namespace {

class ScopedPointer {
public:
    static ScopedPointer from_string(const std::string& value)
    {
        ScopedPointer ptr(value.length());
        ::memcpy(ptr.addr(), value.c_str(), value.length());

        return ptr;
    }

    ScopedPointer() = default;
    ScopedPointer(std::size_t size)
        : _ptr(::calloc(1, size))
        , _size(size)

    {
    }
    ~ScopedPointer()
    {
        if (!valid()) {
            return;
        }
        ::free(_ptr);
    }

    ScopedPointer(const ScopedPointer& other) = delete;
    ScopedPointer& operator=(const ScopedPointer& other) = delete;
    ScopedPointer(ScopedPointer&& other)
    {
        std::swap(_ptr, other._ptr);
        std::swap(_size, other._size);
    }
    ScopedPointer& operator=(ScopedPointer&& other)
    {
        std::swap(_ptr, other._ptr);
        std::swap(_size, other._size);
        return *this;
    };

    bool valid() const
    {
        return _ptr != nullptr;
    }

    void* addr() const
    {
        return _ptr;
    }

    std::size_t size() const
    {
        return _size;
    }

private:
    void* _ptr = nullptr;
    std::size_t _size;
};

class RuntimeInterpreter {
public:
    static std::string to_string(const BaseType& type, const std::vector<char>& buffer)
    {

        const char* ptr = std::data(buffer);

        switch (type.datatype()) {
        case (DataType::Bool):
            return std::to_string(*reinterpret_cast<const bool*>(ptr));
        case (DataType::Uint8):
            return std::to_string(*reinterpret_cast<const uint8_t*>(ptr));
        case (DataType::Int8):
            return std::to_string(*reinterpret_cast<const int8_t*>(ptr));
        case (DataType::Uint16):
            return std::to_string(*reinterpret_cast<const uint16_t*>(ptr));
        case (DataType::Int16):
            return std::to_string(*reinterpret_cast<const int16_t*>(ptr));
        case (DataType::Uint32):
            return std::to_string(*reinterpret_cast<const uint32_t*>(ptr));
        case (DataType::Int32):
            return std::to_string(*reinterpret_cast<const int32_t*>(ptr));
        case (DataType::Uint64):
            return std::to_string(*reinterpret_cast<const uint64_t*>(ptr));
        case (DataType::Int64):
            return std::to_string(*reinterpret_cast<const int64_t*>(ptr));
        case (DataType::Float):
            return std::to_string(*reinterpret_cast<const float*>(ptr));
        case (DataType::Double):
            return std::to_string(*reinterpret_cast<const double*>(ptr));
        case (DataType::String):
            return std::string(buffer.begin(), buffer.end());
        default:
            return "default";
        }
    }

    template <typename T>
    static utils::Expected<ScopedPointer> convert(const std::string& string_value)
    {
        auto long_value = strtold(string_value.c_str(), NULL);
        if (errno == EINVAL || errno == ERANGE) {
            return utils::Error(fmt::format("Error converting datatype : {}", ::strerror(errno)));
        }
        if (long_value > std::numeric_limits<T>::max()) {
            return utils::Error("Error converting : value out of bounds");
        }
        if (long_value < std::numeric_limits<T>::lowest()) {
            return utils::Error("Error converting : value out of bounds");
        }

        T tmp_value = static_cast<T>(long_value);
        ScopedPointer scoped_ptr(sizeof(T));
        ::memcpy(scoped_ptr.addr(), &tmp_value, sizeof(T));
        return scoped_ptr;
    }

    static utils::Expected<ScopedPointer> from_string(BaseType type, const std::string& string_value)
    {
        switch (type.datatype()) {
        case (DataType::Undefined):
            return utils::Error("DataType undefined cannot be interpreted.");
        case (DataType::Bit):
            return convert<bool>(string_value);
        case (DataType::Bool):
            return convert<bool>(string_value);
        case (DataType::Uint8):
            return convert<uint8_t>(string_value);
        case (DataType::Int8):
            return convert<int8_t>(string_value);
        case (DataType::Uint16):
            return convert<uint16_t>(string_value);
        case (DataType::Int16):
            return convert<int16_t>(string_value);
        case (DataType::Uint32):
            return convert<uint32_t>(string_value);
        case (DataType::Int32):
            return convert<int32_t>(string_value);
        case (DataType::Uint64):
            return convert<uint64_t>(string_value);
        case (DataType::Int64):
            return convert<int64_t>(string_value);
        case (DataType::Float):
            return convert<float>(string_value);
        case (DataType::Double):
            return convert<double>(string_value);
        case (DataType::String):
            return ScopedPointer::from_string(string_value);
        case (DataType::Buffer):
            return ScopedPointer::from_string("default");
        }

        return utils::Error(fmt::format("Error converting string {}", string_value));
    }
};

std::vector<std::filesystem::path> dir_content_to_vector(const std::filesystem::path& folder)
{
    std::vector<std::filesystem::path> result;
    auto dir_iter = std::filesystem::directory_iterator(folder);
    for (auto it : dir_iter) {
        result.push_back(*dir_iter);
    }
    return result;
}

std::string application_name("default");
std::string data_name;
std::string data_type;
std::string data_value;
bool verbose = false;

} // namespace

void rio_shs_setup(CLI::App& app)
{
    app.add_option("--app", application_name, "Application name")->envname("RIO_APPLICATION");
    app.add_flag("--verbose", verbose, "Print debug messages");

    auto list_sub = app.add_subcommand("list", "List Data");
    list_sub->callback([&]() {
        auto application = Application::attach(application_name);

        if (!application) {
            std::cout << application.error().message() << std::endl;
            std::exit(EXIT_FAILURE);
        }

        auto storage = acyclic::Storage::attach(application->acyclic_data_path());

        if (!storage) {
            std::cout << storage.error().message() << std::endl;
            std::exit(EXIT_FAILURE);
        }

        namespace fs = std::filesystem;
        auto compare = [](const fs::path& a, const fs::path& b) { return a.filename() <= b.filename(); };
        auto print = [](const fs::path& a) { std::cout << a.filename() << std::endl; };

        std::vector<fs::path> results = dir_content_to_vector(storage->data_folder());
        std::sort(results.begin(), results.end(), compare);
        std::for_each(results.begin(), results.end(), print);

        std::exit(EXIT_SUCCESS);
    });

    auto get_sub = app.add_subcommand("get", "Read Data");
    get_sub->add_option("name", data_name, "Data Name")->required();

    get_sub->callback([&]() {
        auto application = Application::attach(application_name);

        if (!application) {
            std::cout << application.error().message() << std::endl;
            std::exit(EXIT_FAILURE);
        }

        auto storage = acyclic::Storage::attach(application->acyclic_data_path());

        if (!storage) {
            std::cout << storage.error().message() << std::endl;
            std::exit(EXIT_FAILURE);
        }

        auto data = storage->load(data_name);

        if (!data) {
            std::cout << data.error().message() << std::endl;
            std::exit(EXIT_FAILURE);
        }

        std::cout
            << data_name << ","
            << data->type().name() << ","
            << RuntimeInterpreter::to_string(data->type(), data->buffer()) << std::endl;

        std::exit(EXIT_SUCCESS);
    });

    auto set_sub = app.add_subcommand("set", "Write Data");
    set_sub->add_option("name", data_name, "Data Name");
    set_sub->add_option("value", data_value, "Data Value");
    set_sub->add_option("--type", data_type, "Data Type")->required();
    set_sub->callback([&]() {
        auto application = Application::attach(application_name);

        if (!application) {
            std::cout << application.error().message() << std::endl;
            std::exit(EXIT_FAILURE);
        }

        auto storage = acyclic::Storage::attach(application->acyclic_data_path());

        if (!storage) {
            std::cout << storage.error().message() << std::endl;
            std::exit(EXIT_FAILURE);
        }

        auto type = BaseType::type_from_name(data_type);
        auto ptr = RuntimeInterpreter::from_string(type, data_value);

        if (!ptr) {
            std::cerr << ptr.error().message() << std::endl;
            std::exit(EXIT_FAILURE);
        }

        storage->store(data_name, acyclic::Data::create(type, ptr->addr()));

        std::exit(EXIT_SUCCESS);
    });

    auto remove_sub = app.add_subcommand("remove", "Remove Data");
    remove_sub->add_option(data_name, "Data Name")->required();
    remove_sub->callback([&]() {
        auto application_exp = automation::Application::attach(application_name);

        if (!application_exp) {
            std::cout << application_exp.error().message() << std::endl;
            std::exit(EXIT_FAILURE);
        }

        auto application = application_exp.value();
        auto storage = acyclic::Storage::attach(application.acyclic_data_path());

        if (!storage) {
            std::cout << storage.error().message() << std::endl;
            std::exit(EXIT_FAILURE);
        }

        if (storage->reset(data_name)) {
            std::exit(EXIT_SUCCESS);
        }

        std::exit(EXIT_FAILURE);
    });
}
