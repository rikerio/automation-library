#include <CLI/CLI.hpp>
#include "automation.h"

namespace {
std::string name = "default";
std::string size = "4k";
bool purge = false;
bool keep = false;
bool systemd = false;
bool verbose = false;
} // namespace

void rio_app_setup(CLI::App& app)
{

    auto create_sub = app.add_subcommand("create", "Create Application");
    create_sub->add_option("--name", name, "Application name")->required();
    create_sub->add_option("--shm-size", size, "Shared Memory size");
    create_sub->add_flag("--keep", keep, "Keep running and remove application on exit.");
    create_sub->add_flag("--systemd", systemd, "Reports to SystemD.")->needs("--keep");
    create_sub->add_flag("--verbose", verbose, "Print debug messages");

    create_sub->callback([&]() {
        auto application = automation::Application::create(name, { automation::utils::Size(size) });

        if (!application) {
            std::cout << application.error().message() << std::endl;
            std::exit(EXIT_FAILURE);
        }

        if (!keep) {
            std::exit(EXIT_SUCCESS);
        }

        automation::utils::EventService service;
        automation::utils::SignalDescriptor signal({ SIGINT, SIGTERM });

        service.register_handler(signal.fd(), [&]() {
            std::exit(automation::Application::remove(name) ? EXIT_SUCCESS : EXIT_FAILURE);
        });

        service.start();
    });

    auto remove_sub = app.add_subcommand("remove", "Remove Application");
    remove_sub->add_option("--name", name, "Application name")->required();
    remove_sub->add_flag("--purge", purge, "Purge Persistent Data");
    remove_sub->add_flag("--verbose", verbose, "Print debug messages");
    remove_sub->callback([&]() {
        auto remove_result = automation::Application::remove(name, purge);
        if (remove_result) {
            std::exit(EXIT_SUCCESS);
        }

        std::cout << remove_result.error().message() << std::endl;
        ::exit(EXIT_FAILURE);
    });
}
