#include <CLI/CLI.hpp>
#include <unistd.h>
#include "version.h"
#include "utils/semver.h"

extern void rio_app_setup(CLI::App& app);
extern void rio_shm_setup(CLI::App& app);
extern void rio_shs_setup(CLI::App& app);

int main(int argc, char** argv)
{
    automation::utils::SemanticVersion version("v9.9.99");

    try {
        version = automation::utils::SemanticVersion(RIO_VERSION);
    } catch (...) {
    }

    CLI::App app("RikerIO Command Line Interface.");

    app.add_subcommand("version", "Display Version")->callback([&]() {
        std::cout << version.get_version() << std::endl;
        ::exit(EXIT_SUCCESS);
    });

    auto app_sub = app.add_subcommand("app", "Create/Remove Applications");
    auto cyclic_sub = app.add_subcommand("cyclic", "Cyclic Data");
    auto acycle_sub = app.add_subcommand("acyclic", "Acyclic Data");
    auto acycle_data_sub = acycle_sub->add_subcommand("data", "Acyclic Data");

    rio_app_setup(*app_sub);
    rio_shm_setup(*cyclic_sub);
    rio_shs_setup(*acycle_data_sub);

    CLI11_PARSE(app, argc, argv);
}
