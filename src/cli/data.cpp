#include "cli/data.h"
#include "container.h"
#include "shared-storage.h"
#include "utils/types.h"

#include <iostream>

using namespace cli::container;
using namespace cli::container::data;
using namespace automation;

namespace {

class ScopedPointer {
public:
    static ScopedPointer from_string(const std::string& value)
    {

        ScopedPointer ptr(1, value.length());
        ::memcpy(ptr.addr(), value.c_str(), value.length());

        return ptr;
    }

    ScopedPointer() = default;
    ScopedPointer(size_t size, size_t count = 1)
        : _ptr(::calloc(count, size))
    {
    }
    ~ScopedPointer()
    {
        if (!is_valid()) {
            return;
        }
        ::free(_ptr);
    }

    ScopedPointer(const ScopedPointer& other) = delete;
    ScopedPointer& operator=(const ScopedPointer& other) = delete;
    ScopedPointer(ScopedPointer&& other)
    {
        std::swap(_ptr, other._ptr);
    }
    ScopedPointer& operator=(ScopedPointer&& other) = delete;

    bool is_valid() const
    {
        return _ptr != nullptr;
    }

    void* addr() const
    {
        return _ptr;
    }

private:
    void* _ptr = nullptr;
};

class RuntimeInterpreter {
public:
    static std::string to_string(const utils::Type& type, const void* ptr)
    {
        if (type.name() == "bool") {
            return std::to_string(*static_cast<const bool*>(ptr));
        } else if (type.name() == "uint8") {
            return std::to_string(*static_cast<const uint8_t*>(ptr));
        } else if (type.name() == "int8") {
            return std::to_string(*static_cast<const int8_t*>(ptr));
        } else if (type.name() == "uint16") {
            return std::to_string(*static_cast<const uint16_t*>(ptr));
        } else if (type.name() == "int16") {
            return std::to_string(*static_cast<const int16_t*>(ptr));
        } else if (type.name() == "uint32") {
            return std::to_string(*static_cast<const uint32_t*>(ptr));
        } else if (type.name() == "int32") {
            return std::to_string(*static_cast<const int32_t*>(ptr));
        } else if (type.name() == "uint64") {
            return std::to_string(*static_cast<const uint64_t*>(ptr));
        } else if (type.name() == "int64") {
            return std::to_string(*static_cast<const int64_t*>(ptr));
        } else if (type.name() == "float") {
            return std::to_string(*static_cast<const float*>(ptr));
        } else if (type.name() == "double") {
            return std::to_string(*static_cast<const double*>(ptr));
        } else if (type.name() == "string") {
            return static_cast<const char*>(ptr);
        } else {
            return "default";
        }
    }

    template <typename T>
    static ScopedPointer convert(const std::string& string_value)
    {
        auto long_value = strtold(string_value.c_str(), NULL);
        if (errno == EINVAL || errno == ERANGE) {
            return {};
        }
        if (long_value > std::numeric_limits<T>::max()) {
            return {};
        }
        if (long_value < std::numeric_limits<T>::min()) {
            return {};
        }

        T tmp_value = static_cast<T>(long_value);
        ScopedPointer scoped_ptr(sizeof(T), 1);
        ::memcpy(scoped_ptr.addr(), &tmp_value, sizeof(T));
        return scoped_ptr;
    }

    static ScopedPointer from_string(utils::Type type, const std::string& string_value)
    {
        if (type.name() == "bool") {
            return convert<bool>(string_value);
        } else if (type.name() == "uint8") {
            return convert<uint8_t>(string_value);
        } else if (type.name() == "int8") {
            return convert<int8_t>(string_value);
        } else if (type.name() == "uint16") {
            return convert<uint16_t>(string_value);
        } else if (type.name() == "int16") {
            return convert<int16_t>(string_value);
        } else if (type.name() == "uint32") {
            return convert<uint32_t>(string_value);
        } else if (type.name() == "int32") {
            return convert<int32_t>(string_value);
        } else if (type.name() == "uint64") {
            return convert<uint64_t>(string_value);
        } else if (type.name() == "int64") {
            return convert<int64_t>(string_value);
        } else if (type.name() == "float") {
            return convert<float>(string_value);
        } else if (type.name() == "double") {
            return convert<double>(string_value);
        } else if (type.name() == "string") {
            return ScopedPointer::from_string(string_value);
        }

        return {};
    }
};

}

Inspect::Inspect(CLI::App& app, cli::container::Main& main)
    : _main(main)
{
    app.callback(std::bind(&Inspect::execute, this))
        ->add_option("name", _name);
}

void Inspect::execute()
{

    if (!automation::Container::exists(_main.container_name())) {
        std::cerr << "resource not found : " << _main.container_name() << std::endl;
        exit(EXIT_FAILURE);
    }

    try {
        automation::SharedStorage shs(_main.container_name());
        auto data = shs.load(_name);

        if (data.type() == utils::Type::Undefined) {
            std::cerr << "resource not found : " << _name << std::endl;
            exit(EXIT_FAILURE);
        }

        std::cout
            << _name << ","
            << data.type().name() << ","
            << data.type().byte_size() << ","
            << data.type().count() << ","
            << RuntimeInterpreter::to_string(data.type(), data.addr()) << std::endl;
        exit(EXIT_SUCCESS);

    } catch (std::exception& e) {
        std::cerr << e.what() << std::endl;
        exit(EXIT_FAILURE);
    }
}

List::List(CLI::App& app, cli::container::Main& main)
    : _main(main)
{
    app.callback(std::bind(&List::execute, this));
}

void List::execute()
{
    try {

        if (!automation::Container::exists(_main.container_name())) {
            exit(EXIT_FAILURE);
        }

        automation::SharedStorage shs(_main.container_name());

        std::vector<std::string> results;
        for (auto& entry : std::filesystem::directory_iterator(shs.folder())) {
            const std::string& new_item = entry.path().filename();
            results.insert(std::upper_bound(results.begin(), results.end(), new_item), new_item);
        };

        std::for_each(results.begin(), results.end(), [](auto& s) {
            std::cout << s << std::endl;
        });

        exit(EXIT_SUCCESS);

    } catch (std::exception& e) {
        std::cerr << e.what() << std::endl;
        exit(EXIT_FAILURE);
    }
}

Read::Read(CLI::App& app, cli::container::Main& main)
    : _main(main)
{
    app.callback(std::bind(&Read::execute, this))
        ->add_option("name", _name)
        ->required();
}

void Read::execute()
{

    try {
        automation::SharedStorage storage(_main.container_name());

        auto data = storage.load(_name);

        if (!data.valid()) {
            std::cerr << "resource not found : " << _name << std::endl;
            exit(EXIT_FAILURE);
        }

        std::cout
            << _name << ","
            << data.type().name() << ","
            << data.type().byte_size() << ","
            << data.type().count() << ","
            << RuntimeInterpreter::to_string(data.type(), data.addr()) << std::endl;

        exit(EXIT_SUCCESS);

    } catch (std::exception& e) {
        std::cerr << e.what() << std::endl;
        exit(EXIT_FAILURE);
    }
}

Write::Write(CLI::App& app, cli::container::Main& main)
    : _main(main)
{
    auto sub = app.callback(std::bind(&Write::execute, this));
    sub->add_option("name", _name);
    sub->add_option("type", _type);
    sub->add_option("value", _value);
}

void Write::execute()
{

    try {
        automation::SharedStorage storage(_main.container_name());

        auto type = utils::Type::create(_type, 1);
        auto scoped_ptr = RuntimeInterpreter::from_string(type, _value);

        if (!scoped_ptr.is_valid()) {
            std::cerr << "String to value conversion was not successfull." << std::endl;
            exit(EXIT_FAILURE);
        }

        storage.set_data(_name, type, scoped_ptr.addr());

        ::exit(EXIT_SUCCESS);

    } catch (std::exception& e) {
        std::cerr << e.what() << std::endl;
        ::exit(EXIT_FAILURE);
    }
}
