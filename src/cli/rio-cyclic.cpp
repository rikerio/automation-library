#include <CLI/CLI.hpp>
#include "automation.h"
#include <regex>
#include <iterator>

using namespace automation;

namespace {

cyclic::LinkConverter converter_from_string(const std::string& str)
{
    static std::regex reg_all("[^\\;]+\\;", std::regex_constants::ECMAScript);
    static std::regex reg_name("^[a-zA-Z0-9\\.]+$", std::regex_constants::ECMAScript);
    static std::regex reg_arg("^-?[0-9]+(?:\\.[0-9]+)?$", std::regex_constants::ECMAScript);

    std::vector<std::string> substrings;
    std::vector<double> args;

    // check string delimiter ;

    std::sregex_iterator end;
    std::sregex_iterator begin(str.begin(), str.end(), reg_all);
    std::for_each(begin, end, [&](auto& match) {
        std::string sub = match.str();
        sub.pop_back();
        substrings.push_back(sub);
    });

    if (substrings.size() == 0) {
        throw std::runtime_error(fmt::format("Conversion string is invalid '{}'", str));
    }

    std::smatch name_match;

    if (!std::regex_match(substrings.at(0), name_match, reg_name)) {
        throw std::runtime_error(fmt::format("Error parsing conversion name '{}'", substrings.at(0)));
    }

    std::for_each(substrings.begin() + 1, substrings.end(), [&](auto& sub) {
        std::smatch arg_match;
        if (!std::regex_match(sub, arg_match, reg_arg)) {
            throw std::runtime_error(fmt::format("Error parsing argument '{}'", sub));
        }
        double arg_value = std::stod(sub);
        args.push_back(arg_value);
    });

    return cyclic::LinkConverter(substrings.at(0), args);
}

std::string application_name("default");
std::string meta_name;
std::string link_name;
std::string link_offset("0");
std::string link_convert;
bool read_sync = false;
bool verbose = false;

} // namespace

void rio_shm_setup(CLI::App& app)
{
    app.add_option("--app", application_name, "Application name")->envname("RIO_APPLICATION");
    app.add_flag("--verbose", verbose, "Print debug messages");

    auto meta_sub = app.add_subcommand("meta", "Cyclic Data Meta Information");
    auto link_sub = app.add_subcommand("link", "Cyclic Data Link Information");

    auto list_sub = meta_sub->add_subcommand("list", "List Metadata");
    list_sub->callback([&]() {
        auto application_exp = Application::attach(application_name);
        if (!application_exp) {
            std::cout << application_exp.error().message() << std::endl;
            std::exit(EXIT_FAILURE);
        }

        auto exp_storage = cyclic::Storage::attach(application_exp.value().cyclic_data_path());

        if (!exp_storage) {
            std::cout << exp_storage.error().message() << std::endl;
            std::exit(EXIT_FAILURE);
        }

        auto& storage = exp_storage.value();

        std::vector<std::string> results;
        for (auto& entry : std::filesystem::directory_iterator(storage.data_path())) {
            const std::string& new_item = entry.path().filename();
            results.insert(std::upper_bound(results.begin(), results.end(), new_item), new_item);
        };

        std::for_each(results.begin(), results.end(), [](auto& s) {
            std::cout << s << std::endl;
        });

        std::exit(EXIT_SUCCESS);
    });

    auto inspect_sub = meta_sub->add_subcommand("inspect", "Inspect Metadata");
    inspect_sub->add_option("name", meta_name, "Metadata name")->required();

    inspect_sub->callback([&]() {
        auto application_exp = Application::attach(application_name);

        if (!application_exp) {
            std::cout << application_exp.error().message() << std::endl;
            std::exit(EXIT_FAILURE);
        }

        auto exp_storage = cyclic::Storage::attach(application_exp.value().cyclic_data_path());

        if (!exp_storage) {
            std::cout << exp_storage.error().message() << std::endl;
            std::exit(EXIT_FAILURE);
        }

        auto meta = exp_storage.value().load(meta_name);

        if (!meta) {
            std::cout << meta.error().message() << std::endl;
            std::exit(EXIT_FAILURE);
        }

        std::cout
            << meta_name << ","
            << meta->type().name() << ","
            << meta->adr().offset << "."
            << (int)meta->adr().index << std::endl;

        std::exit(EXIT_SUCCESS);
    });

    auto read_sub = meta_sub->add_subcommand("read", "Read Metadata Content");
    read_sub->add_option("name", meta_name, "Metadata name")->required();
    read_sub->add_flag("--sync", read_sync, "Read data with read lock.");
    read_sub->callback([&]() {
        std::cout << "Not implemented yet" << std::endl;
        std::exit(EXIT_FAILURE);
    });

    auto list_links_sub = link_sub->add_subcommand("list", "List Links");
    list_links_sub->callback([&]() {
        auto application_exp = Application::attach(application_name);
        if (!application_exp) {
            std::cout << application_exp.error().message() << std::endl;
            std::exit(EXIT_FAILURE);
        }
        auto& application = application_exp.value();
        auto exp_storage = cyclic::Storage::attach(application.cyclic_data_path());

        if (!exp_storage) {
            std::cout << exp_storage.error().message() << std::endl;
            std::exit(EXIT_FAILURE);
        }

        auto& storage = exp_storage.value();

        std::vector<std::string> results;
        for (auto& entry : std::filesystem::directory_iterator(storage.link_path())) {
            const std::string& new_item = entry.path().filename();
            results.insert(std::upper_bound(results.begin(), results.end(), new_item), new_item);
        };

        std::for_each(results.begin(), results.end(), [](auto& s) {
            std::cout << s << std::endl;
        });

        std::exit(EXIT_SUCCESS);
    });

    auto get_link_sub = link_sub->add_subcommand("get", "Get Metadata in Link");
    get_link_sub->add_option("name", link_name, "Link Name")->required();
    get_link_sub->callback([&]() {
        auto application_exp = Application::attach(application_name);

        if (!application_exp) {
            std::cout << application_exp.error().message() << std::endl;
            std::exit(EXIT_FAILURE);
        }

        auto exp_storage = cyclic::Storage::attach(application_exp.value().cyclic_data_path());

        if (!exp_storage) {
            std::cout << exp_storage.error().message() << std::endl;
            std::exit(EXIT_FAILURE);
        }

        auto& storage = exp_storage.value();
        auto exp_link = storage.link(link_name);

        if (!exp_link) {
            std::cout << exp_link.error().message() << std::endl;
            std::exit(EXIT_FAILURE);
        }

        auto& link = exp_link.value();

        std::for_each(link.begin(), link.end(), [](auto& entry) {
            std::cout << entry.name << "+" << entry.options.offset.offset << "." << (int)entry.options.offset.index << std::endl;
        });

        std::exit(EXIT_SUCCESS);
    });

    auto set_link_sub = link_sub->add_subcommand("set", "Set Link");
    set_link_sub->add_option("linkname", link_name, "Link Name")->required();
    set_link_sub->add_option("metaname", meta_name, "Meta Name")->required();
    set_link_sub->add_option("--offset", link_offset, "Offset from Meta Memory Address in Bits.");
    set_link_sub->add_option("--convert", link_convert, "Conversion name and arguments.");
    set_link_sub->callback([&]() {
        auto application_exp = Application::attach(application_name);

        if (!application_exp) {
            std::cout << application_exp.error().message() << std::endl;
            std::exit(EXIT_FAILURE);
        }

        auto storage = cyclic::Storage::attach(application_exp.value().cyclic_data_path());

        if (!storage) {
            std::cout << storage.error().message() << std::endl;
            std::exit(EXIT_FAILURE);
        }

        MemoryAddress _offset;
        cyclic::LinkOptions link_options;

        if (link_offset != "") {
            try {
                _offset = utils::BitString::from_string(link_offset);
            } catch (std::runtime_error& e) {
                std::cout << "Error Parsing offset : " << e.what() << std::endl;
                std::exit(EXIT_FAILURE);
            }
        }

        if (link_convert != "") {
            try {
                auto converter = converter_from_string(link_convert);
                link_options = cyclic::LinkOptions(_offset, converter);
            } catch (std::runtime_error& e) {
                std::cout << "Error parsing conversion : " << e.what() << std::endl;
                std::exit(EXIT_FAILURE);
            }
        } else {
            link_options = cyclic::LinkOptions(_offset);
        }

        auto link_result = storage->link(link_name, meta_name, link_options);

        if (link_result) {
            std::exit(EXIT_SUCCESS);
        }

        std::cout << link_result.error().message() << std::endl;

        std::exit(EXIT_FAILURE);
    });

    auto remove_link_sub = link_sub->add_subcommand("remove", "Remove Metadata from Link");
    remove_link_sub->add_option("linkname", link_name, "Link Name")->required();
    remove_link_sub->add_option("metaname", meta_name, "Metadata Name");
    remove_link_sub->add_option("--offset", link_offset, "Metadata Address Bit-Offset");
    remove_link_sub->callback([&]() {
        auto application_exp = Application::attach(application_name);

        if (!application_exp) {
            std::cout << application_exp.error().message() << std::endl;
            std::exit(EXIT_FAILURE);
        }

        MemoryAddress _offset;

        try {
            _offset = utils::BitString::from_string(link_offset);
        } catch (...) {
            std::cerr << "Error Parsing offset/size." << std::endl;
        }

        auto exp_storage = cyclic::Storage::attach(application_exp.value().cyclic_data_path());

        if (!exp_storage) {
            std::cout << exp_storage.error().message() << std::endl;
            std::exit(EXIT_FAILURE);
        }

        auto& storage = exp_storage.value();

        if (meta_name == "") {
            storage.unlink(link_name);
        } else {
            storage.unlink(link_name, meta_name, _offset);
        }

        std::exit(EXIT_SUCCESS);
    });
}
