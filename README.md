# RikerIO C++ Automation Library

[![pipeline status](https://gitlab.com/rikerio/automation-library/badges/dev/v3.0/pipeline.svg)](https://gitlab.com/rikerio/automation-library/-/commits/dev/v3.0)

[![coverage report](https://gitlab.com/rikerio/automation-library/badges/dev/v3.0/coverage.svg)](https://gitlab.com/rikerio/automation-library/-/commits/dev/v3.0)

[![Latest Release](https://gitlab.com/rikerio/automation-library/-/badges/release.svg)](https://gitlab.com/rikerio/automation-library/-/releases)

The RikerIO C++ Automation Library is a Framework to help separate IO handling and buisness logic. It enables a quick start into developing scalable and testable automation application.

## Example

A very simple example using a switch and a light bulb looks like this:

```
class SwitchTask {

public:
    SwitchTask(cyclic::Linker& linker)
    {
        linker.link("in.switch", in_switch);
        linker.link("out.bulb", out_bulb);
    }

    void loop()
    {
        if (!in_switch.linked()) {
            out_bulb.value(false);
            return;
        }

        if (in_switch.changed()) {
            out_bulb.value(!out_bulb.value());
            printf("Bulb state changed to %d\n", out_bulb.value());
        }
    }

private:
    cd::ro::Bit in_switch; // read only data (ro)
    cd::wo::Bit out_bulb;  // write only data (ro)
};
```

For more details see and execute the example in the src/examples/switch folder.


## Build Instructions

```
mkdir build
meson ./build
cd build
ninja
```

## Motivation

We implemented many automation applications using common Tools like Beckhoff TwinCAT or Siemens TiA Portal and we were missing the abilities to really reuse applications or even scale them properly without changing the source code and avoiding copy paste methods. We also could simulate hardware properly or use unit and integration tests. That led us to develop our own framework for the development of automation application resulting in the current version 3 represented by this library.

## Background

The Library creates a shared memory file (located in /dev/shm) and stores meta information about the content of the shared memory. These meta data can be linked with application variables and automatically sync them in a cyclic thread.

Data coming from a IO System like GPIOs, industrial Bus Systems, IoT Devices etc. are located in separate processes and are executed separatly. In order to use the IO Data one has to link the data created by the IO System with the data needed for the task.

### Realtime ready

The Framework takes realtime application into account and avoids allocating memory once the cyclic loop starts. It also presents the developer with different realtime strategies for the executing of the cyclic thread related to the linux realtime scheduling capabilities.

## CLI Interface

In order to inspect meta data and create, edit or remove links we added a command line interface. We usually create a simple script for every application to link the IO data to our application.

Environment
RIO_APP_NAME : RikerIO Application Name
RIO_APP_TEMP_FOLDER : RikerIO Application Temp Folder
RIO_APP_PERS_FOLDER : RikerIO Application Persistent Folder

## Commands

### Create a new application

With the command  `rio app create [--name=default] [--shm-size=<size>] [--keep]` one can create a new
application. Be aware the the default location for a RikerIO Application is located in the /var/lib/rikerio folder. This can be changed by setting the RIO_FOLDER environment variable. The --keep option keeps the process alive and removes the application (--purge) once the process terminates.

### Remove a application

A application can be removed by using the `rio app remove [--name=default] [--purge]` command. The --purge option removes even the persistent data like links.

### Handling cyclic meta data

Meta data on the current application (--name=default) can be listed with the command `rio cyclic meta [--name=default] list` and inspected in more detail with `rio cyclic meta [--name=default] inspect <name>`. One can even read the content of a meta data element `rio cyclic meta [--name=default] read <name> [--sync]`but be aware that this could destabelize the cyclic threads running on this application.

## Handling cyclic links

Cyclic links can be listed with the command `rio cyclic link [--name=default] list` and inspected with `rio cyclic link [--name=default] get <name>`. A new link can be set with `rio cyclic link [--name=default] set <linkname> <metaname> [--offset=<offset>]` where multiple links with the same meta data and offset are ignored. To remove a link simple type `rio cyclic link [--name=default] remove <linkname> [metaname] [--offset=<offset>]` but be aware that if you call remove without a meta name the whole link will be cleared.
