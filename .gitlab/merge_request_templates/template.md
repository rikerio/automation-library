# Merge Request Rules

[] Provide a meaningfull description.
[] Squash commits. I don't care if there are multiple commits. But when it should be merged, it should always be squashed.
[] Is documentation needed? Always document what you did. Either for doxygen or in the README. Provide Examples in the wiki.
