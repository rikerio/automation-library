#pragma once

#include "type_infos.h"
#include "bit_type.h"
#include "defs.h"
#include <string>
#include <map>

namespace automation {

/**
 * @brief Base Type hold Datatype information stored in the cyclic and acyclic shared memory.
 */
class BaseType {
public:
    /**
     * @brief Create BaseType from a string containg the name of the type. If not typ with the name
     * is recorgnized the type will be converted to a "buffer" type.
     *
     * @param name Datatype name
     * @param byte_size_type used for strings and buffer types.
     */
    static BaseType type_from_name(const std::string& name, std::size_t size = 0);

    /**
     * @brief Create BaseType from a string containg the name of the type. If not typ with the name
     * is recorgnized the type will be converted to a "buffer" type.
     *
     * @param name Datatype name
     * @param byte_size_type used for strings and buffer types.
     */
    static BaseType type_from_name(DataType datatype, std::size_t size = 0);

    /**
     * @brief Create datatype 'undefined' with zero size
     */
    BaseType() = default;

    /**
     * @brief Construct BaseType object with datatype name and bit size.
     *
     * @param name Datatype name
     * @param bit_size_type bit size of the datatype
     */
    BaseType(DataType datatype, SizingStrategy strategy, std::size_t size);

    /**
     * @brief compare two BaseTypes for equality.
     */
    bool operator==(const BaseType& rhs) const;

    /**
     * @brief compare two BaseTypes for inequality.
     */
    bool operator!=(const BaseType& rhs) const;

    const std::string& name() const;

    /**
     * @brief get datatype name
     */
    DataType datatype() const;

    /**
     * @brief get Sizing Strategy
     */
    SizingStrategy sizing_strategy() const;

    /**
     * @brief get datatype byte size
     */
    std::size_t size() const;

    /**
     * @brief check if datatype is of type 'undefined'
     */
    bool is_undefined() const;

protected:
    DataType _datatype;
    SizingStrategy _sizing_strategy;
    std::size_t _size;
};

template <typename T>
class Type : public BaseType {
public:
    Type()
        : BaseType(TypeInfos<T>::datatype, TypeInfos<T>::sizing_strategy, TypeInfos<T>::size)
    {
    }

    Type(std::size_t size)
        : BaseType(TypeInfos<T>::datatype, TypeInfos<T>::sizing_strategy, size)
    {
    }
};

} // automation
