#pragma once

namespace automation {

enum class Direction {
    r,
    w,
    rw
};

}
