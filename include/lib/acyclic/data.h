#pragma once

#include <string>
#include <vector>
#include <filesystem>
#include "types.h"
#include "utils/expected.h"

namespace automation::acyclic {

class Data {

public:
    template <typename T>
    static Data create(T value)
    {
        if constexpr (std::is_same<std::string, T>::value) {
            return create(Type<std::string>(value.length()), std::data(value));
        } else {
            return create(Type<T>(), &value);
        }
    }

    static Data create(const char* value)
    {
        std::string tmp(value);
        return create(tmp);
    }

    static Data create(BaseType type, const void* ptr);
    static utils::Expected<Data> deserialize(const std::filesystem::path& path);
    static utils::Expected<utils::Success> serialize(const std::filesystem::path& path, const Data& meta);

    Data() = default;

    const BaseType& type() const;
    const void* addr() const;
    const char* data() const;
    const std::vector<char>& buffer() const;

private:
    Data(BaseType type, std::vector<char> buffer = {});

    std::vector<char> _buffer;
    BaseType _type;
};

}
