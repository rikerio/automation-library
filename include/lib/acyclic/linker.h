#pragma once

#include "types.h"
#include "utils/expected.h"
#include "utils/event-service.h"
#include "utils/inotify-descriptor.h"
#include "utils/event-descriptor.h"
#include "utils/scheduler.h"
#include "acyclic/data.h"
#include "acyclic/storage.h"
#include <memory>
#include <thread>
#include <set>
#include <algorithm>
#include <iostream>
#include <optional>

namespace automation::acyclic {

/// @private
class AbstractJob {
public:
    virtual ~AbstractJob() {};
    virtual void update(Data& meta) = 0;
    virtual void execute_read() = 0;
    virtual std::optional<Data> execute_write() = 0;
};

/// @private
template <typename T, Direction D>
class Job : public AbstractJob {
public:
    using value_type = typename TypeInfos<T>::Type;

    Job(Var<T, D>& var)
        : _var(var)
        , _copy_handler(_var)
        , _updated(false)
    {
    }

    void update(Data& meta)
    {
        _meta_data = meta;
        _updated.store(true);
    }

    void execute_read()
    {
        if (D == Direction::w) {
            return;
        }

        if (!_updated.load()) {
            return;
        }

        if (_meta_data.type().size() != sizeof(value_type)) {
            return;
        }

        _copy_handler.copy_from(std::data(_meta_data.buffer()));

        _updated.store(false);
    }

    std::optional<Data> execute_write()
    {

        if (D == Direction::r) {
            return {};
        }

        return Data::create<T>(_var.value());
    }

private:
    Var<T, D> _var;
    TypeCopyHandler<T, D> _copy_handler;
    Data _meta_data;
    std::atomic<bool> _updated;
};

/**
 * @brief Helper Class for Linking acyclic data to variables
 *
 * A link name can be linked to a variable. Using the pre_loop and post_loop
 * methods the variables will be used to synchronize the persistent memory.
 */
class Linker {

public:
    using ErrorList = std::vector<utils::Error>;

    Linker(Storage& storage, const std::string& prefix = "")
        : _storage(storage)
        , _prefix(prefix)
        , _read_task(std::bind(&Linker::read_task_handler, this))
        , _write_task(std::bind(&Linker::write_task_handler, this))
    {
    }

    ~Linker()
    {
        _read_service.stop();
        if (_read_task.joinable()) {
            _read_task.join();
        }

        _write_service.stop();
        if (_write_task.joinable()) {
            _write_task.join();
        }
    }

    /**
     * @brief Link variable to a ayclic data
     * @param data_name
     * @param var
     */
    template <typename T, Direction D>
    void link(const std::string& data_name, Var<T, D>& var)
    {
        auto prefixed_name = _prefix + data_name;
        auto job = std::make_shared<Job<T, D>>(var);
        auto meta = _storage.load(prefixed_name);

        if (meta) {
            job->update(*meta);
            job->execute_read();
        }

        _job_map.insert(std::make_pair(prefixed_name, job));
        _job_list.push_back(job);
    }

    /**
     * @brief Work through a fixed set of jobs in the job_queue
     *
     */
    void pre_loop()
    {
        std::for_each(_job_list.begin(), _job_list.end(), [&](auto& job) {
            job->execute_read();
        });
    }

    /**
     * @brief finish off write queue
     */
    void quit()
    {
        _write_service.stop();
    }

private:
    using JobList = std::vector<std::shared_ptr<AbstractJob>>;
    using JobMap = std::map<std::string, std::shared_ptr<AbstractJob>>;

    Storage& _storage;
    std::string _prefix;

    JobList _job_list;
    JobMap _job_map;
    utils::EventService _read_service;
    utils::EventService _write_service;
    std::thread _read_task;
    std::thread _write_task;

    void write_task_handler()
    {
        utils::Scheduler::set_other();

        _write_service.start();

        std::for_each(_job_map.begin(), _job_map.end(), [&](auto& it) {
            auto meta = it.second->execute_write();

            if (!meta) {
                return;
            }
            _storage.store(it.first, *meta);
        });
    }

    void read_task_handler()
    {
        utils::Scheduler::set_other();
        utils::InotifyDescriptor inotify(_storage.data_folder(), IN_CLOSE_WRITE);

        _read_service.register_handler(inotify.fd(), [&]() {
            auto inotify_event_list = inotify.read();

            std::for_each(inotify_event_list.begin(), inotify_event_list.end(), [&](auto& event) {
                auto it = _job_map.find(event.name());

                if (it == _job_map.end()) {
                    return;
                }

                auto& job = it->second;
                auto meta = _storage.load(event.name());

                if (!meta) {
                    return;
                }

                job->update(*meta);
            });
        });

        _read_service.start();
    }
};
}
