#pragma once

#include "var.h"
#include "types.h"

namespace automation::acyclic {
using ReadOnly = automation::Common<Direction::r>;
using ReadWrite = automation::Common<Direction::rw>;
}

namespace automation::ad {
using ro = acyclic::ReadOnly;
using rw = acyclic::ReadWrite;
}
