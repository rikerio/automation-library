#pragma once

#include <string>
#include <filesystem>
#include "utils/expected.h"
#include "acyclic/data.h"

namespace automation {
class Application;
};

namespace automation::acyclic {

class Storage {

    static constexpr std::string_view data_folder_name = "data";
    static constexpr std::string_view lock_folder_name = "locks";

public:
    static utils::Expected<Storage> create(const std::filesystem::path& root_folder);
    static utils::Expected<Storage> attach(const std::filesystem::path& root_folder);
    static utils::Expected<Storage> attach(const Application& application);

    static utils::Expected<utils::Success> remove(const std::filesystem::path& root_folder);

    Storage() = default;
    Storage(const std::filesystem::path& root_folder);

    const std::filesystem::path& root_folder() const;
    const std::filesystem::path& data_folder() const;
    const std::filesystem::path& lock_folder() const;

    utils::Expected<Data> load(const std::string& name) const;
    utils::Expected<utils::Success> store(const std::string& name, const Data& meta) const;
    utils::Expected<utils::Success> reset(const std::string& name) const;

private:
    std::filesystem::path _root_folder;
    std::filesystem::path _data_folder;
    std::filesystem::path _lock_folder;
};

}
