#pragma once

#include "defs.h"

namespace automation {

struct MemoryAddress {
    byte_size_type offset;
    bit_offset_type index;

    MemoryAddress();
    MemoryAddress(byte_size_type offset, uint16_t index = 0);

    bool equals(const MemoryAddress& other) const;
};

}

bool operator==(const automation::MemoryAddress& a, const automation::MemoryAddress& b);
automation::MemoryAddress operator+(const automation::MemoryAddress& a, const automation::MemoryAddress& b);
