#pragma once

#include <boost/interprocess/managed_shared_memory.hpp>

namespace automation {

using memory_handle_type = boost::interprocess::managed_shared_memory::handle_t;
using bit_offset_type = uint8_t;
using bit_size_type = uint32_t;
using byte_size_type = uint64_t;

};
