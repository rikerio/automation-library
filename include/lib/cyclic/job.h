#pragma once

#include "direction.h"
#include "var.h"

namespace automation::cyclic {

/// @private
class AbstractJob {
public:
    virtual ~AbstractJob() {};
    virtual void execute_read() = 0;
    virtual void execute_write() = 0;
};

/// @private
template <typename T, Direction D>
class Job : public AbstractJob {
public:
    Job(Var<T, D>& var, void* opposite_ptr, MemoryAddress adr)
        : _var(var)
        , _copy_handler(std::make_unique<TypeCopyHandler<T, D>>(var, opposite_ptr, adr))
    {
    }

    void execute_read()
    {
        if (D == Direction::w) {
            return;
        }

        _copy_handler->copy_from();
    }

    void execute_write()
    {
        if (D == Direction::r) {
            return;
        }

        _copy_handler->copy_to();
    }

private:
    Var<T, D> _var;
    std::unique_ptr<AbstractCopyHandler> _copy_handler;
};
}
