#pragma once

#include "types.h"
#include "utils/expected.h"
#include <filesystem>
#include <stdint.h>

namespace automation::cyclic {

/**
 * @brief Meta Data Object
 *
 * The Meta Data Object contains the bit offset and the type of the data located
 * in the cyclic shared memory.
 */
class Meta {

public:
    /**
     * @brief Create a Meta object base on the Type T
     */
    template <typename T>
    static Meta create(MemoryAddress adr)
    {
        return Meta(Type<T>(), adr);
    }

    /**
     * @brief Serialize Meta Object to a file
     */
    static utils::Expected<utils::Success> serialize(const std::filesystem::path& path, const Meta& meta);

    /**
     * @brief Deserialize Meta Data Object from a file
     */
    static utils::Expected<Meta> deserialize(const std::filesystem::path& path);

    Meta() = default;
    Meta(BaseType type, MemoryAddress adr = {});

    const MemoryAddress& adr() const;
    const BaseType& type() const;
    memory_handle_type handle() const;

private:
    BaseType _type;
    MemoryAddress _adr;

    memory_handle_type _handle;
};

}
