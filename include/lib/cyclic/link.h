#pragma once

#include <string>
#include <vector>
#include <filesystem>
#include <optional>
#include <stdint.h>
#include "types.h"
#include "memory_address.h"
#include "utils/expected.h"

namespace automation::cyclic {

struct LinkConverter {
    std::string name;
    std::vector<double> args;

    LinkConverter() = default;

    LinkConverter(const std::string& name, std::vector<double> args)
        : name(name)
        , args(args)
    {
    }

    template <typename... Args>
    LinkConverter(const std::string& name, Args&&... args)
        : name(name)
        , args({ std::forward<Args>(args)... })
    {
    }
};

struct LinkOptions {
    MemoryAddress offset;
    std::optional<LinkConverter> converter;
    LinkOptions()
        : offset(0)
    {
    }

    LinkOptions(MemoryAddress offset)
        : offset(offset)
    {
    }

    LinkOptions(MemoryAddress offset, LinkConverter converter)
        : offset(offset)
        , converter(converter)
    {
    }

    LinkOptions(MemoryAddress offset, const std::string& converter_name, std::vector<double> converter_args)
        : LinkOptions(offset, LinkConverter(converter_name, converter_args))
    {
    }

    template <typename... Args>
    LinkOptions(MemoryAddress offset, const std::string& name, Args&&... args)
        : offset(offset)
        , converter(LinkConverter { name, std::forward<Args>(args)... })
    {
    }
};

/**
 * @brief LinkEntry containing the meta data object name and an additional bit offset
 */
struct Link {

    std::string name;
    LinkOptions options;
    Link() = default;
    Link(const std::string& name, MemoryAddress offset = {})
        : name(name)
        , options { offset }
    {
    }

    Link(const std::string& name, LinkOptions options)
        : name(name)
        , options(std::move(options))
    {
    }

    bool operator==(const Link& other) const
    {
        return other.name == name && other.options.offset == options.offset;
    }
};

/**
 * @brief Link containing Link Entries
 */
class LinkList : public std::vector<Link> {
public:
    static utils::Expected<LinkList> deserialize(const std::filesystem::path& path);
    static utils::Expected<utils::Success> serialize(const std::filesystem::path& path, const LinkList& link);
};

}
