#pragma once

#include "var.h"

namespace automation::cyclic {

using ReadOnly = automation::Common<Direction::r>;
using WriteOnly = automation::Common<Direction::w>;
using ReadWrite = automation::Common<Direction::rw>;

}

namespace automation::cd {
using ro = cyclic::ReadOnly;
using wo = cyclic::WriteOnly;
using rw = cyclic::ReadWrite;

}
