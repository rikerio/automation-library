#pragma once

#include <filesystem>
#include <string>
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/sync/named_upgradable_mutex.hpp>
#include "types.h"
#include "utils/expected.h"
#include "utils/error.h"

namespace bip = boost::interprocess;

namespace automation {
class Application;
}

namespace automation::cyclic {

class LinkList;
class LinkOptions;
class Meta;
class Linker;

/**
 * @brief Bundles functions to manage Cyclic Data and Links
 *
 * The Cyclic Data Storage manages a folder containing
 * - A name to a shared memory object (located in /dev/shm)
 * - A list of meta data describing the content of the shared memory
 * - A list of links containing connections to meta data locations.
 */
class Storage {
public:
    /**
     * @brief Create a new Cyclic Data Storage
     *
     * @param Folder where to create needed files. Folder musst exist already.
     * @param size Size of shared memory in bytes.
     */
    static utils::Expected<Storage> create(const std::filesystem::path& path, size_t size);

    /**
     * @brief Attach to an existing Cyclic Data Storage
     *
     * @param path Path where the storage is located.
     */
    static utils::Expected<Storage> attach(const std::filesystem::path& path);

    /**
     * @brief Attach to an existing Cyclic Data Storage
     *
     * @param app Get the storage location directly from the Application
     */
    static utils::Expected<Storage> attach(const Application& app);

    /**
     * @brief Remove temporary Cyclic Data Storage
     *
     * @param path Cyclic Data Storage location.
     * @param purge Removed persistent data as well
     */
    static utils::Expected<utils::Success> remove(const std::filesystem::path& path, bool purge);

    /**
     * @brief Get Storage root path
     */
    const std::filesystem::path& root_path() const;

    /**
     * @brief Get data path
     *
     */
    const std::filesystem::path& data_path() const;

    /**
     * @brief Get link path
     */
    const std::filesystem::path& link_path() const;

    /**
     * @brief get lock file path
     */
    const std::filesystem::path& lock_path() const;

    /**
     * @brief get shared memory filename
     */
    const std::string& shm_name() const;

    /**
     * @brief get pointer to the shared memory
     */
    char* shm() const;

    /**
     * @brief return shared memory read/write mutex
     */
    bip::named_upgradable_mutex& mutex();

    /**
     * @brief convert memory handles into pointer
     */
    void* get_address_from_handle(memory_handle_type handle);

    /**
     * @brief convert pointer to shared memory handle
     */
    memory_handle_type get_handle_from_address(void* ptr);

    /**
     * @brief Get link object
     *
     * Read the file located in the link_path / link_name. The file contains
     * meta names and additional offset information.
     * @param link_name
     */
    utils::Expected<LinkList> link(const std::string& link_name) const;

    /**
     * @brief create a link to meta data including bit offset
     *
     * A link contains a meta data name representing the data location in the
     * shared memory. A bit offset can relocate the data location.
     *
     * @param link_name
     * @param meta_name
     * @param offset bit offset
     */
    utils::Expected<LinkList> link(const std::string& link_name, const std::string& meta_name, MemoryAddress offset = {}) const;

    /**
     * @brief create a link to meta data including link options like offset and filter
     *
     * This is a advanced method for setting up links. By passing a cyclic::Link object you
     * are able to set the bit offset as well as defining a filter to convert analog data
     * representation to floats and double typed variables.
     *
     * @param link_name
     * @param link_options
     */

    utils::Expected<LinkList> link(const std::string& link_name, const std::string& meta_name, LinkOptions link_options) const;

    /**
     * @brief Remove link identified by link_name, meta_name and bit offset.
     *
     * @param link_name
     * @param meta_name
     * @param offset bit_offset
     */
    utils::Expected<LinkList> unlink(const std::string& link_name, const std::string& meta_name, MemoryAddress offset = {}) const;

    /**
     * @brief unlink complete link
     * @param link_name
     */
    utils::Expected<LinkList> unlink(const std::string& link_name) const;

    /**
     * @brief store a meta data object
     * @param name meta data name
     * @param meta Meta object
     */
    utils::Expected<utils::Success> store(const std::string& name, const Meta& meta) const;

    /**
     * @brief remove a meta data object
     * @param name meta data name
     */
    utils::Expected<utils::Success> remove(const std::string& name) const;

    /**
     * @brief load a meta data object
     * @param name meta data name
     */
    utils::Expected<Meta> load(const std::string& name) const;

    /**
     * @brief allocate memory on the shared memory and setup a meta object
     * @tparam T object type
     * @param name meta data name
     */
    template <typename T>
    utils::Expected<Meta> allocate(const std::string& name)
    {
        return allocate(name, Type<T>());
    }

    /**
     * @brief allocate memory on the shared memory based on the type
     * @param name meta data name
     * @param type
     */
    utils::Expected<Meta> allocate(const std::string& name, BaseType type);

    /**
     * @brief allocate memory on the shared memory
     * @param name meta data name
     * @param size meta data byte size
     */
    utils::Expected<Meta> allocate(const std::string& name, byte_size_type size);

    /**
     * @brief allocate a memory segment on the shared memory
     *
     * No meta data object will be created.
     * @param size byte size
     */
    utils::Expected<void*> allocate(byte_size_type size);

    /**
     * @brief deallocate memory segment from shared memory identified by the offset
     * @param offset byte offset
     */
    utils::Expected<utils::Success> deallocate(void* ptr);

    /**
     * @brief deallocate memory segment from shared memory identified by the handle
     * @param offset byte offset
     */
    utils::Expected<utils::Success> deallocate(memory_handle_type handle);

    Storage() = default;

private:
    Storage(const std::filesystem::path& path, const std::string& shm_name);

    std::filesystem::path _root_path;
    std::filesystem::path _data_path;
    std::filesystem::path _link_path;
    std::filesystem::path _lock_path;
    std::string _shm_name;

    bip::managed_shared_memory _shm;
    std::unique_ptr<bip::named_upgradable_mutex> _mutex;
};

} // namespace automation::cyclic
