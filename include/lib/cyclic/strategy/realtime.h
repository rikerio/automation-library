#pragma once

#include "utils/event-poll.h"
#include "utils/timer-descriptor.h"
#include "utils/event-descriptor.h"
#include "cyclic/thread.h"
#include <atomic>
#include <thread>

namespace automation::cyclic::strategy {

/**
 * @brief Implements Realtime Cyclic Thread Strategy using timer and events
 *
 * The Realtime Cyclic Thread Strategy implements an auxilary thread
 * that emits event and waits itself for a timer event. The event will
 * be received by the current thread and used in the yield function.
 */
class Realtime : public Thread::Strategy {
public:
    enum class Type {
        none,
        fifo,
        rr
    };

    Realtime(Type type, int8_t priority, utils::TimerDescriptor::Duration cycle_time);
    ~Realtime();

    void setup();
    void yield();
    void quit();

private:
    utils::EventPoll _aux_eventbus;
    utils::EventPoll _worker_eventbus;
    utils::TimerDescriptor _timer_event;
    utils::EventDescriptor _notify_event;
    utils::EventDescriptor _aux_exit_event;

    Type _type;
    uint8_t _priority;
    std::thread _thread;

private:
    void aux_thread_handler();
    void setup_scheduler();
};

}
