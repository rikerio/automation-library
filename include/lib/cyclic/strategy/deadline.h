#pragma once

#include "cyclic/thread.h"

namespace automation::cyclic::strategy {

/**
 * @brief Deadline Cyclic Thread Strategy using the deadline scheduler
 *
 * Using the sched_yield function from the deadline scheduler.
 */
class Deadline : public Thread::Strategy {

public:
    Deadline(uint32_t runtime, uint32_t deadline, uint32_t period);
    ~Deadline();

    void setup();
    void yield();
    void quit();

private:
    uint32_t _runtime;
    uint32_t _deadline;
    uint32_t _period;
};

}
