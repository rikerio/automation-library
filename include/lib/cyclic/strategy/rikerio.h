#pragma once

#include <filesystem>
#include "cyclic/thread.h"
#include "scheduler/client.h"
#include "utils/scheduler.h"

namespace automation::cyclic::strategy {

class RikerIO : public Thread::Strategy {
public:
    RikerIO(const std::filesystem::path& socket_path, uint16_t task_id, int8_t priority);

    void setup();
    void yield();
    void quit();

private:
    class HelperThread {
    public:
        HelperThread(RikerIO& rio)
            : _rio(rio)
            , _client {
                _service,
                utils::UnixDomainSocket::connect(_rio._socket_path),
                rio._task_id
            }
            , _thread(std::bind(&HelperThread::_handler, this))
        {
        }

        ~HelperThread()
        {
            _service.stop();
            if (_thread.joinable()) {
                _thread.join();
            }
        }

        void send_done()
        {
            _client.send_done();
        }

    private:
        RikerIO& _rio;
        utils::EventService _service;
        automation::scheduler::Client _client;
        std::thread _thread;

        void _handler()
        {
            if (_rio._priority == 0) {
                utils::Scheduler::set_other();
            } else {
                utils::Scheduler::set_rr(_rio._priority);
            }

            _client.on(scheduler::Client::Event::Loop, [&]() {
                _rio._loop_event.write();
            });

            _service.start();
        }
    };

    friend class HelperThread;

    std::filesystem::path _socket_path;
    uint16_t _task_id;
    int8_t _priority;
    bool _first_yield_call = true;

    utils::EventService _service;
    utils::EventPoll _epoll;
    utils::EventDescriptor _loop_event;
    std::unique_ptr<HelperThread> _helper_thread;
};
}
