#pragma once

#include "utils/event-poll.h"
#include "utils/timer-descriptor.h"
#include "cyclic/thread.h"

namespace automation::cyclic::strategy {

/**
 * @brief Default Cyclic Thread Strategy using simple timer
 *
 * The Default strategy is using a simple timer for the yield
 */
class Default : public Thread::Strategy {
public:
    Default(utils::TimerDescriptor::Duration duration);
    ~Default();

    /**
     * @brief Setup scheduler a SCHED_OTHER
     */
    void setup();

    /**
     * @brief wait for timerfd event
     */
    void yield();

    /**
     * @brief emit exit event
     */
    void quit();

private:
    utils::EventPoll _event_poll;
    utils::TimerDescriptor _timer_event;
};

}
