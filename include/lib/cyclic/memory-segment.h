#pragma once

#include <boost/interprocess/managed_shared_memory.hpp>
#include <vector>
#include <filesystem>
#include <stdint.h>
#include "types.h"
#include "utils/expected.h"

using namespace boost::interprocess;
namespace automation::cyclic {

struct MemorySegment {
    managed_shared_memory::handle_t offset;
    char* ptr;
};

}
