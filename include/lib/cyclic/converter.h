#pragma once

#include "cyclic/job.h"

namespace automation::cyclic {

class AbstractConverter {
public:
    virtual ~AbstractConverter() {};

    virtual double convert(void* source) const = 0;
    virtual void deconvert(double source, void* target) const = 0;
};

/// @private
template <typename T, Direction D>
class ConverterJob : public AbstractJob {
public:
    ConverterJob(void* ptr, Var<T, D> var, std::unique_ptr<AbstractConverter> converter)
        : _var(var)
        , _ptr(ptr)
        , _converter(std::move(converter))
        , _read_copy_handler(std::make_unique<TypeCopyHandler<T, D>>(var, &_local, 0))
    {
    }

    void execute_read()
    {
        if (D != Direction::r) {
            return;
        }

        _local = _converter->convert(_ptr);
        _read_copy_handler->copy_from();
    }

    void execute_write()
    {
        if (D != Direction::w) {
            return;
        }
        _converter->deconvert(_var.value(), _ptr);
    }

private:
    Var<T, D> _var;
    void* _ptr = nullptr;
    T _local = 0.0f;
    std::unique_ptr<AbstractConverter> _converter;
    std::unique_ptr<AbstractCopyHandler> _read_copy_handler;
};

}
