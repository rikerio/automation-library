#pragma once

#include "cyclic/storage.h"
#include "cyclic/job.h"

#include <memory>
#include <vector>
#include <stdint.h>

namespace automation::cyclic {

/**
 * @brief Handling shared memory meta data creation on synchronizes with variables
 *
 * A cyclic data provider creates, in contrast to the cyclic data linker, a meta data object
 * on the shared memory and synchronizes this with a variable.
 */
class Provider {
public:
    /**
     * @brief Construct a Provider object
     */
    Provider(Storage& storage, const std::string& prefix = "");

    /**
     * @brief create a new meta data object and link it with a variable
     */
    template <typename T, Direction D>
    void create(Var<T, D>& var, const std::string& name)
    {
        auto prefixed_name = _prefix + name;
        auto meta = _storage.allocate<T>(prefixed_name);

        if (!meta) {
            return;
        }

        void* ptr = _storage.get_address_from_handle(meta->handle());
        auto job = std::make_unique<Job<T, D>>(var, ptr, 0);

        _job_list.push_back(std::move(job));
    }

    void pre_loop();
    void post_loop();

private:
    Storage& _storage;
    std::string _prefix;
    std::vector<std::unique_ptr<AbstractJob>> _job_list;
};
}
