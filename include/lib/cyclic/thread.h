#pragma once
#include <thread>
#include <memory>
#include "utils/timer-descriptor.h"
#include "utils/event-descriptor.h"
#include "utils/event-service.h"
#include "utils/event-emitter.h"

namespace automation::cyclic {

template <class... Ts>
using void_t = void;

namespace detail {

    /// @private
    template <template <class...> class Trait, class Enabler, class... Args>
    struct is_detected : std::false_type {
    };

    /// @private
    template <template <class...> class Trait, class... Args>
    struct is_detected<Trait, void_t<Trait<Args...>>, Args...> : std::true_type {
    };
}

template <template <class...> class Trait, class... Args>
using is_detected = typename detail::is_detected<Trait, void, Args...>::type;

template <class T>
using init_t = decltype(std::declval<T>().init());

template <class T>
using pre_loop_t = decltype(std::declval<T>().pre_loop());

template <class T>
using loop_t = decltype(std::declval<T>().loop());

template <class T>
using post_loop_t = decltype(std::declval<T>().post_loop());

template <class T>
using quit_t = decltype(std::declval<T>().quit());

/**
 * @brief Represents a Cyclic Thread
 *
 * A cyclic thread implements logic to init, loop and quit
 * a task. Tasks can be added before start and will be executed
 * in a cyclic manner.
 *
 * A cyclic thread nees a strategy on how the cyclic is being executed.
 * Possible default implementations are:
 * - DefaultStrategy
 * - RealtimeStrategy
 * - DeadlineStrategy
 * - RikerIOStrategy (needs implementation)
 */
class Thread {
public:
    class Strategy {
    public:
        virtual ~Strategy() {};
        virtual void setup() = 0;
        virtual void yield() = 0;
        virtual void quit() = 0;
    };

    /**
     * @brief create a new thread and forward arguments to strategy
     * @tparam S Strategy
     * @param args Arguments to be passed onto the Strategy
     */
    template <class S, typename... Args>
    static Thread create(Args&&... args)
    {
        return Thread(std::make_unique<S>(std::forward<Args>(args)...));
    }

    Thread(std::unique_ptr<Strategy> strategy);
    ~Thread();

    /**
     * @brief Add object containing methods be used by the thread
     *
     * Methods will be used if defined in the object, not interface needed
     * - init
     * - pre_loop
     * - loop
     * - post_loop
     * - quit
     */
    template <class T>
    void use(T& t)
    {
        if constexpr (is_detected<init_t, T>::value) {
            _task.on(Task::Event::Init, std::bind(&T::init, &t));
        }

        if constexpr (is_detected<pre_loop_t, T>::value) {
            _task.on(Task::Event::PreLoop, std::bind(&T::pre_loop, &t));
        }

        if constexpr (is_detected<loop_t, T>::value) {
            _task.on(Task::Event::Loop, std::bind(&T::loop, &t));
        }

        if constexpr (is_detected<post_loop_t, T>::value) {
            _task.on(Task::Event::PostLoop, std::bind(&T::post_loop, &t));
        }

        if constexpr (is_detected<quit_t, T>::value) {
            _task.on(Task::Event::Quit, std::bind(&T::quit, &t));
        }
    }

    /**
     * @brief start thread
     */
    void start();

    /**
     * @brief stop thread and join
     */
    void stop();

private:
    enum class Event {
        Loop,
        Quit
    };

    class Task : public utils::EventEmitter<> {
    public:
        enum class Event {
            Init,
            PreLoop,
            Loop,
            PostLoop,
            Quit
        };

        Task();
        ~Task();

        void loop();
        void stop();

    private:
        enum class State {
            Start,
            Init,
            Loop,
            Stopped
        };

        State _state = State::Start;

        utils::EventDescriptor _init_event;
        utils::EventDescriptor _done_event;
        utils::EventService _service;
        std::thread _init_thread;

        bool _start_request = false;
        bool _stop_request = false;

        void init();
    };

    utils::EventEmitter<> _emitter;
    std::unique_ptr<Strategy> _strategy;
    Task _task;
    std::atomic<bool> _running;
    std::unique_ptr<std::thread> _thread_ptr;

    void thread_handler();
};
}
