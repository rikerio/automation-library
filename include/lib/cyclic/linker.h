#pragma once

#include "types.h"
#include "cyclic/storage.h"
#include "cyclic/job.h"
#include "cyclic/converter.h"
#include "cyclic/meta.h"
#include "utils/expected.h"
#include <memory>
#include <vector>
#include <algorithm>
#include <fmt/core.h>

namespace automation::cyclic {

/// @private
class JobFactory {

public:
    JobFactory(Storage& storage)
        : _storage(storage)
    {
    }

    template <typename T, Direction D>
    utils::Expected<std::unique_ptr<Job<T, D>>> create_job(Var<T, D>& var, Meta& meta, LinkOptions link_options)
    {
        auto type_bit_size = meta.type().sizing_strategy() == SizingStrategy::Bits ? meta.type().size() : (meta.type().size() * 8);

        if (D == Direction::r) {
            if (var.bit_size() < type_bit_size) {
                return utils::Error("Variable bit size is smaller than the meta data bit size of read job.");
            }
        } else if (D == Direction::w) {
            if (var.bit_size() > type_bit_size) {
                return utils::Error("Variable bit size is bigger than the meta data bit size of write job.");
            }
        } else {
            if (var.bit_size() != type_bit_size) {
                return utils::Error("Variable bit size is bigger than the meta data bit size of read/write job.");
            }
        }

        // TODO: make extensive boundaries check
        void* ptr = _storage.get_address_from_handle(meta.handle());

        return std::make_unique<Job<T, D>>(var, ptr, link_options.offset);
    }

private:
    Storage& _storage;
};

/// @private
class ConverterFactory {

public:
    using SetupHandler = std::function<std::unique_ptr<AbstractConverter>(const std::vector<double>&)>;

    static std::map<std::string, SetupHandler> _setup_map;

    static void setup(const std::string& name, SetupHandler handler)
    {
        _setup_map.insert(std::make_pair(name, handler));
    }

    ConverterFactory(Storage& storage)
        : _storage(storage)
    {
    }

    template <typename T, Direction D>
    utils::Expected<std::unique_ptr<ConverterJob<T, D>>> create_job(Var<T, D>& var, Meta& meta, LinkOptions link_options)
    {

        auto it = _setup_map.find(link_options.converter->name);

        if (it == _setup_map.end()) {
            return utils::Error(fmt::format("Unable to find converter {}", link_options.converter->name));
        }

        auto converter = it->second(link_options.converter->args);

        if (!converter) {
            return utils::Error(fmt::format("Error creating converter object for converter {}.", link_options.converter->name));
        }

        void* ptr = _storage.get_address_from_handle(meta.handle());

        return std::make_unique<ConverterJob<T, D>>(ptr, var, std::move(converter));
    }

private:
    Storage& _storage;
};

/**
 * @brief Helper Class for Linking links to variables
 *
 * A link name can be linked to a variable. Using the pre_loop and post_loop
 * methods the variables will be used to synchronize the shared memory.
 */
class Linker {

public:
    using ErrorList = std::vector<utils::Error>;

    Linker(Storage& storage, const std::string& prefix = "");

    /**
     * @brief Link variable to a cyclic data link
     * @param link_name
     * @param var
     */
    template <typename T, Direction D>
    utils::Expected<utils::Success> link(const std::string& link_name, Var<T, D>& var)
    {
        auto prefixed_name = _prefix + link_name;
        auto link = _storage.link(prefixed_name);

        if (!link) {
            return utils::Success {};
        }

        if (var.readable() && link->size() > 1) {
            utils::Error error(fmt::format("No handling link '{}', read with multiple links not allowed.", prefixed_name));
            _error_list.push_back(error);
            return error;
        }

        for (auto it = link->begin(); it != link->end(); it++) {
            auto& entry = *it;
            auto meta = _storage.load(entry.name);

            if (!meta) {
                utils::Error error(fmt::format("Error fetching meta '{}' for link '{}' : {}", prefixed_name, entry.name, meta.error().message()));
                _error_list.push_back(error);
                return error;
            }

            if (entry.options.converter) {

                // Handle a converter job since the converter is defined in the link descriptions

                if (!std::is_same<T, float>::value && !std::is_same<T, double>::value) {
                    _error_list.push_back(utils::Error(fmt::format("Error handling link {}: for convertered links this musst be a float or double datatype.", prefixed_name)));
                    continue;
                }

                auto converter_job = _converter_factory.create_job(var, *meta, entry.options);

                if (!converter_job) {
                    _error_list.push_back(utils::Error(fmt::format("Error handling link {}", prefixed_name)));
                    _error_list.push_back(std::move(converter_job.error()));
                    continue;
                }

                _job_list.push_back(std::move(*converter_job));

            } else {

                // Handle a simple copy job

                auto copy_job = _job_factory.create_job<T, D>(var, *meta, entry.options);

                if (!copy_job) {
                    _error_list.push_back(utils::Error(fmt::format("Error handling Link {}", prefixed_name)));
                    _error_list.push_back(std::move(copy_job.error()));
                    continue;
                }

                _job_list.push_back(std::move(*copy_job));
            }
        };

        return utils::Success {};
    }

    /**
     * @brief Lock Shared Memory and transfer data to the variables
     *
     * The Lock on the shared memory will be a read lock (shared)
     */
    void pre_loop();

    /**
     * @brief Lock shared memory and transfer data from the variables to the shared memory
     *
     * The lock on the shared memory will be a write lock (exclusive)
     */
    void post_loop();

    /**
     * @brief execute one last post_loop call
     */
    void quit();

    /**
     * @brief get collected errors
     */
    const ErrorList& error_list() const
    {
        return _error_list;
    }

private:
    using JobList = std::vector<std::unique_ptr<AbstractJob>>;

    std::string _prefix;

    Storage& _storage;
    ConverterFactory _converter_factory;
    JobFactory _job_factory;
    JobList _job_list;
    ErrorList _error_list;
};

}
