#pragma once
#include "direction.h"
#include "type_infos.h"
#include "memory_address.h"

namespace {

template <automation::Direction D, typename T>
struct Writeable {
};

template <typename T>
struct Writeable<automation::Direction::w, T> {
    using type = typename automation::TypeInfos<T>::Type;
};

template <typename T>
struct Writeable<automation::Direction::rw, T> {
    using type = typename automation::TypeInfos<T>::Type;
};

} // namespace

namespace automation {

struct BitType;
struct MemoryAddress;

// forward declaration
template <typename T, Direction D>
class TypeCopyHandler;

template <typename T, Direction D>
class Var {
    using base_type = typename TypeInfos<T>::Type;

    struct LocalMemory {
        bool linked;
        uint32_t ref_counter;
        uint32_t write_counter;
        base_type last_content;
        base_type content;
    };

public:
    Var()
        : _memory((LocalMemory*)::calloc(1, sizeof(LocalMemory)))
    {
        _memory->ref_counter = 1;
    }

    Var(base_type initial_value)
        : _memory((LocalMemory*)::calloc(1, sizeof(LocalMemory)))
    {
        _memory->content = initial_value;
        _memory->ref_counter = 1;
    }

    Var(const Var& other)
    {
        _memory = other._memory;
        _memory->ref_counter += 1;
    }

    Var& operator=(const Var& other)
    {
        _memory = other._memory;
        _memory->ref_counter += 1;

        return *this;
    }

    Var(Var&& other) = delete;
    Var& operator=(Var&& other) = delete;

    ~Var()
    {
        if (_memory->ref_counter > 1) {
            _memory->ref_counter--;
            return;
        }

        ::free(_memory);
    }

    base_type value() const
    {
        return _memory->content;
    }

    base_type last_value() const
    {
        return _memory->last_content;
    }

    bool changed() const
    {
        return _memory->content != _memory->last_content;
    }

    template <Direction _D = D>
    typename Writeable<_D, base_type>::type value(base_type v)
    {
        _memory->content = v;
        _memory->write_counter += 1;
        return _memory->content;
    }

    bool linked() const
    {
        return _memory->linked;
    }

    bool readable() const
    {
        return D == Direction::r || D == Direction::rw;
    }

    bool writeable() const
    {
        return D == Direction::w || D == Direction::rw;
    }

    uint32_t bit_size() const
    {
        if (TypeInfos<T>::sizing_strategy == SizingStrategy::Bits) {
            return TypeInfos<T>::size;
        } else {
            return TypeInfos<T>::size * 8;
        }
    }

private:
    friend class TypeCopyHandler<T, D>;
    LocalMemory* _memory;

    void read_from(base_type& new_value)
    {
        _memory->last_content = _memory->content;
        _memory->content = new_value;
    }

    void write_to(base_type* target)
    {
        if (D == Direction::r) {
            return;
        } else if (D == Direction::w) {
            *target = _memory->content;
        } else if (_memory->write_counter > 0) {
            *target = _memory->content;
            _memory->write_counter = 0;
        }
    }
};

template <Direction D>
class Common {
public:
    using Bit = Var<BitType, D>;
    using Bool = Var<bool, D>;
    using UInt8 = Var<uint8_t, D>;
    using Int8 = Var<int8_t, D>;
    using UInt16 = Var<uint16_t, D>;
    using Int16 = Var<int16_t, D>;
    using UInt32 = Var<uint32_t, D>;
    using Int32 = Var<int32_t, D>;
    using UInt64 = Var<uint64_t, D>;
    using Int64 = Var<int64_t, D>;
    using Float = Var<float, D>;
    using Double = Var<double, D>;
};

class AbstractCopyHandler {
public:
    virtual ~AbstractCopyHandler() {};
    virtual void copy_from() = 0;
    virtual void copy_from(const void*) = 0;
    virtual void copy_to() = 0;
};

template <typename T, Direction D>
class TypeCopyHandler : public AbstractCopyHandler {
public:
    TypeCopyHandler(Var<T, D> var, void* shm_ptr, MemoryAddress adr)
        : _var(var)
        , _adr(adr)
    {
        _var._memory->linked = true;
        _shm_ptr = (T*)((uint8_t*)shm_ptr + adr.offset);
    }

    TypeCopyHandler(Var<T, D> var)
        : _var(var)
    {
        _var._memory->linked = true;
    }

    void copy_from()
    {
        _var.read_from(*_shm_ptr);
    }

    void copy_from(const void* ptr)
    {
        T* _type_ptr = (T*)ptr;
        _var.read_from(*_type_ptr);
    }

    void copy_to()
    {
        _var.write_to(_shm_ptr);
    }

private:
    Var<T, D> _var;
    MemoryAddress _adr;
    T* _shm_ptr = nullptr;
};

} // automation
