#pragma once

#include "cyclic-thread.h"
#include "cyclic-data.h"
#include "cd/linker.h"
#include "acyclic-data.h"

#include <stdint.h>

namespace automation {

class Linker {
public:
    Linker(const std::string& application_name, const std::string& prefix = "")
        : _app(Application::attach(application_name))
        , _cd_storage(cd::Storage::attach(_app.cyclic_data_path()))
        , _cd_linker(_cd_storage, prefix)
    {
    }

    template <typename T, Direction D>
    void link(Var<T, D>& var, const std::string& name)
    {
        _cd_linker.link(name, var);
    }

    template <typename T, Direction D>
    void link(ad::Var<T, D>& variable, const std::string& name)
    {
    }

    void pre_loop()
    {
        _cd_linker.pre_loop();
    }

    void post_loop()
    {
        _cd_linker.post_loop();
    }

private:
    Application _app;
    cd::Storage _cd_storage;
    cd::Linker _cd_linker;
};
}
