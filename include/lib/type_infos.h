#pragma once

#include <string_view>
#include <typeinfo>
#include <type_traits>
#include <stdexcept>
#include <cstring>
#include <cmath>

namespace automation {

enum class DataType {
    Undefined,
    Bit,
    Bool,
    Uint8,
    Int8,
    Uint16,
    Int16,
    Uint32,
    Int32,
    Uint64,
    Int64,
    Float,
    Double,
    String,
    Buffer
};

enum class SizingStrategy {
    Bits,
    Bytes
};

/**
 * @brief Structure that holds Datatype Information for any
 * unspecialized datatype.
 *
 * Datatypename is always represented as "buffer"
 */
template <typename T>
struct TypeInfos {
    static_assert(!std::is_same<T, void>::value);
    using Type = T;
    static constexpr DataType datatype = DataType::Buffer;
    static constexpr SizingStrategy sizing_strategy = SizingStrategy::Bytes;
    static constexpr size_t size = sizeof(T);
};

template <>
struct TypeInfos<void> {
    using Type = void;
    static constexpr DataType datatype = DataType::Undefined;
    static constexpr SizingStrategy sizing_strategy = SizingStrategy::Bytes;
    static constexpr size_t size = 0;
};

template <>
struct TypeInfos<bool> {
    using Type = bool;
    static constexpr DataType datatype = DataType::Bool;
    static constexpr SizingStrategy sizing_strategy = SizingStrategy::Bytes;
    static constexpr size_t size = sizeof(bool);
};

template <>
struct TypeInfos<int8_t> {
    using Type = int8_t;
    static constexpr DataType datatype = DataType::Int8;
    static constexpr SizingStrategy sizing_strategy = SizingStrategy::Bytes;
    static constexpr size_t size = sizeof(int8_t);
};

template <>
struct TypeInfos<uint8_t> {
    using Type = uint8_t;
    static constexpr DataType datatype = DataType::Uint8;
    static constexpr SizingStrategy sizing_strategy = SizingStrategy::Bytes;
    static constexpr size_t size = sizeof(uint8_t);
};

template <>
struct TypeInfos<int16_t> {
    using Type = int16_t;
    static constexpr DataType datatype = DataType::Int16;
    static constexpr SizingStrategy sizing_strategy = SizingStrategy::Bytes;
    static constexpr size_t size = sizeof(uint16_t);
};

template <>
struct TypeInfos<uint16_t> {
    using Type = uint16_t;
    static constexpr DataType datatype = DataType::Uint16;
    static constexpr SizingStrategy sizing_strategy = SizingStrategy::Bytes;
    static constexpr size_t size = sizeof(uint16_t);
};

template <>
struct TypeInfos<int32_t> {
    using Type = int32_t;
    static constexpr DataType datatype = DataType::Int32;
    static constexpr SizingStrategy sizing_strategy = SizingStrategy::Bytes;
    static constexpr size_t size = sizeof(int32_t);
};

template <>
struct TypeInfos<uint32_t> {
    using Type = uint32_t;
    static constexpr DataType datatype = DataType::Uint32;
    static constexpr SizingStrategy sizing_strategy = SizingStrategy::Bytes;
    static constexpr size_t size = sizeof(uint32_t);
};

template <>
struct TypeInfos<int64_t> {
    using Type = int64_t;
    static constexpr DataType datatype = DataType::Int64;
    static constexpr SizingStrategy sizing_strategy = SizingStrategy::Bytes;
    static constexpr size_t size = sizeof(int64_t);
};

template <>
struct TypeInfos<uint64_t> {
    using Type = uint64_t;
    static constexpr DataType datatype = DataType::Uint64;
    static constexpr SizingStrategy sizing_strategy = SizingStrategy::Bytes;
    static constexpr size_t size = sizeof(uint64_t);
};

template <>
struct TypeInfos<float> {
    using Type = float;
    static constexpr DataType datatype = DataType::Float;
    static constexpr SizingStrategy sizing_strategy = SizingStrategy::Bytes;
    static constexpr size_t size = sizeof(Type);
};

template <>
struct TypeInfos<double> {
    using Type = double;
    static constexpr DataType datatype = DataType::Double;
    static constexpr SizingStrategy sizing_strategy = SizingStrategy::Bytes;
    static constexpr size_t size = sizeof(Type);
};

struct Undefined { };
template <>
struct TypeInfos<Undefined> {
    using Type = char;
    static constexpr DataType datatype = DataType::Undefined;
    static constexpr SizingStrategy sizing_strategy = SizingStrategy::Bytes;
    static constexpr size_t size = 0;
};

template <>
struct TypeInfos<std::string> {
    using Type = std::string;
    static constexpr DataType datatype = DataType::String;
    static constexpr SizingStrategy sizing_strategy = SizingStrategy::Bytes;
    static constexpr size_t size = 0;
};

struct Buffer { };
template <>
struct TypeInfos<Buffer> {
    using Type = char;
    static constexpr DataType datatype = DataType::Buffer;
    static constexpr SizingStrategy sizing_strategy = SizingStrategy::Bytes;
    static constexpr size_t size = 0;
};

}
