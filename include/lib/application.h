#pragma once
#include <filesystem>
#include <optional>
#include "utils/error.h"
#include "utils/expected.h"

namespace automation {

/**
 * @brief The Application class represents the main container for a RikerIO Application.
 *
 * It mainly holds information about system folders that contain the cyclic and acyclic files.
 */
class Application {

public:
    struct Config {
        std::size_t shared_memory_size;
    };

    static std::filesystem::path build_root_path(const std::string& name);

    /**
     * Create a new application in the root_path folder/name.
     *
     * @param name Application name / Folder name
     * @param config Configuration object.
     * @return Application object on success otherwise utils::Error object.
     */
    static utils::Expected<Application> create(const std::string& name, Config config);

    /**
     * Attach a an existing application in the root_path folder.
     *
     * @param name Application name / Folder name
     * @return Application object on success otherwise utils::Error object.
     */
    static utils::Expected<Application> attach(const std::string& name);

    /**
     * Remove an existing application in the root_path folder.
     *
     * @param name Application name / Folder name to be removed
     * @param purge Removed persistent data as well.
     */
    static utils::Expected<utils::Success> remove(const std::string& name, bool purge = false);

    /**
     * Check wheather a Application exists.
     *
     * @paran name Application name / Folder name
     * @returns true if the application exists.
     */
    static bool exists(const std::string& name);

    Application() = default;

    Application(const Application&) = default;
    Application& operator=(const Application&) = default;
    Application(Application&& other);
    Application& operator=(Application&& other);

    /**
     * Get the application name
     */
    const std::string& name() const;

    /**
     * Get the application root_path
     */
    const std::filesystem::path& root_path() const;

    /**
     * Get the application cyclic subfolder path.
     */
    const std::filesystem::path& cyclic_data_path() const;

    /**
     * Get the application acyclic subfolder path
     */
    const std::filesystem::path& acyclic_data_path() const;

private:
    Application(const std::string& name);

    std::string _name;
    std::filesystem::path _root_path;
    std::filesystem::path _cyclic_data_path;
    std::filesystem::path _acyclic_data_path;

    void swap(Application& other);
};

}
