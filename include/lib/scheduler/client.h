#pragma once
#include "utils/event-service.h"
#include "utils/expected.h"
#include "utils/unix-domain-socket.h"
#include "utils/event-emitter.h"
#include "utils/log.h"
#include <filesystem>

namespace automation::scheduler {

enum class Function : uint16_t {
    Assign = 0x01,
    Loop = 0x02,
    Done = 0x03,
};

struct Message {
    Function fc;
    uint16_t argument;
};

/**
 * @brief Client represents a Connection to the Scheduling server.
 *
 * Incoming loop requests will be emitted as Event::Loop. A Done can be triggered with the
 * send_done command
 */
class Client : public utils::EventEmitter<> {
public:
    enum class Event {
        Loop
    };

    /**
     * @brief Create a new Client instance.
     *
     * The Client will connect and identify immediatly.
     * @param service
     * @param socket_path
     * @param task_id The Task ID to be identified with
     */
    Client(utils::EventService& service, utils::UnixDomainSocket::Connection connection, uint16_t task_id = 1)
        : _service(service)
        , _task_id(task_id)
        , _connection(std::move(connection))
    {
        _service.register_handler(
            _connection.fd(),
            std::bind(&Client::_handle_message, this));
        _identify();
        // std::cout << "scheduler::Client: task " << task_id << " identified with server" << std::endl;
    }

    /**
     * @brief send a done message to the server
     */
    void send_done()
    {
        Log::debug("scheduler::Client Task {} sending 'done' message to server", _task_id);

        _connection.write(Message { Function::Done, 0 });
    }

private:
    utils::EventService& _service;
    uint16_t _task_id;
    utils::UnixDomainSocket::Connection _connection;

    void _handle_message()
    {
        do {
            auto msg = utils::DescriptorEvent<Message>::read(_connection.fd());

            if (msg.type() == utils::DescriptorEvent<Message>::Type::closed) {
                Log::debug("scheduler::Client Task {} received close message", _task_id);
                return;
            }

            if (msg.type() == utils::DescriptorEvent<Message>::Type::error) {
                Log::debug("scheduler::Client Task {} handling error while reading from socket", _task_id);
                return;
            }

            if (msg.type() == utils::DescriptorEvent<Message>::Type::again) {
                Log::debug("scheduler::Client Task {} received no new message", _task_id);
                return;
            }

            if (msg.value().fc == Function::Loop) {
                Log::debug("scheduler::Client Task {} received loop message", _task_id);
                emit(Event::Loop);
            }

        } while (1);
    }

    void _identify()
    {
        Log::debug("scheduler::Client Task {} sending 'Assign' message", _task_id);
        _connection.write(Message { Function::Assign, _task_id });
    }
};

}
