#pragma once

#include "type_infos.h"
#include "var.h"

namespace automation {

struct MemoryAddress;

/**
 * @brief Structure for a Bit Datatype representing a single bit in the
 * cyclic shared memory.
 */
struct BitType {
    bool value;

    BitType& operator=(bool v)
    {
        value = v;
        return *this;
    }

    BitType(bool v)
        : value(v)
    {
    }
    bool operator==(bool v) const { return value == v; }
    bool operator!=(bool v) const { return value != v; }
    operator bool() { return value; }

    BitType()
        : value(false)
    {
    }
};

/**
 * @brief Description for the BitType Datatype
 */
template <>
struct TypeInfos<BitType> {
    using Type = bool;
    static constexpr DataType datatype = DataType::Bit;
    static constexpr SizingStrategy sizing_strategy = SizingStrategy::Bits;
    static constexpr size_t size = 1;
};

/**
 * @brief Copy Handler for the BitType Datatype to read and write
 * bits from the cyclic shared memory.
 */
template <Direction D>
class TypeCopyHandler<BitType, D> : public AbstractCopyHandler {
public:
    TypeCopyHandler(Var<BitType, D> var, void* shm_ptr, MemoryAddress adr)
        : _var(var)
    {
        auto rest = adr.index % 8;

        _var._memory->linked = true;
        _mask = 0x01 << rest;
        _shm_ptr = (uint8_t*)shm_ptr + adr.offset;
    }

    void copy_from()
    {
        bool value = (*_shm_ptr & _mask) > 0;
        _var.read_from(value);
    }

    void copy_from(const void* ptr)
    {
        throw std::runtime_error("pointer copy cannot be performed on bit type.");
    }

    void copy_to()
    {
        uint8_t old_value = *_shm_ptr;

        if (_var._memory->content) {
            *_shm_ptr = old_value | _mask;
        } else {
            *_shm_ptr = old_value & !_mask;
        }
    }

private:
    Var<BitType, D> _var;
    uint8_t _mask = 0;
    uint8_t* _shm_ptr = nullptr;
};

} // automation
