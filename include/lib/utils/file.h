#pragma once

#include <string>
#include <vector>
#include <filesystem>
#include <fstream>
#include <type_traits>

template <typename Test, template <typename...> class Ref>
struct is_specialization : std::false_type {
};

template <template <typename...> class Ref, typename... Args>
struct is_specialization<Ref<Args...>, Ref> : std::true_type {
};

namespace automation::utils {

template <typename T>
void file_write(std::ofstream& file, const T& t)
{
    if constexpr (is_specialization<T, std::vector>::value) {
        file_write(file, t.size());
        file.write((const char*)std::data(t), sizeof(typename T::value_type) * t.size());
    } else if constexpr (std::is_same<T, std::string>::value) {
        file_write(file, t.length());
        file.write(std::data(t), t.length());
    } else {
        file.write((char*)&t, sizeof(T));
    }
}

template <typename T>
T file_read(std::ifstream& file)
{
    T tmp;
    if constexpr (is_specialization<T, std::vector>::value) {
        auto size = file_read<typename T::size_type>(file);
        tmp.resize(size);
        file.read((char*)std::data(tmp), sizeof(typename T::value_type) * size);
    } else if constexpr (std::is_same<T, std::string>::value) {
        auto tmp_length = file_read<std::string::size_type>(file);
        tmp.resize(tmp_length, 0x00);
        file.read(std::data(tmp), tmp_length);
    } else {
        file.read((char*)&tmp, sizeof(T));
    }
    return tmp;
}

} // automation::utils
