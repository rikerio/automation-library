#pragma once

#include "utils/error.h"

namespace automation::utils {

template <typename T, typename E = Error>
class Expected {
public:
    using value_type = T;
    using error_type = E;

    Expected(const value_type& value)
        : _value(new T())
        , _error(nullptr)
    {
        *_value = value;
    }

    Expected(value_type&& value)
        : _value(new T())
        , _error(nullptr)
    {
        *_value = std::move(value);
    }

    Expected(error_type&& error)
        : _value(nullptr)
        , _error(new E())
    {
        *_error = std::move(error);
    }

    ~Expected()
    {
        if (_value != nullptr) {
            delete _value;
        }

        if (_error != nullptr) {
            delete _error;
        }
    }

    constexpr const T& operator->() const noexcept
    {
        return *_value;
    }

    constexpr T* operator->() noexcept
    {
        return _value;
    }

    constexpr const T* operator*() const noexcept
    {
        return _value;
    }

    constexpr T& operator*() noexcept
    {
        return *_value;
    }

    constexpr explicit operator bool() const noexcept
    {
        return has_value();
    }

    constexpr bool has_value() const noexcept
    {
        return _value != nullptr;
    }

    constexpr value_type& value()
    {
        return *_value;
    }

    constexpr const value_type& value() const noexcept
    {
        return *_value;
    }

    constexpr error_type& error() & noexcept
    {
        return *_error;
    }

    constexpr const error_type& error() const& noexcept
    {
        return *_error;
    }

    constexpr error_type&& error() && noexcept
    {
        return *_error;
    }

    constexpr const error_type&& error() const&& noexcept
    {
        return *_error;
    }

private:
    T* _value = nullptr;
    E* _error = nullptr;
};

}
