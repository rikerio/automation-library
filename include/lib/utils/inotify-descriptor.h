#pragma once
#include "file-descriptor.h"
#include <sys/inotify.h>
#include <string.h>
#include <filesystem>
#include <functional>

namespace automation::utils {

class InotifyDescriptor {

public:
    class Event {
    public:
        Event(inotify_event* event)
            : _mask(event->mask)
            , _name(event->name) {};

        uint32_t mask() const
        {
            return _mask;
        }

        const std::string& name()
        {
            return _name;
        }

    private:
        uint32_t _mask;
        std::string _name;
    };

    using EventVector = std::vector<Event>;

    InotifyDescriptor() = default;
    InotifyDescriptor(const std::filesystem::path& path, uint32_t mask);
    InotifyDescriptor(const InotifyDescriptor& other) = delete;
    InotifyDescriptor& operator=(const InotifyDescriptor& other) = delete;
    ~InotifyDescriptor();

    EventVector read();
    int fd() const;

private:
    FileDescriptor _fd;
    int _watch = -1;
};
}
