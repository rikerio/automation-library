#pragma once
#include "descriptor-event.h"
#include "file-descriptor.h"
#include <sys/timerfd.h>
#include <cstdarg>
#include <functional>
#include <system_error>
#include <stdexcept>
#include <signal.h>
#include <unistd.h>
#include <chrono>

namespace automation::utils {

class TimerDescriptor {

public:
    using event_type = uint64_t;
    using read_type = DescriptorEvent<event_type>;
    using Duration = std::chrono::duration<unsigned long, std::micro>;

    static TimerDescriptor create_oneshot(Duration dur);
    static TimerDescriptor create_cyclic(Duration cyclic);

    TimerDescriptor(Duration initial, Duration cyclic);

    TimerDescriptor() = default;
    TimerDescriptor(const TimerDescriptor&) = delete;
    TimerDescriptor& operator=(const TimerDescriptor&) = delete;

    read_type read();
    int fd() const;

private:
    FileDescriptor _fd;
};
}
