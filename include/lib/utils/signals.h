#pragma once
#include <signal.h>
#include <initializer_list>
#include <algorithm>
#include <system_error>

namespace automation::utils {

class Signals {
    enum class Mode {
        Block
    };

public:
    static Signals block(std::initializer_list<int> signals);
    ~Signals();

private:
    Signals(Mode mode, sigset_t mask);

    Mode _mode;
    sigset_t _mask;
};

}
