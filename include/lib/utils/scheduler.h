#pragma once

namespace automation::utils {

class Scheduler {
public:
    static void set_other();
    static void set_fifo(int priority);
    static void set_rr(int priority);
    static void set_deadline(int runtime, int period, int deadline);
};

}
