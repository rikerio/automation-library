#pragma once
#include "file-descriptor.h"
#include <functional>
#include <stdexcept>
#include <sys/eventfd.h>
#include "descriptor-event.h"

namespace automation::utils {

class EventDescriptor {
public:
    using event_type = uint64_t;
    using read_type = DescriptorEvent<event_type>;

    static EventDescriptor create(int flags = EFD_NONBLOCK | EFD_CLOEXEC);

    EventDescriptor() = default;

    EventDescriptor(EventDescriptor&& other);
    EventDescriptor& operator=(EventDescriptor&& other);

    EventDescriptor(const EventDescriptor&) = delete;
    EventDescriptor& operator=(const EventDescriptor&) = delete;

    read_type read();
    void write(event_type value = 1);
    int fd() const;

private:
    EventDescriptor(int fd);

    utils::FileDescriptor _fd;
};
}
