#pragma once
#include "regex"
#include "string"
#include "tgmath.h"

namespace automation::utils {

class Size {
public:
    Size(const std::string& str)
        : size(0)
    {

        static std::regex unit_regex("^(\\d*(?:.\\d+)?)(B|K|M|b|k|m)?$");

        float fsize = 0.0f;
        float factor = 1.0f;

        std::smatch match;
        if (!std::regex_match(str, match, unit_regex)) {
            return;
        }

        if (match.size() == 3) {
            fsize = std::stof(match[1]);

            if (match[2] == "B" || match[2] == "b") {
                factor = 1.0f;
            } else if (match[2] == "K" || match[2] == "k") {
                factor = 1000.0f;
            } else if (match[2] == "M" || match[2] == "m") {
                factor = pow(1000.0f, 2);
            }

        } else {
            return;
        }

        float ffsize = fsize * factor;

        if (ffsize <= 1.0f) {
            return;
        }

        error = false;
        size = ffsize;
    }

    operator size_t()
    {
        return size;
    }

    bool is_error() const
    {
        return error;
    }

private:
    size_t size;
    bool error = true;
};

}
