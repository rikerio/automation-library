#pragma once

#include "file-descriptor.h"
#include <filesystem>

namespace automation::utils {

class LockFile {
public:
    enum class Type {
        Exclusive,
        Shared
    };

    static LockFile exclusive(const std::filesystem::path&);
    static LockFile shared(const std::filesystem::path&);

    LockFile(const std::filesystem::path&, Type type);
    ~LockFile();

    bool valid() const;
    void lock();
    void unlock();

private:
    Type _type;
    FileDescriptor _fd;
    std::filesystem::path _path;
};

}
