#pragma once

#include <mutex>
#include "event-poll.h"
#include "event-descriptor.h"

namespace automation::utils {

class EventService {
public:
    using Handler = std::function<void()>;

    class Driver {
    public:
        virtual ~Driver() {};
        virtual void add(int fd, Handler) = 0;
        virtual void remove(int fd) = 0;
        virtual Handler get(int fd) = 0;
    };

    class DefaultDriver : public Driver {

    public:
        void add(int fd, Handler handler)
        {
            std::lock_guard lock(_mutex);
            _map.insert(std::make_pair(fd, handler));
        }

        void remove(int fd)
        {
            std::lock_guard lock(_mutex);
            _map.erase(fd);
        }

        Handler get(int fd)
        {
            std::lock_guard lock(_mutex);
            auto it = _map.find(fd);

            if (it == _map.end()) {
                return nullptr;
            }

            return it->second;
        }

    private:
        std::map<int, Handler> _map;
        std::mutex _mutex;
    };

    class FixedSizeDriver : public Driver {
        struct Entry {
            bool in_use = false;
            int fd = -2;
            Handler handler = nullptr;
        };

    public:
        FixedSizeDriver(std::size_t size)
            : _handler_list(size)
        {
        }

        void add(int fd, Handler handler)
        {
            auto it = std::find_if(_handler_list.begin(), _handler_list.end(), [](auto& entry) {
                return !entry.in_use;
            });

            if (it == _handler_list.end()) {
                throw std::runtime_error("utils::EventSystem::FixedSizeDriver over satisfied.");
            }

            *it = Entry { true, fd, handler };
        }

        void remove(int fd)
        {
            auto found = std::find_if(_handler_list.begin(), _handler_list.end(), [&](auto& entry) {
                return entry.fd == fd;
            });

            if (found == _handler_list.end()) {
                return;
            }

            *found = {};
        }

        Handler get(int fd)
        {
            auto found = std::find_if(_handler_list.begin(), _handler_list.end(), [&](auto& entry) {
                return entry.fd == fd;
            });

            if (found == _handler_list.end()) {
                return nullptr;
            }

            return found->handler;
        }

    private:
        std::vector<Entry> _handler_list;
    };

    enum class Strategy {
        Dynamic,
        Fixed
    };

    EventService(EventService&& other);
    EventService& operator=(EventService&& other);

    EventService(const EventService& other) = delete;
    EventService& operator=(const EventService& other) = delete;

    EventService(Strategy stategy = Strategy::Dynamic, std::size_t max_size = 32);
    ~EventService();

    void register_handler(int fd, Handler handler)
    {
        _driver->add(fd, handler);
        _epoll.reg(fd);
    }

    void register_descriptor(int fd)
    {
        _epoll.reg(fd);
    }

    void deregister_descriptor(int fd)
    {
        _epoll.unreg(fd);
        _driver->remove(fd);
    }

    void start();
    void stop();

private:
    bool _running;
    Strategy _strategy;
    utils::EventDescriptor _exit_event;
    utils::EventPoll _epoll;

    std::unique_ptr<Driver> _driver;
    std::vector<utils::EventPoll::Event> _buffer;

    void swap(EventService& other);
};
}
