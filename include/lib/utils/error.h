#pragma once

#include <cstring>
#include <string>
#include <utility>
#include <system_error>

namespace automation::utils {

class Success { };

class Error {
public:
    Error() = default;

    Error(std::string message)
        : _msg(std::move(message))
    {
    }

    Error(const Error& other) = default;
    Error& operator=(const Error& other) = default;

    Error(Error&& other) = default;
    Error& operator=(Error&& other) = default;

    const std::string& message() const
    {
        return _msg;
    }

private:
    std::string _msg;
};
}
