#pragma once
#include <sys/signalfd.h>
#include <cstdarg>
#include <functional>
#include <stdexcept>
#include <signal.h>
#include <unistd.h>
#include "descriptor-event.h"
#include "file-descriptor.h"
#include "expected.h"

namespace automation::utils {

class SignalDescriptor {

public:
    using event_type = signalfd_siginfo;
    using read_type = DescriptorEvent<event_type>;

    SignalDescriptor() = default;

    SignalDescriptor(const SignalDescriptor&) = delete;
    SignalDescriptor& operator=(const SignalDescriptor&) = delete;

    SignalDescriptor(std::initializer_list<int> s, int flags = SFD_NONBLOCK | SFD_CLOEXEC);
    Expected<read_type, read_type::Type> read();
    int fd() const;

private:
    FileDescriptor _fd;
};

}
