#pragma once

#include "memory_address.h"

namespace automation::utils {

class BitString {
public:
    static MemoryAddress from_string(const std::string& str);
};
}
