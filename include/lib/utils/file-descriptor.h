#pragma once

#include <algorithm>
#include <unistd.h>

namespace automation::utils {

class FileDescriptor {
public:
    FileDescriptor() = default;
    FileDescriptor(const FileDescriptor&) = delete;
    FileDescriptor& operator=(const FileDescriptor&) = delete;
    FileDescriptor(FileDescriptor&& other)
    {
        std::swap(_fd, other._fd);
    }
    FileDescriptor& operator=(FileDescriptor&& other)
    {
        std::swap(_fd, other._fd);
        return *this;
    }

    FileDescriptor(int fd)
        : _fd(fd) {};
    ~FileDescriptor()
    {
        if (_fd != -1) {
            ::close(_fd);
        }
    }

    int fd() const
    {
        return _fd;
    }

private:
    int _fd = -1;
};
}
