#pragma once
#include "stdexcept"
#include "string"
#include "vector"

namespace automation::utils {

class SemanticVersion {

public:
    SemanticVersion(const std::string&);

    unsigned int get_major() const;
    unsigned int get_minor() const;
    unsigned int get_patch() const;
    unsigned int get_commit() const;
    const std::string& get_hash() const;
    const std::string& get_version() const;
    const std::string& get_extended_version() const;

    // reads left ^ right as "left is compatible to right"
    bool operator^(const SemanticVersion& right) const
    {

        if (major != right.get_major()) {
            return false;
        }

        return minor >= right.get_minor();
    }

private:
    std::string orig;
    std::string version;
    std::string extended_version;

    unsigned int major = 0;
    unsigned int minor = 0;
    unsigned int patch = 0;
    unsigned int commit = 0;

    std::string hash = "";
};

}
