#pragma once
#include <utility>
#include <chrono>
#include <iostream>
#include <fmt/core.h>

/* if you need some loging, for now, make a #define DEBUG 1 and, where
 * entered, a debug message shall appear. This needs to be more severe
 * of course since I think logging is important. Hence, in a realtime
 * environment this task is not trivial. So this is due for further
 * development
 */

namespace automation {

class Log {
public:
    template <typename... Arg>
    static void debug(const char* format, Arg... args)
    {
#ifdef DEBUG
        auto now = std::chrono::system_clock::now();
        auto duration = now.time_since_epoch();
        auto micros = std::chrono::duration_cast<std::chrono::microseconds>(duration).count();
        std::cout << micros << " : " << fmt::format(format, std::forward<Arg>(args)...) << std::endl;
#endif
    }
};

}
