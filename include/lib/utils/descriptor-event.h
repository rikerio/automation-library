#pragma once

#include <unistd.h>
#include <utility>
#include <cerrno>
#include <string>

namespace automation::utils {

template <typename T>
class DescriptorEvent {
public:
    enum class Type {
        ok,
        again,
        error,
        closed
    };

    static DescriptorEvent<T> read(int fd, std::size_t capacity = sizeof(T))
    {
        T result;
        int ret;
        if constexpr (std::is_same<T, std::string>::value) {
            result.resize(capacity);
            ret = ::read(fd, std::data(result), capacity);
        } else {
            ret = ::read(fd, &result, capacity);
        }

        if (ret == -1 && errno == EAGAIN) {
            return Type::again;
        }

        if (ret == -1) {
            return Type::error;
        }

        if (ret == 0) {
            return Type::closed;
        }

        if constexpr (std::is_same<T, std::string>::value) {
            // warn, possible error where the capacity equals the read
            // in that case the end of the stream cannot be detected
            std::string tmp = result.substr(0, ret);
            return DescriptorEvent(std::move(tmp));
        }

        return DescriptorEvent(result);
    }

    DescriptorEvent() = default;
    DescriptorEvent(T value)
        : _type(Type::ok)
        , _value(value)
    {
    }

    DescriptorEvent(Type type)
        : _type(type)
    {
    }

    explicit operator bool() const
    {
        return valid();
    }

    const T& value() const
    {
        return _value;
    }

    bool valid() const
    {
        return _type == Type::ok;
    }

    Type type() const
    {
        return _type;
    }

private:
    Type _type;
    T _value;
};

}
