#pragma once

#include <filesystem>
#include <vector>
#include "utils/file-descriptor.h"
#include "utils/descriptor-event.h"

namespace automation::utils {

/**
 * @brief Representation of a Unix Domain Socket
 *
 */
class UnixDomainSocket {

public:
    /**
     * @brief Representation of a Unix Domain Socket Client Connection
     */
    class Connection {
    public:
        Connection(const Connection&) = delete;
        Connection& operator=(const Connection&) = delete;
        Connection(Connection&& other);
        Connection& operator=(Connection&& other);

        Connection() = default;
        Connection(int fd);

        /**
         * @brief Write a binary buffer to the server/client.
         * @param buffer Pointer to the data
         * @param size Size of the data
         */
        void write(const void* buffer, std::size_t size);

        /**
         * @brief Write a binary buffer to the server/client.
         * @param buffer
         */
        void write(const std::vector<char>& buffer);

        /**
         * @brief Write a string message to the server/client.
         * @param message
         */
        void write(const std::string& message);

        template <typename T>
        void write(T object)
        {
            write(&object, sizeof(object));
        }

        /**
         * @brief Read a message from the server/client
         */
        DescriptorEvent<std::string> read_as_string();
        DescriptorEvent<std::vector<char>> read_as_buffer();

        template <typename T>
        DescriptorEvent<T> read()
        {
            return DescriptorEvent<T>::read(_fd.fd(), sizeof(T));
        }

        int fd() const;

    private:
        FileDescriptor _fd;
    };

    /**
     * @brief Creates a new Connection object when a client connects.
     * @param socket The UnixDomainSocket on which the connection event occured
     * @return a new Connection object.
     */
    static Connection accept(UnixDomainSocket& socket);

    /**
     * @brief Creates a new Connection object by connecting to the
     * specified socket path
     * @param socket_path
     * @return a new Connection object.
     */
    static Connection connect(const std::filesystem::path& socket_path);

    /**
     * @brief Create a new Unix Domain Server Socket at the given path
     * @param socket_path
     */
    UnixDomainSocket(const std::filesystem::path socket_path);
    explicit UnixDomainSocket(int fd);

    int fd() const;

    /**
     * @brief Returns a new connection file descriptor if present
     * @return file descriptor or -1
     */
    int accept() const;

private:
    FileDescriptor _fd;
};

}
