#pragma once

#include <map>
#include <atomic>
#include <stdexcept>
#include <system_error>
#include <memory>
#include <functional>
#include <iostream>
#include <string.h>
#include <sys/epoll.h>
#include <unistd.h>
#include "utils/file-descriptor.h"

namespace automation::utils {

class EventPoll {

public:
    using Event = struct epoll_event;

    EventPoll(EventPoll&& other)
    {
        std::swap(_fd, other._fd);
    }

    EventPoll& operator=(EventPoll&& other)
    {
        std::swap(_fd, other._fd);
        return *this;
    }

    EventPoll();

    void reg(int fd);
    void unreg(int fd);

    template <typename T>
    std::size_t wait(T& events, int timeout = -1)
    {
        auto n = epoll_wait(_fd.fd(), events.data(), events.size(), timeout);

        if (n == -1) {
            throw std::system_error(errno, std::system_category(), "epoll_wait");
        }

        return n;
    }

private:
    FileDescriptor _fd;
};
}
