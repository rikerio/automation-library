#pragma once

namespace automation::utils {

class Pointer {

public:
    Pointer(void* addr)
        : _addr(addr)
    {
    }

    void* add(size_t byte_offset, uint64_t bit_offset = 0)
    {
        char* tmp = (char*)_addr;
        tmp += byte_offset;
        if (bit_offset > 0) {
            uint8_t rest = bit_offset % 8;
            tmp += (bit_offset - rest) / 8;
        }
        return static_cast<void*>(tmp);
    }

private:
    void* _addr = nullptr;
};

}
