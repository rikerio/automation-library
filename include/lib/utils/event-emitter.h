#pragma once

#include <functional>
#include <map>
#include <stdexcept>

namespace automation::utils {

template <typename... Args>
class EventEmitter {

public:
    using Handler = std::function<void(Args...)>;

    template <typename T>
    void on(T event_id, Handler handler)
    {
        on(static_cast<unsigned int>(event_id), handler);
    }

    template <typename T>
    void emit(T event_id, Args... args)
    {
        emit(static_cast<unsigned int>(event_id), std::forward<Args>(args)...);
    }

    void on(unsigned int event_id, Handler handler)
    {
        if (_executing) {
            throw std::runtime_error("EventEmitter nesting not allowed");
        }
        _handler_map.insert(std::make_pair(event_id, handler));
    }

    void emit(unsigned int event_id, Args... args)
    {
        std::for_each(_handler_map.lower_bound(event_id), _handler_map.upper_bound(event_id), [&](auto& h) {
            if (h.second) {
                _executing = true;
                h.second(std::forward<Args>(args)...);
                _executing = false;
            }
        });
    }

private:
    std::multimap<unsigned int, Handler> _handler_map;
    bool _executing = false;
};

}
