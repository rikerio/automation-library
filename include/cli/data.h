#pragma once
#include "cli/container.h"

namespace cli::container::data {

class Inspect {
public:
    Inspect(CLI::App& app, cli::container::Main& main);

private:
    cli::container::Main& _main;
    std::string _name;
    void execute();
};

class List {
public:
    List(CLI::App& app, cli::container::Main& main);

private:
    cli::container::Main& _main;
    void execute();
};

class Read {
public:
    Read(CLI::App& app, cli::container::Main& main);

private:
    cli::container::Main& _main;
    std::string _name;
    void execute();
};

class Write {
public:
    Write(CLI::App& app, cli::container::Main& main);

private:
    cli::container::Main& _main;
    std::string _name;
    std::string _type;
    std::string _value;
    void execute();
};

}
