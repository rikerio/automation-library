#pragma once
#include "cli/container.h"

namespace cli::container::link {

class Inspect {
public:
    Inspect(CLI::App& app, cli::container::Main& main);

private:
    cli::container::Main& _main;
    std::string _name;
    void execute();
};

class List {
public:
    List(CLI::App& app, cli::container::Main& main);

private:
    cli::container::Main& _main;
    void execute();
};

class Add {
public:
    Add(CLI::App& app, cli::container::Main& main);

private:
    std::string _link_name = "";
    std::string _data_name = "";
    cli::container::Main& _main;
    void execute();
};

class Remove {
public:
    Remove(CLI::App& app, cli::container::Main& main);

private:
    std::string _link_name;
    std::string _data_name;
    cli::container::Main& _main;
    void execute();
};

}
