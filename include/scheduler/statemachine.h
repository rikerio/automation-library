#pragma once
#include <vector>
#include <functional>
#include <map>
#include <set>
#include <stdexcept>
#include <stdint.h>

namespace automation::scheduler {

class StateMachine {
public:
    using Handler = std::function<void(uint16_t)>;

    class Config : public std::vector<std::vector<uint16_t>> {
    public:
        Config() = default;
        Config(std::initializer_list<std::vector<uint16_t>> _init_list);

        std::size_t task_count() const;
        std::size_t sprint_count() const;

        void add_lap();
        void add_task(uint16_t id);

    private:
        std::set<uint16_t> _task_ids;
    };

    enum class Event {
        Loop,
        Done,
        Stale
    };

    StateMachine(Config config);

    void start();
    void attach(uint16_t id);
    void detach(uint16_t id);
    void report_done(uint16_t id);
    void on(Event event, Handler handler);

private:
    enum class State {
        Offline,
        Ready,
        InLoop,
        Done,
        Stale
    };

    struct Task {
        uint16_t id;
        State state;
        Task(uint16_t id, State state);
    };

    class EventMap : public std::multimap<Event, Handler> {
    public:
        void add(Event event, Handler handler);
        void execute(Event event, uint16_t task_id);
    };

    struct Lap {
        std::vector<Task> tasks;
        bool tasks_done() const;
    };

    class Sprint : public std::vector<Lap> {
    public:
        Sprint(const Config& config);

        void mark_stale_tasks(Handler handler);
        void reset_task(uint16_t task_id);
        void for_each_task(std::function<void(Task&)> handler);
        void reset();
    };

    Config _config;
    Sprint _sprint;
    EventMap _event_map;

    Sprint::iterator _cur_lap;

    void notify();
};

}
