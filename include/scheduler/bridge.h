#pragma once

#include <algorithm>
#include "utils/event-emitter.h"
#include "server.h"

namespace automation::scheduler {

class Bridge : public utils::EventEmitter<uint16_t> {
public:
    Bridge(scheduler::AbstractServer& server, std::size_t size);

    void send_loop_request(int task_id);
    uint16_t get_task_id(int fd);
    int get_fd(int task_id);

private:
    struct Entry {
        int fd = -1;
        uint16_t task_id = 0;
    };

    scheduler::AbstractServer& _server;
    std::vector<Entry> _list;

    void _assign(int fd, uint16_t task_id);
    void _unassign(int fd, uint16_t task_id);
    void _handle_event(scheduler::Server::Event event, int fd, scheduler::Message msg);
};

}
