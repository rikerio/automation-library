#pragma once

#include <filesystem>
#include "statemachine.h"
#include "utils/expected.h"

using namespace automation;

namespace automation::scheduler {

class ConfigParser {
public:
    static utils::Expected<StateMachine::Config> parse_file(const std::filesystem::path& path);
};

}
