#pragma once

#include "scheduler/strategy.h"
#include "os/eventbus.h"
#include "os/unix-domain-socket.h"
#include <cstddef>
#include <filesystem>

namespace automation::scheduler {

class RikerIO : public Strategy {

public:
    struct Config {
        std::filesystem::path socket_filepath;
        uint16_t index;
    };

    RikerIO(Config config);

    void setup();
    int yield(int);

private:
    const Config _config;

    os::Eventbus _eventbus;
    os::UnixDomainSocket::Client _client;

    bool _loop_request = false;
    bool _close_request = false;
    bool _init_request = false;
    bool _first_loop_flag = true;

private:
    void handle_message();
};

}
