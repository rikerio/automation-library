#pragma once

#include "utils/event-service.h"
#include "utils/expected.h"
#include "utils/unix-domain-socket.h"
#include "utils/event-emitter.h"
#include "scheduler/client.h"
#include <filesystem>

using namespace automation;

namespace automation::scheduler {

class AbstractServer : public utils::EventEmitter<int, Message> {
public:
    virtual ~AbstractServer() {};

    virtual void send_loop_request(int fd) = 0;
};

/**
 * @brief The server handles the incoming messages and sends of
 * loop requests. The messages that are received will be emitted
 * as Events Event::NewTask, Event::TaskDone and Event::TaskExisted
 *
 * The server accepts only a fixed number of connections. If this will be exceeded then
 * a runtime_error will be thrown.
 */
class Server : public AbstractServer {
public:
    enum class Event {
        NewTask,
        TaskDone,
        TaskExited
    };

    Server() = default;

    /**
     * @brief Setup new Scheduling Server
     *
     * @param service
     * @param path socket path
     * @param pool_size max connections count
     */
    Server(utils::EventService& service, utils::UnixDomainSocket socket, std::size_t pool_size);
    ~Server();

    /**
     * @brief Returns number of current connections
     */
    std::size_t connections() const;

    /**
     * @brief Send loop request to connection specified by fd
     */
    void send_loop_request(int fd);

private:
    using ConnectionPool = std::vector<utils::UnixDomainSocket::Connection>;

    utils::EventService& _service;
    ConnectionPool _pool;
    utils::UnixDomainSocket _socket;

    void _handle_connection();
    void _handle_message(utils::UnixDomainSocket::Connection& connection);
    void _reset_connection(int fd);

    ConnectionPool::iterator _find_by_fd(int fd);
    utils::UnixDomainSocket::Connection& _emplace_connection(int fd);
};
}
